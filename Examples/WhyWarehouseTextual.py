'''
PyEOQ Example: WhyWarehouseExample.py
----------------------------------------------------------------------------------------
This file implements the following warehouse example in PyEOQ with textual commands: 

Imagine having the following hierarchically organized domain-specific model of you warehouse? 

                                                          _______________
     ___________              _______________            |    Article    |
    | Warehouse | categories |   Category    |  articles |---------------|
    |-----------|<>--------->|---------------|<>-------->| name : string |
    |           |           *| name : string |          *| price : real  |
    |___________|        o-->|_______________|<>-o       | sells : int   |
                         |  *                    |       |_______________|
                         o-----------------------o
                               categories

Would you like to get a list of articles?

    GET !Article

Would you like to know which one is sold less than 3-times?

    GET !Article{/sells<3}

You might want to reduce their price by 10%?

    SET !Article{/sells<3} 'price' !Article{/sells<3}/price&MUL0.9

Would you like to see a list of the names of the categories of the badly selling articles sorted by ascendingly?

    GET !Article{/sells<3}@PARENT/name_[]:SORTASC

See EOQ User Manual for more information: https://gitlab.com/eoq/doc

2020 Bjoern Annighoefer
'''

from eoq2.mdb.pyecore import PyEcoreSingleFileMdbProvider,PyEcoreMdbAccessor,PyEcoreIdCodec
from eoq2.domain.local import LocalMdbDomain

from eoq2.serialization import TextSerializer
      
'''
MAIN: Execution starts here
'''            
if __name__ == '__main__':
    # Initialize an WorkspaceMDB
    mdbProvider = PyEcoreSingleFileMdbProvider('Workspace/GeneralStore.warehouse',"Workspace/Meta/warehouse.ecore")
    valueCodec = PyEcoreIdCodec() #create the default value codec that converts objects to ids
    mdbAccessor = PyEcoreMdbAccessor(mdbProvider.GetMdb(),valueCodec)
    domain = LocalMdbDomain(mdbAccessor)
    mdbProvider.CoupleWithDomain(domain, valueCodec)
    
    serializer = TextSerializer() # The text serializer transforms EOQ expressions in string and back
    
    textCmds = [
            "GET !Article",
            "GET !Article{/sells<3}",
            "SET !Article{/sells<3} 'price' !Article{/sells<3}/price&MUL0.9",
            "GET !Article{/sells<3}@PARENT/name_[]:'SORTASC'",
            "SET !Article{/sells<3} 'price' !Article{/sells<3}/price&DIV0.9",
           ]
    
    for textCmd in textCmds:
        cmd = serializer.Des(textCmd) #create a command out of a string
        res = domain.Do(cmd) #execute the common on the domain
        textRes = str(res) #results can be stringified like this
        print("%s --> %s"%(textCmd,textRes))
    
