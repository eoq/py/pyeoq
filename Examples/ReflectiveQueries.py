'''
PyEOQ Example: CommandsAndQueries.py
----------------------------------------------------------------------------------------
This example gives a provides examples on EOQ commands and queries that can be used to 
access the meta-model.  

See EOQ User Manual for more information: https://gitlab.com/eoq/doc

2020 Bjoern Annighoefer
'''

from eoq2.mdb.pyecore import PyEcoreSingleFileMdbProvider,PyEcoreWorkspaceMdbProvider,PyEcoreMdbAccessor,PyEcoreIdCodec
from eoq2.domain.local import LocalMdbDomain

from eoq2.action.externalpy import ExternalPyScriptHandler
from eoq2 import Get,Set,Add,Rem,Mov,Clo,Crt,Crn,Qrf,Sts,Chg,Gaa,Cal,Asc,Abc,Cmp,CloModes,Hel,Ses,Gby,Obs,Ubs
from eoq2 import Qry,Obj,His,Cls,Ino,Met,Idx,Pth,Arr,Equ,Eqa,Neq,Les,Gre
from eoq2.serialization import JsonSerializer,TextSerializer,PySerializer,JsSerializer
from eoq2.event import EvtTypes

from eoq2.util import Backupper,NoLogging,ConsoleLogger,ConsoleAndFileLogger,LogLevels,DEFAULT_LOG_LEVELS

from timeit import default_timer as timer #used to time the command's execution time.
import sys


'''
MAIN: Execution starts here
'''          
if __name__ == '__main__':
    #Basic configuration
    WORKSPACE_MDB = False
    WORKSPACE_DIR = 'Workspace'
    LOG_DIR = './log'
    MODELFILE = "MinimalFlightControl.oaam"

    #create a backup. This is only for testing purposes and not necessary (both lines needed)
    #backupper = Backupper([WORKSPACE_DIR,LOG_DIR])
    #backupper.CreateBackup()

    #define loglevels (chose one of the following lines)
    #logLevels = DEFAULT_LOG_LEVELS
    logLevels = DEFAULT_LOG_LEVELS+["change","transaction","event"]
    #logLevels = DEFAULT_LOG_LEVELS+[LogLevels.DEBUG,"change","transaction","event"]

    #initialize logger. For the file based logger this must happen after the backup. (chose one of the following lines)
    #logger = NoLogging() #no output at all
    #logger = ConsoleLogger() #only console output
    logger = ConsoleAndFileLogger(logDir=LOG_DIR,activeLevels=logLevels) #console and file output

    #Create a model data base (MDB) (chose one of the following lines)
    mdbProvider = None
    if(WORKSPACE_MDB):
        mdbProvider = PyEcoreWorkspaceMdbProvider(WORKSPACE_DIR,metaDir=['./Meta'],saveTimeout=1.0,logger=logger)
    else:
        mdbProvider = PyEcoreSingleFileMdbProvider(WORKSPACE_DIR+"/"+MODELFILE,WORKSPACE_DIR+"/Meta/oaam.ecore",saveTimeout=1.0,logger=logger)
        

    #Create an encoding strategy for model based data 
    valueCodec = PyEcoreIdCodec()

    #Create an unique accessor to the data
    mdbAccessor = PyEcoreMdbAccessor(mdbProvider.GetMdb(),valueCodec)

    #Create a domain and couple it with the mdb provider
    domain = LocalMdbDomain(mdbAccessor,logger=logger)
    mdbProvider.CoupleWithDomain(domain, valueCodec)
    
    #Initialize a new session
    print("Opening session")
    sessionId = domain.Do(Hel('user','pw'))
    
    #Initialize the event listener (specific events must be enabled afterwards).
    print('My session ID: %s'%(sessionId))
    
    #Register external actions (optional)
    #externalActionHandler = ExternalPyScriptHandler(domain.cmdRunner.callManager,'../Examples/workspace/actions',logger=logger)

    #prepare serializers
    jsonSerializer = JsonSerializer()
    textSerializer = TextSerializer()
    pySerializer = PySerializer()
    jsSerializer = JsSerializer()

    #Retrieve the model resource for the file we would like to work on. (Single file and workspace mdb have different solutions)
    root = None
    if(WORKSPACE_MDB):
        cmd = Get(Pth('resources').Sel(Pth('name').Equ(MODELFILE)).Idx(0).Pth('contents').Idx(0)) #workspace mdb
        jsonCmd = jsonSerializer.Ser(cmd) #workspace mdb
        print(jsonCmd) #workspace mdb
        root = domain.Do(cmd) #workspace mdb
    else:
        root = mdbAccessor.GetRoot() #single file mdb

    #comprehensive list of commands
    cmds = [
            #get all feature names
            Get(Qry(root).Cls('Task').Idx(0).Met('FEATURENAMES')),
            #get all feature values
            Get(Qry(root).Cls('Task').Idx(0).Met('FEATUREVALUES')),
            #get all the contained children
            Get(Qry(root).Cls('Task').Idx(0).Met('CONTAINMENTVALUES')),
            #get all the contained children in one list
            Get(Qry(root).Cls('Task').Idx(0).Met('CONTAINMENTVALUES').Idx('FLATTEN')),
            #browse all containing features
            Cmp().Get(Qry(root).Cls('Task').Idx(0).Met('CONTAINMENTS'))
                 .Get(His(0).Pth('name'))
                 .Get(His(0).Pth('eType').Pth('name'))
                 .Get(His(0).Pth('lowerBound'))
                 .Get(His(0).Pth('upperBound')),
            #browse all features starting from a class using the ecore EMF API
            Cmp().Get(Qry(root).Cls('Task').Idx(0).Met('CLASS'))
                 .Get(His(0).Pth('eStructuralFeatures'))
                 .Get(His(1).Pth('name'))
                 .Get(His(1).Pth('eType').Pth('name'))
                 .Get(His(1).Pth('lowerBound'))
                 .Get(His(1).Pth('upperBound')),
            #browse all attributes starting from a class using the ecore EMF API
            Cmp().Get(Qry(root).Cls('Task').Idx(0).Met('CLASS'))
                 .Get(His(0).Pth('eAttributes'))
                 .Get(His(1).Pth('name'))
                 .Get(His(1).Pth('eType').Pth('name'))
                 .Get(His(1).Pth('lowerBound'))
                 .Get(His(1).Pth('upperBound')),
            Gby(sessionId)
#             #test meta
#             Get(Met('METAMODELS')),
#             Get(Qry(root).Met('CLASS')),
#             Get(Met('CLASS',['http://www.oaam.de/oaam/model/v140/functions','Task'])), #context less
#             Get(Qry(root).Met('CLASSNAME')),
#             Get(Qry(root).Met('CONTAINER')),
#             Get(Qry(root).Met('PARENT')),
#             Get(Qry(root).Cls('Task').Idx(0).Met('ALLPARENTS')),
#             Get(Qry(root).Cls('Task').Idx(0).Met('ASSOCIATES').Met('CLASSNAME')),
#             Get(Qry(root).Cls('Task').Idx(0).Met('ASSOCIATES',[Qry(root).Pth('allocations')]).Met('CLASSNAME')),
#             Get(Qry(root).Met('INDEX')),
#             Get(Qry(root).Met('CONTAININGFEATURE')),
#             Get(Qry(root).Met('FEATURES')),
#             Get(Qry(root).Met('FEATURENAMES')),
#             Get(Qry(root).Met('FEATUREVALUES')),
#             Get(Qry(root).Met('ATTRIBUTES')),
#             Get(Qry(root).Met('ATTRIBUTENAMES')),
#             Get(Qry(root).Met('ATTRIBUTEVALUES')),
#             Get(Qry(root).Met('REFERENCES')),
#             Get(Qry(root).Met('REFERENCENAMES')),
#             Get(Qry(root).Met('REFERENCEVALUES')),
#             Get(Qry(root).Met('CONTAINMENTS')),
#             Get(Qry(root).Met('CONTAINMENTNAMES')),
#             Get(Qry(root).Met('CONTAINMENTVALUES')),
#             Get(Qry(root).Met('IF',[Met("PARENT"),Met("PARENT").Pth("name"),"no"])),
#             Get(Met('IF',[Met("PARENT"),Met("PARENT").Pth("name"),"no"])),
#             Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Pth('tasks').Idx('FLATTEN')),
#             #test class meta
#             Get(Qry(root).Cls('Task').Idx(0).Met('CLASS').Met('PACKAGE').Pth('name')),
#             Get(Qry(root).Cls('Task').Idx(0).Met('CLASS').Met('SUPERTYPES').Pth('name')),
#             Get(Qry(root).Cls('Task').Idx(0).Met('CLASS').Met('ALLSUPERTYPES').Pth('name')),
#             Get(Qry(root).Cls('Task').Idx(0).Met('CLASS').Met('IMPLEMENTERS').Pth('name')),
#             Get(Qry(root).Cls('Task').Idx(0).Met('CLASS').Met('ALLIMPLEMENTERS').Pth('name')),
#                                
#             #index
#             Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Pth('tasks')),
#             Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Pth('tasks').Idx(0)),
#             Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Pth('tasks').Idx('FLATTEN')),
#             Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Pth('tasks').Idx('SIZE')),
#             Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Pth('tasks').Idx('SORTASC')),
#             Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Pth('tasks').Idx('SORTDSC')),
#             Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Pth('tasks').Idx([0,6,2])), #extract range
#                            
#             #test eqa
#             Get( Qry(root).Cls('Task').Pth('name') ),
#             Get( Qry(root).Cls('Task').Pth('name').Eqa(Arr(['ElevatorR1','AileronL1'])) ),
#             Get( Qry(root).Cls('Task').Pth('name').Sel(Eqa(Arr(['ElevatorR1','AileronL1']))) ),
#             Get( Qry(root).Cls('Task').Sel(Pth('name').Eqa(Arr(['ElevatorR1','AileronL1']))) ),    
#                                        
#            
            #finished
            ]

    #Run an commands sequentially and print their output
    testNr = 1
    failedTxt2PyTests = []
    for cmd in cmds:
        start = timer()
        jsonCmd = jsonSerializer.Ser(cmd)
        #test the different serializers as well as text parsing
        print("%d: CMD (JSON): %s"%(testNr,jsonCmd))
        print("%d: CMD (TXT): %s" % (testNr, textSerializer.Ser(cmd)))
        try:
            res = textSerializer.Des(textSerializer.Ser(cmd))
            try:
                print(f"{testNr}: CMD (TXT2PY): Success! {res.cmd} {res.a}")
            except:
                print(f"{testNr}: CMD (TXT2PY): Success! {res}")
        except Exception as e:
            print("%d: CMD (TXT2PY): Failed! -> %s"%(testNr,str(e)))
            failedTxt2PyTests.append(testNr)
        print("%d: CMD (PY): %s" % (testNr, pySerializer.Ser(cmd)))
        print("%d: CMD (JS): %s" % (testNr, jsSerializer.Ser(cmd)))
        cmd2 = jsonSerializer.Des(jsonCmd)
        #execute the command on the domain
        try:
            val = domain.Do(cmd2)
            #print("Result: %s"%(val))
            print("Result: %s"%(textSerializer.Ser(val)))
        except Exception as e:
            print("Command failed: %s"%(str(e)))

        # print the execution time
        end = timer()
        print("Command time: %f s"%(end-start))
        print("") #newline

        testNr += 1

    # print statistics on text parsing
    print(f"Txt2Py failed on {len(failedTxt2PyTests)} tests, explicitly on {failedTxt2PyTests}")
