'''
PyEOQ Example: SessionsAndEvents.py
----------------------------------------------------------------------------------------
This example shows how to initialize, use and close a session. Moreover, it shows how 
specific events can be observed and unobserved in relation to that session.

See EOQ User Manual for more information: https://gitlab.com/eoq/doc

2020 Bjoern Annighoefer
'''

from eoq2.mdb.pyecore import PyEcoreWorkspaceMdbProvider,PyEcoreMdbAccessor,PyEcoreIdCodec
from eoq2.domain.local import LocalMdbDomain

from eoq2.action.externalpy import ExternalPyScriptHandler
from eoq2 import Get,Set,Add,Rem,Mov,Clo,Crt,Crn,Sts,Chg,Gaa,Cal,Asc,Abc,Cmp,CloModes,Hel,Ses,Gby,Obs,Ubs
from eoq2 import Qry,Obj,His,Cls,Ino,Met,Idx,Pth,Arr,Equ,Eqa,Neq,Les,Gre
from eoq2.event import EvtTypes

import sys

'''
OnEvent (EventCallback):
This function is registered below as an event handler. All event types can be handled by this generic event 
handler and are printed to the screen according to the event type. By default no event will appear here, since 
events are only forwarded by the domain after appropriate observe OBS and unobserve commands UBS. 
'''
def OnEvent(evts,source):
    # mask the print function because action to prevent that the stdout capturing of 
    # actions calls captures the event printout as well.
    def print(data):
        sys.stderr.write(data)
    
    for evt in evts:
        if(evt.evt == EvtTypes.CST):
            print("EVT(%s): Status of call %d changed to %s"%(evt.k,evt.a[0],evt.a[1]))
        elif(evt.evt == EvtTypes.OUP):
            print("EVT(%s): Output of call %d on channel %s: %s"%(evt.k,evt.a[0],evt.a[1],evt.a[2]))
        elif(evt.evt == EvtTypes.CVA):
            print("EVT(%s): Result of call %d: %s"%(evt.k,evt.a[0],evt.a[1]))
        elif(evt.evt == EvtTypes.CHG):
            print("EVT(%s): Change (%d): %s t: %s, f: %s, n: %s, [was: v:%s, o: %s, f:%s, i:%s] (tid:%d)"%(evt.k,evt.a[0],evt.a[1],evt.a[2],evt.a[3],evt.a[4],evt.a[5],evt.a[6],evt.a[7],evt.a[8],evt.a[9]))
        elif(evt.evt == EvtTypes.MSG):
            print("EVT(%s): Message: %s"%(evt.k,evt.a))
        
'''
MAIN: Execution starts here
'''          
if __name__ == '__main__':
    #Initialize an WorkspaceMDB
    mdbProvider = PyEcoreWorkspaceMdbProvider('Workspace',metaDir=['./Meta']) #./Meta must contain oaam.ecore
    valueCodec = PyEcoreIdCodec() #create the default value codec that converts 
    mdbAccessor = PyEcoreMdbAccessor(mdbProvider.GetMdb(),valueCodec)
    domain = LocalMdbDomain(mdbAccessor)
    mdbProvider.CoupleWithDomain(domain, valueCodec)
    #Register external actions
    externalActionHandler = ExternalPyScriptHandler(domain.cmdRunner.callManager,'Actions')
    #listen to events
    predefineSessionId = 'my-new-session' #will take care later to make sure that this will be the session id
    domain.Observe(OnEvent,sessionId=predefineSessionId)
    
    #initialize a session to gain fine control on events
    print("Opening session")
    sessionId = domain.Do(Hel('user','pw'),sessionId=predefineSessionId) #session id is optional
    print('My session ID: %s'%(sessionId))
    #PART 1: Get all events
    #listen to all events on this session
    domain.Do(Cmp()
                  .Ses(sessionId)
                  .Obs('*','*'))
    #create an object and set properties to force change events
    print("1.1: Creating CHG events and listen...")
    domain.Do(Cmp().Crn('http://www.oaam.de/oaam/model/v140/functions','Task',1)
                   .Crn('http://www.oaam.de/oaam/model/v140/library','TaskType',1)
                   .Set(His(0),['name','type'],Arr(['New Task',His(1)])))
    
    print("1.2: Creating OUP, CST,and CVA events and listen...")
    domain.Do(Asc('Misc/helloworld',[1]))
    domain.Do(Cal('Misc/sleep',[1])) #wait a second to make the async output appear
    
    #PART 2: Get no events
    #stop listening to all commands
    domain.Do(Ubs('*','*'),sessionId=sessionId) #the optional sessionId arg can be used instead of the SES command
    #do the same thing, but this time no events should be issued
    print("2.1: Creating CHG events, but this time stay silent...")
    domain.Do(Cmp().Crn('http://www.oaam.de/oaam/model/v140/functions','Task',1)
                   .Crn('http://www.oaam.de/oaam/model/v140/library','TaskType',1)
                   .Set(His(0),['name','type'],Arr(['New Task',His(1)])))
    print("2.2: Creating OUP, CST, and CVA, but this time stay silent...")
    domain.Do(Asc('Misc/helloworld',[1]))
    domain.Do(Cal('Misc/sleep',[1])) #wait a second to make the async output appear
    
    #PART 3: Listen only to CHG events
    #stop listening to all commands
    domain.Do(Obs('CHG','*'),sessionId=sessionId) #the optional sessionId arg can be used instead of the SES command
    #do the same thing, but this time no events should be issued
    print("3.1: Creating CHG events and listen to CHG only...")
    domain.Do(Cmp().Crn('http://www.oaam.de/oaam/model/v140/functions','Task',1)
                   .Crn('http://www.oaam.de/oaam/model/v140/library','TaskType',1)
                   .Set(His(0),['name','type'],Arr(['New Task',His(1)])))
    print("3.1: Creating OUP, CST, and CVA, but stay silent...")
    domain.Do(Asc('Misc/helloworld',[1]))
    domain.Do(Cal('Misc/sleep',[1])) #wait a second to make the async output appear
    
    #PART 4: Listen only to only the OUP events of a specific call
    #TODO: part 4 works in principle, but only if putting a break point on the output event. If no breakpoint is present, the event seems to be postponed until the end of the program.
    #stop listening to all commands
    domain.Do(Ubs('*','*'),sessionId=sessionId)
    #do the same thing, but this time no events should be issued
    print("4.1: Creating CHG events, but stay silent...")
    domain.Do(Cmp().Crn('http://www.oaam.de/oaam/model/v140/functions','Task',1)
                   .Crn('http://www.oaam.de/oaam/model/v140/library','TaskType',1)
                   .Set(His(0),['name','type'],Arr(['New Task',His(1)])))
    print("4.2: Creating OUP, CST, and CVA, but listen only to the OUP events of the async call")
    res = domain.Do(Cmp().Ses(sessionId) #could also use sessionId arg
                   .Asc('Misc/helloworld',[1])
                   .Obs('OUP',His(-1))) #immediately register the call id to listen to.
    domain.Do(Cal('Misc/sleep',[1])) #wait a second to make the async output appear
    #the async task should be finished here, so stop listening to it
    callId = res[1]
    domain.Do(Ubs('OUP',callId),sessionId=sessionId)
    
    #PART 5: Get no events again
    print("5.1: Creating CHG events, but nothing should be shown...")
    domain.Do(Cmp().Crn('http://www.oaam.de/oaam/model/v140/functions','Task',1)
                   .Crn('http://www.oaam.de/oaam/model/v140/library','TaskType',1)
                   .Set(His(0),['name','type'],Arr(['New Task',His(1)])))
    print("5.2: Creating OUP, CST, and CVA, but nothing should be shown...")
    domain.Do(Asc('Misc/helloworld',[1]))
    domain.Do(Cal('Misc/sleep',[1])) #wait a second to make the async output appear
    
    #END
    #end session
    domain.Do(Gby(sessionId))
    print("Session closed")
    
    
