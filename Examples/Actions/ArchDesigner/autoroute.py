__tags__ = ['archdesigner','autoroute'] #classification tags to be used in clients

from eoq2 import Get,Set,Add,Rem,Mov,Clo,Crt,Crn,Gmm,Rmm,Umm,Sts,Chg,Gaa,Cal,Asc,Abc,Cmp,CloModes
from eoq2 import Qry,Obj,His,Cls,Ino,Met,Idx,Pth,Arr,Sel
from eoq2.serialization import JsonSerializer,TextSerializer,PySerializer,JsSerializer

from dijkstar import Graph, find_path

def autoroute(domain,root : 'ArchModel', signals : 'Signal[1..*]')->'Route[1..*]':
    ADNSURI = 'ADPACK'
    #use implementation from https://pypi.org/project/Dijkstar/ to calculate the paths
    devices = Do(domain,Get(Qry(root).Pth('devices')))
    connections = Do(domain,Get(Qry(root).Pth('connections')))
    graph = Graph()
    cn = 0
    connectionCosts = []
    for c in connections: 
        cInfo = Do(domain,Get(Qry(c).Arr([Pth('start'),Pth('end'),Pth('length')])))
        graph.add_edge(devices.index(cInfo[0]), devices.index(cInfo[1]), cn)
        connectionCosts.append(cInfo[2]) #will be used later in the cost function
        cn = cn+1
    #find the paths
    routes = []
    for s in signals:
        [sName,sSrc,sDst] =  Do(domain,Get(Qry(s).Arr([Pth('name').Trm(None,'NONAME'),Pth('src'),Pth('dst')])))
        [sDev,tDev] = Do(domain,Get(Qry(root).Pth('taskAssigns').Arr([Sel(Pth('task').Equ(sSrc)).Idx(0),Sel(Pth('task').Equ(sDst)).Idx(0)]).Pth('device')))
        [sDevName,tDevName] = Do(domain,Get(Arr([Qry(sDev).Pth('name').Trm(None,'DEVICE'),Qry(tDev).Pth('name').Trm(None,'DEVICE')])))
        print("Calculating route for signal: %s: %s -> %s"%(sName,sDevName,tDevName))
        try:
            path = find_path(graph, devices.index(sDev), devices.index(tDev),cost_func=lambda u, v, e, prev_e: connectionCosts[e])
            print(graph)
            print(path.nodes)
            print(path.edges)
            elemPath = []
            segmentRefs = []
            for i in range(len(path.nodes)):
                elemPath.append(devices[path.nodes[i]])
                segmentRefs.append([devices[path.nodes[i]],None])
                if i < len(path.edges): 
                    elemPath.append(connections[path.edges[i]])
                    segmentRefs.append([None,connections[path.edges[i]]])
            pathNames = Do(domain,Get(Qry(Arr(elemPath)).Pth('name')))
            print("Route: %s (length: %d)"%('->'.join(pathNames),path.total_cost))
            #create the route object
            route = Do(domain,Cmp().Crn(ADNSURI,'Route',1)
              .Add(root,'routes',His(-1))
              .Set(His(-2),'signal',s)
              .Crn(ADNSURI,'Segment',2*len(path.nodes)-1)
              .Add(His(-4),'segments',His(-1))
              .Set(His(-2),['device','connection'],segmentRefs))[0]
            routes.append(route)
        except:
            print('No path found!')
    return routes
    

def Do(domain,cmd):
#     serializer = TextSerializer()
#     print(">> %s"%(serializer.Ser(cmd)))
    res = domain.Do(cmd)
#     print("%s"%serializer.Ser(res))
#     print("") #newline
    return res
