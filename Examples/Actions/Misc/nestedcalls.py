'''
    An action demonstrating that an action can call other actions
'''

from eoq2 import Cal,Obs,His,Cmp,Ubs
from eoq2.event import EvtTypes
from eoq2.action.util import AscAndWait
from eoq2.util.error import EoqCallError,EoqCallAborted

def nestedcalls(domain : 'Domain'):
    print("Calling progress a first time for 5 seconds.")
    res = AscAndWait(domain,'Misc/progress',[0.5,10],forwardOutput=True)
    print("Calling progress a first time finished: res=%s"%(res))
    
    print("Calling a subaction that failes.")
    try:
        (res) = AscAndWait(domain,'Misc/failedaction',[],forwardOutput=True)
        print("Calling progress a second time finished: res=%s"%(res))
    except EoqCallError as e:
        print("Call failed: %s"%(e.msg))
    except EoqCallAborted as e:
        print("Call aborted: %s"%(e.msg))
      
    print("Calling progress a second time for 2 seconds.")
    res = AscAndWait(domain,'Misc/progress',[0.2,10],forwardOutput=True)
    print("Calling progress a second time finished: res=%s"%(res))
    
            
        
    
    