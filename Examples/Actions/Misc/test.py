'''
    An action which has the only purpose to test argument parsing
'''


def test(domain : 'Domain', 
         arg1 : 'Integer', 
         arg2 : 'String[1..2]{ALL,SOME,NONE}=ALL:This is the description for arg2',
         arg3 : 'Real=3.24:This is the description for arg2',
         arg4 : 'Char[*]',
         arg5 : 'String=undefined:This is a String with a default value',
         arg6 : 'Integer{1,2,3,4}') -> 'Nothing is returned':
    print("Test action executed!")
    return
