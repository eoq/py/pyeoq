'''
    A simple action to test the action server. 
    Retrieves the target object, looks if it has 
    a name and than renames it.
'''

from eoq2 import Get,Set
from eoq2 import Cls,Arr


def arraytest(domain : 'Domain')->'None':
    tasks = domain.Do(Get(Cls('Task')))
    names = domain.Do(Get(Arr(tasks).Pth('name')))
    print(names)
    newNames = ["newName%d"%(i) for i in range(len(names))]
    print(newNames)
    #native style 
    domain.Do(Set(tasks,'name',newNames)) #critical point
    domain.Do(Set(tasks,'name',names)) #reset names
    #array style
    domain.Do(Set(Arr(tasks),'name',newNames)) #critical point
    domain.Do(Set(Arr(tasks),'name',Arr(names))) #reset names
    return
