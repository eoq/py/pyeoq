import time

def abortTest(domain : 'Domain', seconds : 'Real=1.0:The sleep time in seconds'):
    print('Sleep start.')
    time.sleep(seconds)
    print('Sleep end.')
    return