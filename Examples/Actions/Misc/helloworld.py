__tags__ = ['misc','test'] #classification tags to be used in clients

def helloworld(domain : 'Domain', times : 'Integer=2')->'String':
    for i in range(times):
        print("Hello world!")
    return 'I printed %d times Hello world!'%(times)
