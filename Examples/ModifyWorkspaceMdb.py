'''
PyEOQ Example: ModifyWorkspaceMdb.py
----------------------------------------------------------------------------------------
This example shows how to use EOQ to modify the structure of the workspace MDB itself, i.e.
work with the Workspace Mdb Model. It shows how to create, move, and remove model 
resources and directories. Denote that all modifications will also result in files
system changes in the workspace directory.

See EOQ User Manual for more information: https://gitlab.com/eoq/doc

2020 Bjoern Annighoefer
'''

from eoq2.mdb.pyecore import PyEcoreWorkspaceMdbProvider,PyEcoreMdbAccessor,PyEcoreIdCodec
from eoq2.domain.local import LocalMdbDomain

from eoq2.action.externalpy import ExternalPyScriptHandler
from eoq2 import Get,Set,Add,Rem,Mov,Clo,Crt,Crn,Sts,Chg,Gaa,Cal,Asc,Abc,Cmp,CloModes
from eoq2 import Qry,Obj,His,Cls,Ino,Met,Idx,Pth,Arr
from eoq2.serialization import JsonSerializer,TextSerializer,PySerializer,JsSerializer
from eoq2.event import EvtTypes

from eoq2.util import Backupper,NoLogging,ConsoleLogger,ConsoleAndFileLogger,LogLevels,DEFAULT_LOG_LEVELS

from eoq2.util.model import CreateNewModelResource,CloneModelResource,GetName

from timeit import default_timer as timer
import sys 

'''
OnEvent (EventCallback):
This function is registered below as an event handler. All event types can be handled by this generic event 
handler and are printed to the screen according to the event type. 
'''
def OnEvent(evts,source):
    # mask the print function because action to prevent that the stdout capturing of 
    # actions calls captures the event printout as well.
    def print(data):
        sys.stderr.write(data)
    
    for evt in evts:
        if(evt.evt == EvtTypes.CST):
            print("EVT: Status of call %d changed to %s"%(evt.a[0],evt.a[1]))
        elif(evt.evt == EvtTypes.OUP):
            print("EVT: Output of call %d on channel %s: %s"%(evt.a[0],evt.a[1],evt.a[2]))
        elif(evt.evt == EvtTypes.CVA):
            print("EVT: Result of call %d: %s"%(evt.a[0],evt.a[1]))
        elif(evt.evt == EvtTypes.CHG):
            print("EVT: Change (%d): %s t: %s, f: %s, n: %s, [was: v:%s, o: %s, f:%s, i:%s] (tid:%d)"%(evt.a[0],evt.a[1],evt.a[2],evt.a[3],evt.a[4],evt.a[5],evt.a[6],evt.a[7],evt.a[8],evt.a[9]))
        elif(evt.evt == EvtTypes.MSG):
            print("EVT: Message: %s"%(evt.a))
        
'''
MAIN: Execution starts here
'''            
if __name__ == '__main__':
    #Basic configuration
    workspaceDir = 'Workspace'
    logDir = './log'
    modelfile = "MinimalFlightControl.oaam"
    
    #create a backup. This is only for testing purposes and not necessary (both lines needed)
    #backupper = Backupper([workspaceDir,logDir])
    #backupper.CreateBackup()
    
    #define loglevels (chose one of the following lines)
    #logLevels = DEFAULT_LOG_LEVELS
    logLevels = DEFAULT_LOG_LEVELS+["change","transaction","event"]
    #logLevels = DEFAULT_LOG_LEVELS+[LogLevels.DEBUG,"change","transaction","event"]
    
    #initialize logger. For the file based logger this must happen after the backup. (chose one of the following lines)
    #logger = NoLogging() #no output at all
    #logger = ConsoleLogger() #only console output
    logger = ConsoleAndFileLogger(logDir=logDir,activeLevels=logLevels) #console and file output
          
    #Create a model data base (MDB) (chose one of the following lines)
    #mdb = PyEcoreSingleFileMdb("workspace/MinimalFlightControl.oaam","workspace/.meta/oaam.ecore",saveTimeout=1.0,logger=logger)
    mdbProvider = PyEcoreWorkspaceMdbProvider(workspaceDir,metaDir=['./Meta'],saveTimeout=1.0,logger=logger)
    
    #Create an encoding strategy for model based data (chose one of the following lines)
    #valueCodec = SimpleEObjectCodec()
    valueCodec = PyEcoreIdCodec()
    
    #Create an unique accessor to the data
    mdbAccessor = PyEcoreMdbAccessor(mdbProvider.GetMdb(),valueCodec)
    
    #Create a domain and couple it with the mdb provider
    domain = LocalMdbDomain(mdbAccessor,logger=logger)
    mdbProvider.CoupleWithDomain(domain, valueCodec)
    
    
    #Listen to all events of the domain (optional)
    domain.Observe(OnEvent)
    
    #Register external actions (optional)
    externalActionHandler = ExternalPyScriptHandler(domain.cmdRunner.callManager,'Actions',logger=logger)
    
    #prepare serializers
    serializer = JsonSerializer()
    serializer2 = TextSerializer()
    serializer3 = PySerializer()
    serializer4 = JsSerializer()
    
    #Retrieve the model resource for the file we would like to work on. (Single file and workspace mdb have different solutions)
    #root = mdbAccessor.GetRoot(valueCodec) #single file mdb
    cmd = Get(Pth('resources').Sel(Pth('name').Equ(modelfile)).Idx(0).Pth('contents').Idx(0)) #workspace mdb
    jsonCmd = serializer.Ser(cmd) #workspace mdb
    print(jsonCmd) #workspace mdb
    root = domain.Do(cmd) #workspace mdb    
    
    #Define a list of demo commands
    cmds = [
            #test resource creation, move and delete
            #create a new resource 
            Cmp().Crn('http://www.eoq.de/workspacemdbmodel/v1.0','ModelResource',1)
                 .Set(His(0),'name','NewFile.oaam')
                 .Add(Obj(0),'resources',His(0)), #add the new resource
             
            #rename the resource
            Set(Obj(0).Pth('resources').Sel(Pth('name').Equ('NewFile.oaam')).Idx(0),'name','OldFile.oaam'),
             
            #move the resource 
            Add(Obj(0).Pth('subdirectories').Sel(Pth('name').Equ('Subdir')).Idx(0),'resources',Obj(0).Pth('resources').Sel(Pth('name').Equ('OldFile.oaam')).Idx(0)),
             
            #add contents to the file
            Cmp().Crn('http://www.oaam.de/oaam/model/v140','Architecture',1)
                 .Set(His(0),'name','MyArchitecture')
                 .Add(Obj(0).Pth('subdirectories').Sel(Pth('name').Equ('Subdir')).Idx(0).Pth('resources').Sel(Pth('name').Equ('OldFile.oaam')).Idx(0),'contents',His(0)),
                  
            #wait until the file is saved
            Cal('Misc/sleep',[2]), #wait some time
             
            #delete the resource
            Rem(Obj(0).Pth('subdirectories').Sel(Pth('name').Equ('Subdir')).Idx(0),'resources',Obj(0).Pth('subdirectories').Sel(Pth('name').Equ('Subdir')).Idx(0).Pth('resources').Sel(Pth('name').Equ('OldFile.oaam')).Idx(0)),
             
            #test directory functions
            #create new directory
            Cmp().Crn('http://www.eoq.de/workspacemdbmodel/v1.0','Directory',1)
                 .Set(His(0),'name','TmpSubdir')
                 .Add(Obj(0),'subdirectories',His(0)), #add the new dir
                  
            #create two new resources
            Cmp().Crn('http://www.eoq.de/workspacemdbmodel/v1.0','ModelResource',1)
                 .Set(His(-1),'name','Res1.oaam')
                 .Add(Obj(0),'resources',His(-2)) #add the new resource
                 .Crn('http://www.eoq.de/workspacemdbmodel/v1.0','ModelResource',1)
                 .Set(His(-1),'name','Res2.oaam')
                 .Add(Obj(0).Pth('subdirectories').Sel(Pth('name').Equ('TmpSubdir')).Idx(0),'resources',His(-2)), #add the new resource
                  
            #wait until the files are saved
            Cal('Misc/sleep',[2]), #wait some time
             
            #move the directory
            Add(Obj(0).Pth('subdirectories').Sel(Pth('name').Equ('Subdir')).Idx(0),'subdirectories',Obj(0).Pth('subdirectories').Sel(Pth('name').Equ('TmpSubdir')).Idx(0)),
             
            #rename the directory
            Set(Obj(0).Pth('subdirectories').Sel(Pth('name').Equ('Subdir')).Idx(0).Pth('subdirectories').Sel(Pth('name').Equ('TmpSubdir')).Idx(0),'name','DirToDelete'),
             
            #move second resource to new dir
            Add(Obj(0).Pth('subdirectories').Sel(Pth('name').Equ('Subdir')).Idx(0).Pth('subdirectories').Sel(Pth('name').Equ('DirToDelete')).Idx(0),'resources',Obj(0).Pth('resources').Sel(Pth('name').Equ('Res1.oaam')).Idx(0)),
             
            #move the dir back
            Add(Obj(0),'subdirectories',Obj(0).Pth('subdirectories').Sel(Pth('name').Equ('Subdir')).Idx(0).Pth('subdirectories').Sel(Pth('name').Equ('DirToDelete')).Idx(0)),
             
            #delete new directory
            Rem(Obj(0),'subdirectories',Obj(0).Pth('subdirectories').Sel(Pth('name').Equ('DirToDelete')).Idx(0)),
 
            # Check cross references
            # let one file reference the other
            Cmp().Get(Obj(0).Pth('subdirectories').Sel(Pth('name').Equ('Subdir')).Idx(0).Pth('resources').Sel(Pth('name').Equ('CrossRef1.oaam')).Idx(0))
                 .Get(Obj(0).Pth('subdirectories').Sel(Pth('name').Equ('Subdir2')).Idx(0).Pth('resources').Sel(Pth('name').Equ('CrossRef2.oaam')).Idx(0))
                 .Add(His(0).Pth('contents').Idx(0),'include',His(1).Pth('contents').Idx(0)),
           ]
    
    
    #Run an commands sequentially and print their output
    testNr = 1
    for cmd in cmds:
        start = timer()
        jsonCmd = serializer.Ser(cmd)
        print("%d: CMD (JSON): %s"%(testNr,jsonCmd))
        print("%d: CMD (TXT): %s"%(testNr,serializer2.Ser(cmd)))
        print("%d: CMD (PY): %s"%(testNr,serializer3.Ser(cmd)))
        print("%d: CMD (JS): %s"%(testNr,serializer4.Ser(cmd)))
        cmd2 = serializer.Des(jsonCmd)
        try:
            val = domain.Do(cmd2)
            print("Result: %s"%(val))
        except Exception as e:
            print("Command failed: %s"%(str(e)))
        
        end = timer()
        
        print(""); #newline
        
        testNr += 1
        