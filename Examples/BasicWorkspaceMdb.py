'''
PyEOQ Example: BasicWorkspaceMdb.py
----------------------------------------------------------------------------------------
This example shows how to initialize a local workspace MDB, link it to a domain, and use
the domain to issue queries. 

See EOQ User Manual for more information: https://gitlab.com/eoq/doc

2020 Bjoern Annighoefer
'''

from eoq2.mdb.pyecore import PyEcoreWorkspaceMdbProvider,PyEcoreMdbAccessor,PyEcoreIdCodec
from eoq2.domain.local import LocalMdbDomain

from eoq2 import Get #import GET command
from eoq2 import Qry,Cls,Pth,Arr #import query segments
      
'''
MAIN: Execution starts here
'''            
if __name__ == '__main__':
    # Initialize an WorkspaceMDB
    mdbProvider = PyEcoreWorkspaceMdbProvider('Workspace',metaDir=['./Meta']) #./Meta must contain oaam.ecore
    valueCodec = PyEcoreIdCodec() #create the default value codec that converts objects to ids
    mdbAccessor = PyEcoreMdbAccessor(mdbProvider.GetMdb(),valueCodec)
    domain = LocalMdbDomain(mdbAccessor)
    mdbProvider.CoupleWithDomain(domain, valueCodec)
    
    # Get all elements of class task contained in the workspace
    allTasks = domain.Do( Get(Cls('Task')) ) 
    print("These are all tasks in the workspace: %s"%(allTasks))
    
    # Get the name property of all tasks
    allTaskNames = domain.Do( Get(Cls('Task').Pth('name')) ) 
    print("These tasks are named': %s"%(allTaskNames))
    
    # Get the name property of all tasks by using intermediate result
    allTaskNames = domain.Do( Get(Arr(allTasks).Pth('name')) ) 
    print("These tasks are named (second time)': %s"%(allTaskNames))
    
    # Find the root object of the resource with the name 'MinimalFlightControl.oaam'
    root = domain.Do( Get(Pth('resources').Sel(Pth('name').Equ("MinimalFlightControl.oaam")).Idx(0).Pth('contents').Idx(0)) ) 
    print("The root object of 'MinimalFlightControl.oaam' is %s"%(root))
    
    # Get all elements of class task contained in the models root
    allTasksInRoot = domain.Do( Get(Qry(root).Cls('Task')) ) 
    print("These are all tasks in 'MinimalFlightControl.oaam': %s"%(allTasksInRoot))
    