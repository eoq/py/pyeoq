'''
PyEOQ Example: InteractiveEoqConsole.py
----------------------------------------------------------------------------------------
This example opens an EOQ domain and provides interactive access to it via the console.
The user can input textual EOQ commands manually and view the results. 

See EOQ User Manual for more information: https://gitlab.com/eoq/doc

2020 Bjoern Annighoefer
'''

from timeit import default_timer as timer #used to time the command's execution time.
import sys

from prompt_toolkit import prompt
from prompt_toolkit.history import FileHistory
from prompt_toolkit.auto_suggest import AutoSuggestFromHistory
from prompt_toolkit.completion import WordCompleter
from pip._vendor.requests.sessions import session

try:
    import eoq2
except ModuleNotFoundError:
    sys.path.append('../')
    import eoq2
    

from eoq2.mdb.pyecore import PyEcoreSingleFileMdbProvider,PyEcoreWorkspaceMdbProvider,PyEcoreMdbAccessor,PyEcoreIdCodec
from eoq2.domain.local import LocalMdbDomain

from eoq2.action.externalpy import ExternalPyScriptHandler
from eoq2 import Hel,Obs
from eoq2.serialization import JsonSerializer,TextSerializer,PySerializer,JsSerializer
from eoq2.event import EvtTypes

from eoq2.util import Backupper,NoLogging,ConsoleLogger,ConsoleAndFileLogger,LogLevels,DEFAULT_LOG_LEVELS



'''
OnEvent (EventCallback):
This function is registered below as an event handler for EOQ transactions. Transactions cause 
events when modifying model elements. All event types are handled by this basic event handler and 
printed to the screen according to the event type. By default not all events will be caught, but only 
those after the event observe command OBS and the unobserve command UBS. See the list of commands
for a better understanding.
'''
def OnEvent(evts,source):
    # mask the print function because action to prevent that the stdout capturing of 
    # actions calls captures the event printout as well.
    def print(data):
        sys.stderr.write('\n')
        sys.stderr.write(data)
        sys.stderr.write('\n>> ')
        
    for evt in evts:
        if(evt.evt == EvtTypes.CST):
            print("EVT(%s): Status of call %d changed to %s"%(evt.k,evt.a[0],evt.a[1]))
        elif(evt.evt == EvtTypes.OUP):
            print("EVT(%s): Output of call %d on channel %s: %s"%(evt.k,evt.a[0],evt.a[1],evt.a[2]))
        elif(evt.evt == EvtTypes.CVA):
            print("EVT(%s): Result of call %d: %s"%(evt.k,evt.a[0],evt.a[1]))
        elif(evt.evt == EvtTypes.CHG):
            print("EVT(%s): Change (%d): %s t: %s, f: %s, n: %s, [was: v:%s, o: %s, f:%s, i:%s] (tid:%d)"%(evt.k,evt.a[0],evt.a[1],evt.a[2],evt.a[3],evt.a[4],evt.a[5],evt.a[6],evt.a[7],evt.a[8],evt.a[9]))
        elif(evt.evt == EvtTypes.MSG):
            print("EVT(%s): Message: %s"%(evt.k,evt.a))
        
'''
MAIN: Execution starts here
'''          
if __name__ == '__main__':
    #Basic configuration
    WORKSPACE_MDB = True
    WORKSPACE_DIR = 'Workspace'
    LOG_DIR = './log'
    MODELFILE = "MinimalFlightControl.oaam"

    #create a backup. This is only for testing purposes and not necessary (both lines needed)
    #backupper = Backupper([WORKSPACE_DIR,LOG_DIR])
    #backupper.CreateBackup()

    #define loglevels (chose one of the following lines)
    #logLevels = DEFAULT_LOG_LEVELS
    #logLevels = DEFAULT_LOG_LEVELS+["change","transaction","event"]
    #logLevels = DEFAULT_LOG_LEVELS+[LogLevels.DEBUG,"change","transaction","event"]

    #initialize logger. For the file based logger this must happen after the backup. (chose one of the following lines)
    logger = NoLogging() #no output at all
    #logger = ConsoleLogger() #only console output
    #logger = ConsoleAndFileLogger(logDir=LOG_DIR,activeLevels=logLevels) #console and file output

    #Create a model data base (MDB) (chose one of the following lines)
    mdbProvider = None
    if(WORKSPACE_MDB):
        mdbProvider = PyEcoreWorkspaceMdbProvider(WORKSPACE_DIR,metaDir=['./Meta'],saveTimeout=1.0,logger=logger,trackFileChanges=True)
    else:
        mdbProvider = PyEcoreSingleFileMdbProvider(WORKSPACE_DIR+"/"+MODELFILE,WORKSPACE_DIR+"/Meta/oaam.ecore",saveTimeout=1.0,logger=logger)
        

    #Create an encoding strategy for model based data 
    valueCodec = PyEcoreIdCodec()

    #Create an unique accessor to the data
    mdbAccessor = PyEcoreMdbAccessor(mdbProvider.GetMdb(),valueCodec)

    #Create a domain and couple it with the mdb provider
    domain = LocalMdbDomain(mdbAccessor,logger=logger)
    mdbProvider.CoupleWithDomain(domain, valueCodec)
    
    #Initialize a new session
    print("Opening session")
    sessionId = domain.Do(Hel('user','pw'))
    
    #Initialize the event listener (specific events must be enabled afterwards).
    print('Session ID is: %s'%(sessionId))
    domain.Observe(OnEvent,sessionId=sessionId)
    
    #get all events
    domain.Do(Obs('*','*'),sessionId)
    
    #Register external actions (optional)
    #externalActionHandler = ExternalPyScriptHandler(domain.cmdRunner.callManager,'../Examples/workspace/actions',logger=logger)

    #prepare serializer
    textSerializer = TextSerializer()
    
    EOQCompleter = WordCompleter(['quit','GET', 'SET', 'MOV', 'CMP', 'REM', 'MET'],
                             ignore_case=False)

    ##main loop
    print('*********************************************************************************')
    print('* Interactive EOQ console                                                       *')
    print('*                                                                               *')
    print('* Version: %s                                                                *'%(eoq2.__version__))
    print('* Usage:                                                                        *')
    print('*   Enter textual EOQ commands and press enter to have them processed.          *')
    print('*   Type \'quit\' to quit.                                                        *')
    print('*********************************************************************************')
    isRunning = True
    usePromtToolkit = True
    while isRunning:
        textCmd = ''
        if usePromtToolkit:
            try:
                textCmd = prompt('>>',
                            history=FileHistory('InteractiveEoqConsoleHistory.txt'),
                            auto_suggest=AutoSuggestFromHistory(),
                            completer=EOQCompleter,
                            )
            except:
                usePromtToolkit = False
                textCmd = input('>> ')
        else:
            textCmd = input('>> ')
        
        if(textCmd == 'quit'): 
            isRunning = False
            print('Goodbye')
        else:
            try:
                cmd = textSerializer.Des(textCmd)
                res = domain.Do(cmd)
                textRes = textSerializer.Ser(res)
                print('>> %s'%(textRes))
            except Exception as e:
                print('!! %s',str(e))
                print('\n>> ')
    
    print('Interactive console closed.')