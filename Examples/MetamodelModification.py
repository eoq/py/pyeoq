'''
PyEOQ Example: MetamodelModication.py
----------------------------------------------------------------------------------------
This example shows access the packages and classes of the meta-model(s) used by the domain. 
In addition it gives an example how a new meta-model can be created and used programmatically
during runtime.

See EOQ User Manual for more information: https://gitlab.com/eoq/doc

2020 Bjoern Annighoefer
'''
from eoq2.mdb.pyecore import PyEcoreWorkspaceMdbProvider,PyEcoreMdbAccessor,PyEcoreIdCodec
from eoq2.domain.local import LocalMdbDomain

from eoq2.action.externalpy import ExternalPyScriptHandler
from eoq2 import Get,Set,Add,Rem,Mov,Clo,Crt,Crn,Gmm,Rmm,Umm,Sts,Chg,Gaa,Cal,Asc,Abc,Cmp,CloModes
from eoq2 import Qry,Obj,His,Cls,Ino,Met,Idx,Pth,Arr
from eoq2.serialization import JsonSerializer,TextSerializer,PySerializer,JsSerializer
from eoq2.event import EvtTypes

from eoq2.util import Backupper,NoLogging,ConsoleLogger,ConsoleAndFileLogger,LogLevels,DEFAULT_LOG_LEVELS

from timeit import default_timer as timer 

'''
OnEvent (EventCallback):
This function is registered below as an event handler. All event types can be handled by this generic event 
handler and are printed to the screen according to the event type. 
'''
def OnEvent(evts,source):
    for evt in evts:
        if(evt.evt == EvtTypes.CST):
            print("EVT: Status of call %d changed to %s"%(evt.a[0],evt.a[1]))
        elif(evt.evt == EvtTypes.OUP):
            print("EVT: Output of call %d on channel %s: %s"%(evt.a[0],evt.a[1],evt.a[2]))
        elif(evt.evt == EvtTypes.CVA):
            print("EVT: Result of call %d: %s"%(evt.a[0],evt.a[1]))
        elif(evt.evt == EvtTypes.CHG):
            print("EVT: Change (%d): %s t: %s, f: %s, n: %s, [was: v:%s, o: %s, f:%s, i:%s] (tid:%d)"%(evt.a[0],evt.a[1],evt.a[2],evt.a[3],evt.a[4],evt.a[5],evt.a[6],evt.a[7],evt.a[8],evt.a[9]))
        elif(evt.evt == EvtTypes.MSG):
            print("EVT: Message: %s"%(evt.a))
        
'''
MAIN: Execution starts here
'''          
if __name__ == '__main__':
    #Basic configuration
    workspaceDir = 'Workspace'
    logDir = './log'
    
    #define loglevels (chose one of the following lines)
    #logLevels = DEFAULT_LOG_LEVELS
    logLevels = DEFAULT_LOG_LEVELS+["change","transaction","event"]
    #logLevels = DEFAULT_LOG_LEVELS+[LogLevels.DEBUG,"change","transaction","event"]
    
    #initialize logger. For the file based logger this must happen after the backup. (chose one of the following lines)
    #logger = NoLogging() #no output at all
    #logger = ConsoleLogger() #only console output
    logger = ConsoleAndFileLogger(logDir=logDir,activeLevels=logLevels) #console and file output
          
    #Create a model data base (MDB) (chose one of the following lines)
    #mdb = PyEcoreSingleFileMdb("workspace/MinimalFlightControl.oaam","workspace/.meta/oaam.ecore",saveTimeout=1.0,logger=logger)
    mdbProvider = PyEcoreWorkspaceMdbProvider(workspaceDir,metaDir=['./Meta'],saveTimeout=1.0,logger=logger)
    
    #Create an encoding strategy for model based data (chose one of the following lines)
    #valueCodec = SimpleEObjectCodec()
    valueCodec = PyEcoreIdCodec()
    
    #Create an unique accessor to the data
    mdbAccessor = PyEcoreMdbAccessor(mdbProvider.GetMdb(),valueCodec)
    
    #Create a domain and couple it with the mdb provider
    domain = LocalMdbDomain(mdbAccessor,logger=logger)
    mdbProvider.CoupleWithDomain(domain, valueCodec)
    
    
    #Listen to all events of the domain (optional)
    domain.Observe(OnEvent)
    
    #Register external actions (optional)
    externalActionHandler = ExternalPyScriptHandler(domain.cmdRunner.callManager,'Actions',logger=logger)
    
    #prepare serializers
    serializer = JsonSerializer()
    serializer2 = TextSerializer()
    serializer3 = PySerializer()
    serializer4 = JsSerializer()    
    
    #Define a list of demo commands
    cmds = [
            #inspect meta models
            Gmm(),
            #get name and URI of all meta models
            Cmp().Gmm()
                 .Get(His(0).Pth('name'))
                 .Get(His(0).Pth('nsURI')),
                 
            #create and register my own meta model
            Cmp().Crn('http://www.eclipse.org/emf/2002/Ecore','EPackage',1)
                 .Set(His(0),'name','myPackage')
                 .Set(His(0),'nsURI','http://temp.package.url')
                 .Crn('http://www.eclipse.org/emf/2002/Ecore','EClass',1,['MyClass']) #for the creation of eClass objects the name must be passed as an argument
                 .Gmm() #retrieve the meta models to get the data types of the ecore model, e.g. EString
                 .Get(His(-1).Sel(Pth('nsURI').Equ('http://www.eclipse.org/emf/2002/Ecore')).Idx(0)) 
                 .Get(His(-1).Pth('eClassifiers').Sel(Pth('name').Equ('EString')).Idx(0))
                 .Crn('http://www.eclipse.org/emf/2002/Ecore','EAttribute',1,Arr(['name',His(-1)])) #create a new attribute. The arguments are name and type of the attribute
                 .Add(His(3),'eStructuralFeatures',His(-1)) #add the new attribute
                 .Add(His(0),'eClassifiers',His(3))
                 .Rmm(His(0)), #register the new meta model
            
            #show that the new model exists
            Get(Met('METAMODELS').Pth('name')),
            
            #create a new instance of the own meta model
            Cmp().Crn('http://temp.package.url','MyClass',1)
                 .Get(His(0).Met('CLASS').Pth('name')),
                 
            #create a new class and serialize it
            Cmp().Crn('http://www.eoq.de/workspacemdbmodel/v1.0','ModelResource',1)
                 .Set(His(0),'name','Test.mypackage')
                 .Add(Obj(0),'resources',His(0)) #add the new resource
                 .Crn('http://temp.package.url','MyClass',1)
                 .Add(His(0),'contents',His(3))
                 .Set(His(3),'name','name of my new class'), #this attribute has been created above
                 
            #serialize the meta model
            Cmp().Crn('http://www.eoq.de/workspacemdbmodel/v1.0','ModelResource',1)
                 .Set(His(0),'name','mypackage.ecore')
                 .Add(Obj(0),'resources',His(0)) #add the new resource
                 .Gmm()
                 .Get(His(-1).Sel(Pth('name').Equ('myPackage')).Idx(0))
                 .Add(His(0),'contents',His(-1)),
                 
            #wait until the files are saved
            Cal('Misc/sleep',[2]), #wait some time
                 
            #get name and URI of all meta models
            Cmp().Gmm()
                 .Get(His(0).Sel( Pth('nsURI').Equ('http://temp.package.url')).Idx(0))
                 .Umm(His(1))
           ]
    
    
    #Run an commands sequentially and print their output
    testNr = 1
    for cmd in cmds:
        start = timer()
        jsonCmd = serializer.Ser(cmd)
        print("%d: CMD (JSON): %s"%(testNr,jsonCmd))
        print("%d: CMD (TXT): %s"%(testNr,serializer2.Ser(cmd)))
        print("%d: CMD (PY): %s"%(testNr,serializer3.Ser(cmd)))
        print("%d: CMD (JS): %s"%(testNr,serializer4.Ser(cmd)))
        cmd2 = serializer.Des(jsonCmd)
        try:
            val = domain.Do(cmd2)
            print("Result: %s"%(val))
        except Exception as e:
            print("Command failed: %s"%(str(e)))
        
        end = timer()
        
        print(""); #newline
        
        testNr += 1
        