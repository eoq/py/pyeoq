'''
PyEOQ Example: BasicTextualCommands.py
----------------------------------------------------------------------------------------
This example shows how to use textual EOQ expressions (commands and queries) in Python.
It shows basic commands on a Single file MDB and how to use the TextSetializer to 
transform between textual and programmatic EOQ expressions.

See EOQ User Manual for more information: https://gitlab.com/eoq/doc

2020 Bjoern Annighoefer
'''

from eoq2.mdb.pyecore import PyEcoreSingleFileMdbProvider,PyEcoreMdbAccessor,PyEcoreIdCodec
from eoq2.domain.local import LocalMdbDomain
from eoq2.serialization import TextSerializer
      
'''
MAIN: Execution starts here
'''            
if __name__ == '__main__':
    # Opening a single user model (and meta-model) as the model database (MDB)
    mdbProvider = PyEcoreSingleFileMdbProvider("Workspace/MinimalFlightControl.oaam","Workspace/Meta/oaam.ecore")
    valueCodec = PyEcoreIdCodec() #create the default value codec that converts objects to #IDs and back
    mdbAccessor = PyEcoreMdbAccessor(mdbProvider.GetMdb(),valueCodec) #The accessor abstracts the meta-model runtime from EOQ
    domain = LocalMdbDomain(mdbAccessor) #Create a domain, which is the sole element to interact with the domain
    mdbProvider.CoupleWithDomain(domain, valueCodec)  #This is necessary, to notify the MDB about model changes, e.g. for auto save of model files
    
    # Create a serializer to parse text commands
    serializer = TextSerializer() # The text serializer transforms EOQ expressions in string and back
    
    #Get all elements of class task contained in the model
    cmd = serializer.Des("GET !Task") #create a command out of a string
    allTasks = domain.Do(cmd) #execute the common on the domain
    print("These are all tasks in the model: %s"%(allTasks))
    
    # Get the name property of all tasks
    cmd = serializer.Des("GET !Task/name")
    allTaskNames = domain.Do(cmd) 
    print("These tasks are named': %s"%(allTaskNames))
    
    # Get the name property of all tasks by using the intermediate result
    cmd = serializer.Des("GET %s/name"%(allTasks))
    allTaskNames = domain.Do(cmd) 
    print("These tasks are named (second time)': %s"%(allTaskNames))
    
    # Find the object which is referenced by the subfunctions association of 
    # the object with the name 'Systems' of the subfunctions association of 
    # the object in function association.
    cmd = serializer.Des("GET /functions/subfunctions{/name='Systems'}:0/subfunctions{/name='FlightControl'}:0")
    root = domain.Do(cmd) 
    print("The root object of the OAAM functions element named 'FlightControl' is %s"%(root))
    
    # Get all elements of class task contained in the models root
    cmd = serializer.Des("GET %s!Task"%(root))
    allTasksInRoot = domain.Do(cmd) 
    print("These are all tasks in 'FlightControl': %s"%(allTasksInRoot))
    
    
