'''
PyEOQ Example: MergeObjects.py
----------------------------------------------------------------------------------------
This example shows how to merge the changes of a second model in a first model by using 
the MRG command.

See EOQ User Manual for more information: https://gitlab.com/eoq/doc

2020 Bjoern Annighoefer
'''
import os,sys,inspect,shutil
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 

from eoq2.mdb.pyecore import PyEcoreWorkspaceMdbProvider,PyEcoreMdbAccessor,PyEcoreIdCodec
from eoq2.domain.local import LocalMdbDomain

from eoq2 import Get,Cpr,Mrg,Clo,Crn,Rem,Add,CloModes #import GET and MRG command
from eoq2 import Cmp,Qry,His,Cls,Pth,Arr,Idx #import query segments

from eoq2.util import NoLogging,ConsoleLogger,FileLogger,ConsoleAndFileLogger,LogLevels,DEFAULT_LOG_LEVELS
from eoq2.serialization import JsonSerializer,TextSerializer

from timeit import default_timer as timer #used to time the command's execution time.
      
'''
MAIN: Execution starts here
'''            
if __name__ == '__main__':
    # Select Logging
    # logger = ConsoleAndFileLogger() # for more output 
    logger = FileLogger() # preferred for merge
    # logger = NoLogging()

    # Initialize an WorkspaceMDB
    mdbProvider = PyEcoreWorkspaceMdbProvider('Workspace',metaDir=['./Meta'],trackFileChanges=False) #./Meta must contain oaam.ecore
    valueCodec = PyEcoreIdCodec() #create the default value codec that converts objects to ids
    mdbAccessor = PyEcoreMdbAccessor(mdbProvider.GetMdb(),valueCodec)
    domain = LocalMdbDomain(mdbAccessor,logger=logger)
    mdbProvider.CoupleWithDomain(domain, valueCodec)
    
    #Get the merge Subdir:
    print("Getting merge dir... ",end="")
    start = timer()
    mergeDir = domain.Do( Get(Pth('subdirectories').Sel(Pth('name').Equ('Merge')).Idx(0)) )
    print("ok. (%f s)"%(timer()-start))
    
    #get the original file
    
    original = domain.Do( Get(Qry(mergeDir).Pth('resources').Sel(Pth('name').Equ("MinimalFlightControlOrig.oaam")).Idx(0).Pth('contents').Idx(0)) )
    
    
    #Merge test 1: Merge two identical files
    print("Merge test 1: Original vs same: ")
    resultFile = "Merge1.oaam"
    print("Cloning original... ",end="")
    start = timer()
    originalClone = domain.Do( Clo(original,CloModes.FUL))
    print("ok. (%f s)"%(timer()-start))
    
    same = domain.Do( Get(Qry(mergeDir).Pth('resources').Sel(Pth('name').Equ("MinimalFlightControlSame.oaam")).Idx(0).Pth('contents').Idx(0))  )
    
    print("Merging... ")
    start = timer()
    domain.Do( Mrg(originalClone,same) )
    print("ok. (%f s)"%(timer()-start))
    
    print("Saving result to %s... "%(resultFile),end="")
    start = timer()
    storage = domain.Do( Get(Qry(mergeDir).Pth('resources').Sel(Pth('name').Equ(resultFile)).Trm(Idx('SIZE').Equ(0),None).Idx(0)) )
    if(None==storage):
        storage = domain.Do(Cmp().Crn('http://www.eoq.de/workspacemdbmodel/v1.0','ModelResource',1)
             .Set(His(-1),'name',resultFile)
             .Add(mergeDir,'resources',His(-2)) )[0]#add the new resource
    else:
        domain.Do(Rem(storage,'contents',Qry(storage).Pth('contents').Idx(0)))
        
    domain.Do(Add(storage,'contents',originalClone))
    print("ok. (%f s)"%(timer()-start))
    
    #Merge test 2: Merge two different files
    print("Merge test 2: Original vs changed: ")
    resultFile = "Merge2.oaam"
    print("Cloning original... ",end="")
    start = timer()
    originalClone = domain.Do( Clo(original,CloModes.FUL))
    print("ok. (%f s)"%(timer()-start))
    
    changed = domain.Do( Get(Qry(mergeDir).Pth('resources').Sel(Pth('name').Equ("MinimalFlightControlChanged.oaam")).Idx(0).Pth('contents').Idx(0))  )
    
    print("Merging... ")
    start = timer()
    domain.Do( Mrg(originalClone,changed) )
    print("ok. (%f s)"%(timer()-start))
    
    print("Saving result to %s... "%(resultFile),end="")
    start = timer()
    storage = domain.Do( Get(Qry(mergeDir).Pth('resources').Sel(Pth('name').Equ(resultFile)).Trm(Idx('SIZE').Equ(0),None).Idx(0)) )
    if(None==storage):
        storage = domain.Do(Cmp().Crn('http://www.eoq.de/workspacemdbmodel/v1.0','ModelResource',1)
             .Set(His(-1),'name',resultFile)
             .Add(mergeDir,'resources',His(-2)) )[0]#add the new resource
    else:
        domain.Do(Rem(storage,'contents',Qry(storage).Pth('contents').Idx(0)))
        
    domain.Do(Add(storage,'contents',originalClone))
    print("ok. (%f s)"%(timer()-start))
    
    #Validate results with a compare
    textSerializer = TextSerializer()
    saved = domain.Do( Get(Qry(mergeDir).Pth('resources').Sel(Pth('name').Equ("Merge2.oaam")).Idx(0).Pth('contents').Idx(0))  )
    res = domain.Do( Cpr(changed, saved) )
    print("Compare commands (original <-> merge2):")
    for cmd in res:
        print(cmd)
    print("Done.")


