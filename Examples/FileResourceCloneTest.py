'''
PyEOQ Example: BasicWorkspaceMdb.py
----------------------------------------------------------------------------------------
This example shows how to initialize a local workspace MDB, link it to a domain, and use
the domain to issue queries. 

See EOQ User Manual for more information: https://gitlab.com/eoq/doc

2020 Bjoern Annighoefer
'''

from eoq2.mdb.pyecore import PyEcoreWorkspaceMdbProvider,PyEcoreMdbAccessor,PyEcoreIdCodec
from eoq2.domain.local import LocalMdbDomain

from eoq2 import Get,Clo,Set,CloModes,Add,Rem,Crn #import GET command
from eoq2 import Qry,Cls,Pth,Arr #import query segments

from eoq2.util import Backupper,NoLogging,ConsoleLogger,ConsoleAndFileLogger,LogLevels,DEFAULT_LOG_LEVELS

import os,shutil,time
      
'''
MAIN: Execution starts here
'''            
if __name__ == '__main__':
    WORKSPACE_BLUEPRINT = "WorkspaceBlueprint"
    WORKSPACE_PATH = "WorkspaceFileClone"
    MODEL_OF_INTEREST = "MinimalFlightControl.oaam"
    
    #initalize logger
    logLevels = DEFAULT_LOG_LEVELS+["change","transaction","event"]
    #logLevels = DEFAULT_LOG_LEVELS+[LogLevels.DEBUG,"change","transaction","event"]

    logger = ConsoleLogger(activeLevels=logLevels) 
    
    # Create or clear the workspace directory
    if(os.path.exists(WORKSPACE_PATH)):
        shutil.rmtree(WORKSPACE_PATH)
    shutil.copytree(WORKSPACE_BLUEPRINT, WORKSPACE_PATH)
    
    
    # Initialize an WorkspaceMDB
    mdbProvider = PyEcoreWorkspaceMdbProvider(WORKSPACE_PATH,metaDir=['./Meta'],saveTimeout=1.0,logger=logger,trackFileChanges=False) 
    valueCodec = PyEcoreIdCodec() #create the default value codec that converts objects to ids
    mdbAccessor = PyEcoreMdbAccessor(mdbProvider.GetMdb(),valueCodec)
    domain = LocalMdbDomain(mdbAccessor)
    mdbProvider.CoupleWithDomain(domain, valueCodec)
    
    # Get the dir
    directory = domain.Do( Get(Qry()) ) 
    print("directory: %s"%(directory))
    
    # Find the root object of the resource with the name 'MinimalFlightControl.oaam'
    fileResource = domain.Do( Get( Qry(directory).Pth('resources').Sel(Pth('name').Equ(MODEL_OF_INTEREST)).Idx(0)) ) 
    print("original resource: %s"%(fileResource))
    
    '''variant 1: clone resource, rename and add.'''
    
#     #create a clone of the resource 
#     fileResourceClone = domain.Do( Clo(fileResource,CloModes.FUL) )
#     print("cloned resource: %s"%(fileResourceClone))
#     
#     #rename existing resources
#     domain.Do( Set(fileResource,'name','MinimalFlightControl_backup.oaam') )
#     print("Original renamed.")
#     
#     #change file Name of clone
#     domain.Do( Set(fileResourceClone,'name',MODEL_OF_INTEREST) )
#     print("Cloned renamed.")
#     
#     #attach clone to workspace
#     domain.Do( Add(directory,'resources',fileResourceClone))
#     print("Clone added to dir")
    
    '''variant 2: create resource and clone contents'''
    
#     #rename existing resources
#     domain.Do( Set(fileResource,'name','MinimalFlightControl_backup.oaam') )
#     print("Original renamed.")
#     
#     #clone architecture
#     newArch = domain.Do( Clo(Qry(fileResource).Pth('contents').Idx(0),CloModes.FUL) )
#     
#     #create new resource
#     fileResourceCopy = domain.Do(Crn('http://www.eoq.de/workspacemdbmodel/v1.0','ModelResource', 1))
#     domain.Do(Set(fileResourceCopy, 'name', MODEL_OF_INTEREST))
#     domain.Do(Add(directory, 'resources', fileResourceCopy))
#     domain.Do(Rem(fileResourceCopy, 'contents', Qry(fileResourceCopy).Pth('contents'))) # clear the contents
#     domain.Do(Add(fileResourceCopy, 'contents', newArch))
    
    '''variant 3: clone resource, empty content and clone contents'''
    
    #create a clone of the resource 
    fileResourceClone = domain.Do( Clo(fileResource,CloModes.FUL) )
    print("cloned resource: %s"%(fileResourceClone))
     
    #rename existing resources
    domain.Do( Set(fileResource,'name','MinimalFlightControl_backup.oaam') )
    print("Original renamed.")
     
    #change file Name of clone
    domain.Do( Set(fileResourceClone,'name',MODEL_OF_INTEREST) )
    print("Cloned renamed.")
     
    #attach clone to workspace
    domain.Do( Add(directory,'resources',fileResourceClone))
    print("Clone added to dir")
    
    #clone architecture
    newArch = domain.Do( Clo(Qry(fileResource).Pth('contents').Idx(0),CloModes.FUL) )
    print("cloned architecture: %s"%(newArch))
    
    #clean contents and add new
    domain.Do(Rem(fileResourceClone, 'contents', Qry(fileResourceClone).Pth('contents'))) # clear the contents
    domain.Do(Add(fileResourceClone, 'contents', newArch))
    print("New arch added to cloned resources.")
    
    #wait for domain changes to be saved
    #time.sleep(3)
    
    #close domain and mdb
    domain.Close()
    mdbProvider.Close()

    