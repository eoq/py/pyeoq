'''
PyEOQ Example: ReadAndWriteXmlResources.py
----------------------------------------------------------------------------------------
This example shows how to use the generic XML model that comes with EOQ to read and 
write any xml file in the workspace in the same way as domain-specific models. The class 
diagram of the XML Resource Model of EOQ can be found in the user manual.

See EOQ User Manual for more information: https://gitlab.com/eoq/doc

2020 Bjoern Annighoefer
'''

from eoq2.mdb.pyecore import PyEcoreWorkspaceMdbProvider,PyEcoreMdbAccessor,PyEcoreIdCodec
from eoq2.domain.local import LocalMdbDomain

from eoq2.action.externalpy import ExternalPyScriptHandler
from eoq2 import Get,Set,Add,Rem,Mov,Clo,Crt,Crn,Sts,Chg,Gaa,Cal,Asc,Abc,Cmp,CloModes
from eoq2 import Qry,Obj,His,Cls,Ino,Met,Idx,Pth,Arr
from eoq2.serialization import JsonSerializer,TextSerializer,PySerializer,JsSerializer
from eoq2.event import EvtTypes

from eoq2.util import Backupper,NoLogging,ConsoleLogger,ConsoleAndFileLogger,LogLevels,DEFAULT_LOG_LEVELS

from timeit import default_timer as timer 
import sys

'''
OnEvent (EventCallback):
This function is registered below as an event handler. All event types can be handled by this generic event 
handler and are printed to the screen according to the event type. 
'''
def OnEvent(evts,source):
    # mask the print function because action to prevent that the stdout capturing of 
    # actions calls captures the event printout as well.
    def print(data):
        sys.stderr.write(data)
        
    for evt in evts:
        if(evt.evt == EvtTypes.CST):
            print("EVT: Status of call %d changed to %s"%(evt.a[0],evt.a[1]))
        elif(evt.evt == EvtTypes.OUP):
            print("EVT: Output of call %d on channel %s: %s"%(evt.a[0],evt.a[1],evt.a[2]))
        elif(evt.evt == EvtTypes.CVA):
            print("EVT: Result of call %d: %s"%(evt.a[0],evt.a[1]))
        elif(evt.evt == EvtTypes.CHG):
            print("EVT: Change (%d): %s t: %s, f: %s, n: %s, [was: v:%s, o: %s, f:%s, i:%s] (tid:%d)"%(evt.a[0],evt.a[1],evt.a[2],evt.a[3],evt.a[4],evt.a[5],evt.a[6],evt.a[7],evt.a[8],evt.a[9]))
        elif(evt.evt == EvtTypes.MSG):
            print("EVT: Message: %s"%(evt.a))
        
'''
MAIN: Execution starts here
'''         
if __name__ == '__main__':
    #Basic configuration
    workspaceDir = 'Workspace'
    logDir = './log'
    xmlfile = "books.xml"
    
    #create a backup. This is only for testing purposes and not necessary (both lines needed)
    #backupper = Backupper([workspaceDir,logDir])
    #backupper.CreateBackup()
    
    #define loglevels (chose one of the following lines)
    #logLevels = DEFAULT_LOG_LEVELS
    logLevels = DEFAULT_LOG_LEVELS+["change","transaction","event"]
    #logLevels = DEFAULT_LOG_LEVELS+[LogLevels.DEBUG,"change","transaction","event"]
    
    #initialize logger. For the file based logger this must happen after the backup. (chose one of the following lines)
    #logger = NoLogging() #no output at all
    #logger = ConsoleLogger() #only console output
    logger = ConsoleAndFileLogger(logDir=logDir,activeLevels=logLevels) #console and file output
          
    #Create a model data base (MDB) (chose one of the following lines)
    #mdb = PyEcoreSingleFileMdb("workspace/MinimalFlightControl.oaam","workspace/.meta/oaam.ecore",saveTimeout=1.0,logger=logger)
    mdbProvider = PyEcoreWorkspaceMdbProvider(workspaceDir,metaDir=['./Meta'],saveTimeout=1.0,logger=logger)
    
    #Create an encoding strategy for model based data (chose one of the following lines)
    #valueCodec = SimpleEObjectCodec()
    valueCodec = PyEcoreIdCodec()
    
    #Create an unique accessor to the data
    mdbAccessor = PyEcoreMdbAccessor(mdbProvider.GetMdb(),valueCodec)
    
    #Create a domain and couple it with the mdb provider
    domain = LocalMdbDomain(mdbAccessor,logger=logger)
    mdbProvider.CoupleWithDomain(domain, valueCodec)
    
    
    #Listen to all events of the domain (optional)
    domain.Observe(OnEvent)
    
    #Register external actions (optional)
    externalActionHandler = ExternalPyScriptHandler(domain.cmdRunner.callManager,'Actions',logger=logger)
    
    #prepare serializers
    serializer = JsonSerializer()
    serializer2 = TextSerializer()
    serializer3 = PySerializer()
    serializer4 = JsSerializer()
    
    #Retrieve the model resource for the file we would like to work on.
    cmd = Get(Pth('resources').Sel(Pth('name').Equ(xmlfile)).Idx(0).Pth('document')) #workspace mdb
    jsonCmd = serializer.Ser(cmd)
    print(jsonCmd)
    root = domain.Do(cmd)
    
    #Define a list of demo commands
    cmds = [
                #get the documents name
                Get(Qry(root).Pth('name')),
                 
                #get the root tag 
                Get(Qry(root).Pth('rootelement').Pth('name')),
                 
                #get all first level tags
                Get(Qry(root).Pth('rootelement').Pth('subelements')),
                 
                #get the name of all first level tags
                Get(Qry(root).Pth('rootelement').Pth('subelements').Pth('name')),
                 
                #get the attributes of every book tag
                Get(Qry(root).Pth('rootelement').Pth('subelements').Pth('attributes').Zip([Pth('name'),Pth('value')])),
                 
                #get the value of every books element
                Get(Qry(root).Pth('rootelement').Pth('subelements').Pth('subelements').Zip([Pth('name'),Pth('content')])),
                 
                #search a specific book
                Get(Qry(root).Cls('Element').Sel( 
                    Pth('name').Equ('book') 
                ).Sel( 
                    Pth('subelements').Sel(
                        Pth('name').Equ('author') 
                    ).Sel( 
                        Pth('content').Equ('Corets, Eva') 
                    ).Idx('SIZE').Gre(0) 
                ).Pth('attributes').Sel(Pth('name').Equ('id')).Idx(0).Pth('value') ),
                 
                #save an XML file
                Cmp().Clo(root,CloModes.FUL)
                     .Crn('http://www.eoq.de/workspacemdbmodel/v1.0','XmlResource',1)
                     .Set(His(-1),'name','copy.xml')
                     .Add(Obj(0),'resources',His(1))
                     .Set(His(1),'document',His(0)),
           ]
    
    
    #Run an commands sequentially and print their output
    testNr = 1
    for cmd in cmds:
        start = timer()
        jsonCmd = serializer.Ser(cmd)
        print("%d: CMD (JSON): %s"%(testNr,jsonCmd))
        print("%d: CMD (TXT): %s"%(testNr,serializer2.Ser(cmd)))
        print("%d: CMD (PY): %s"%(testNr,serializer3.Ser(cmd)))
        print("%d: CMD (JS): %s"%(testNr,serializer4.Ser(cmd)))
        cmd2 = serializer.Des(jsonCmd)
        try:
            val = domain.Do(cmd2)
            print("Result: %s"%(val))
        except Exception as e:
            print("Command failed: %s"%(str(e)))
        
        end = timer()
        
        print(""); #newline
        
        testNr += 1
        