'''
PyEOQ Example: MergeObjectsValidation.py
----------------------------------------------------------------------------------------
This is a validation case where a list of modifications are applied to the 
original file and the merge routine should output the exact same commands.

See EOQ User Manual for more information: https://gitlab.com/eoq/doc

2021 Fabian Boerner
'''
import os,sys,inspect,shutil
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 

from eoq2.mdb.pyecore import PyEcoreWorkspaceMdbProvider,PyEcoreMdbAccessor,PyEcoreIdCodec
from eoq2.domain.local import LocalMdbDomain

from eoq2 import CmdTypes,Cmd,Get,Set,Mrg,Cpr,Clo,Crn,Rem,Add,CloModes #import GET and MRG command
from eoq2 import Cmp,Qry,His,Cls,Pth,Arr,Idx #import query segments
from eoq2.serialization import JsonSerializer,TextSerializer

from eoq2.util import NoLogging,ConsoleLogger,FileLogger,ConsoleAndFileLogger,LogLevels,DEFAULT_LOG_LEVELS
      
'''
MAIN: Execution starts here
'''            
if __name__ == '__main__':
    # Select Logging
    # logger = ConsoleAndFileLogger() # for more output 
    logger = FileLogger() # preferred for merge
    # logger = NoLogging()
    textSerializer = TextSerializer()

    # Initialize an WorkspaceMDB
    mdbProvider = PyEcoreWorkspaceMdbProvider('Workspace',metaDir=['./Meta']) #./Meta must contain oaam.ecore
    valueCodec = PyEcoreIdCodec() #create the default value codec that converts objects to ids
    mdbAccessor = PyEcoreMdbAccessor(mdbProvider.GetMdb(),valueCodec)
    domain = LocalMdbDomain(mdbAccessor,logger=logger)
    mdbProvider.CoupleWithDomain(domain, valueCodec)
    
    #Get the merge Subdir:
    print("Getting merge dir... ",end="")
    mergeDir = domain.Do( Get(Pth('subdirectories').Sel(Pth('name').Equ('Merge')).Idx(0)) )
    print("ok.")
    
    #get and clone the original file
    original = domain.Do( Get(Qry(mergeDir).Pth('resources').Sel(Pth('name').Equ("MinimalFlightControlOrig.oaam")).Idx(0).Pth('contents').Idx(0)) )
    originalClone = domain.Do( Clo(original,CloModes.FUL))

    #list of modifications (cloning and children)
    cmds = [
        Set(Qry(originalClone).Pth('hardware').Pth('subhardware').Idx(0).Pth('subhardware').Idx(0).Pth('connections').Idx(0), 'name', 'testName1'),
        Cmp()
            .Clo(Qry(originalClone).Pth('hardware').Pth('subhardware').Sel( Pth('name').Equ('Hardware') ).Idx(0).Pth('devices').Sel( Pth('name').Equ('CPIOM1') ), CloModes.FUL)
            .Add(Qry(originalClone).Pth('hardware').Pth('subhardware').Sel( Pth('name').Equ('Hardware') ).Idx(0), 'devices', Qry(His(-1)))
            .Set(Qry(His(-2)), 'name', 'CPIOM55'),
        Cmp()    
            .Crn('http://www.oaam.de/oaam/model/v140/hardware', 'Device', 1)
            .Set(Qry(His(-1)), 'name', 'RandomSubdeviceA')
            .Set(Qry(His(-2)), 'type', Qry(originalClone).Pth('library').Pth('sublibraries').Sel( Pth('name').Equ('Library') ).Idx(0).Pth('sublibraries').Sel( Pth('name').Equ('DeviceTypes') ).Idx(0).Pth('deviceTypes').Sel( Pth('name').Equ('CPIOM-NEO') ).Idx(0))
            .Add(Qry(originalClone).Pth('hardware').Pth('subhardware').Sel( Pth('name').Equ('Hardware') ).Idx(0).Pth('devices').Sel( Pth('name').Equ('CPIOM55') ), 'subdevices', Qry(His(-3))),
        Cmp()    
            .Crn('http://www.oaam.de/oaam/model/v140/hardware', 'Device', 1)
            .Set(Qry(His(-1)), 'name', 'RandomSubdeviceA1')
            .Set(Qry(His(-2)), 'type', Qry(originalClone).Pth('library').Pth('sublibraries').Sel( Pth('name').Equ('Library') ).Idx(0).Pth('sublibraries').Sel( Pth('name').Equ('DeviceTypes') ).Idx(0).Pth('deviceTypes').Sel( Pth('name').Equ('CPIOM-NEO') ).Idx(0))
            .Add(Qry(originalClone).Pth('hardware').Pth('subhardware').Sel( Pth('name').Equ('Hardware') ).Idx(0).Pth('devices').Sel( Pth('name').Equ('CPIOM55') ).Pth('subdevices').Idx(0), 'subdevices', Qry(His(-3))),
        Cmp()    
            .Crn('http://www.oaam.de/oaam/model/v140/hardware', 'Device', 1)
            .Set(Qry(His(-1)), 'name', 'RandomSubdeviceA11')
            .Set(Qry(His(-2)), 'type', Qry(originalClone).Pth('library').Pth('sublibraries').Sel( Pth('name').Equ('Library') ).Idx(0).Pth('sublibraries').Sel( Pth('name').Equ('DeviceTypes') ).Idx(0).Pth('deviceTypes').Sel( Pth('name').Equ('CPIOM-NEO') ).Idx(0))
            .Add(Qry(originalClone).Pth('hardware').Pth('subhardware').Sel( Pth('name').Equ('Hardware') ).Idx(0).Pth('devices').Sel( Pth('name').Equ('CPIOM55') ).Pth('subdevices').Idx(0).Pth('subdevices').Idx(0), 'subdevices', Qry(His(-3))),
        Cmp()    
            .Crn('http://www.oaam.de/oaam/model/v140/hardware', 'Device', 1)
            .Set(Qry(His(-1)), 'name', 'RandomSubdeviceA12')
            .Set(Qry(His(-2)), 'type', Qry(originalClone).Pth('library').Pth('sublibraries').Sel( Pth('name').Equ('Library') ).Idx(0).Pth('sublibraries').Sel( Pth('name').Equ('DeviceTypes') ).Idx(0).Pth('deviceTypes').Sel( Pth('name').Equ('CPIOM-NEO') ).Idx(0))
            .Add(Qry(originalClone).Pth('hardware').Pth('subhardware').Sel( Pth('name').Equ('Hardware') ).Idx(0).Pth('devices').Sel( Pth('name').Equ('CPIOM55') ).Pth('subdevices').Idx(0).Pth('subdevices').Idx(0), 'subdevices', Qry(His(-3))),
        Cmp()    
            .Crn('http://www.oaam.de/oaam/model/v140/hardware', 'Device', 1)
            .Set(Qry(His(-1)), 'name', 'RandomSubdeviceA2')
            .Set(Qry(His(-2)), 'type', Qry(originalClone).Pth('library').Pth('sublibraries').Sel( Pth('name').Equ('Library') ).Idx(0).Pth('sublibraries').Sel( Pth('name').Equ('DeviceTypes') ).Idx(0).Pth('deviceTypes').Sel( Pth('name').Equ('CPIOM-NEO') ).Idx(0))
            .Add(Qry(originalClone).Pth('hardware').Pth('subhardware').Sel( Pth('name').Equ('Hardware') ).Idx(0).Pth('devices').Sel( Pth('name').Equ('CPIOM55') ).Pth('subdevices').Idx(0), 'subdevices', Qry(His(-3))),
        Cmp()    
            .Crn('http://www.oaam.de/oaam/model/v140/hardware', 'Device', 1)
            .Set(Qry(His(-1)), 'name', 'RandomSubdeviceB')
            .Set(Qry(His(-2)), 'type', Qry(originalClone).Pth('library').Pth('sublibraries').Sel( Pth('name').Equ('Library') ).Idx(0).Pth('sublibraries').Sel( Pth('name').Equ('DeviceTypes') ).Idx(0).Pth('deviceTypes').Sel( Pth('name').Equ('CPIOM-NEO') ).Idx(0))
            .Add(Qry(originalClone).Pth('hardware').Pth('subhardware').Sel( Pth('name').Equ('Hardware') ).Idx(0).Pth('devices').Sel( Pth('name').Equ('CPIOM55') ), 'subdevices', Qry(His(-3)))
    ]

    #apply modifications (to originalClone)
    for cmd in cmds:
        domain.Do(cmd)

    #save modified file
    resultFile = "MergeValidation.oaam"        
    print("Saving MergeValidation to %s... "%(resultFile), end = "")
    storage = domain.Do( Get(Qry(mergeDir).Pth('resources').Sel(Pth('name').Equ(resultFile)).Trm(Idx('SIZE').Equ(0),None).Idx(0)) )
    if(None==storage):
        storage = domain.Do(Cmp().Crn('http://www.eoq.de/workspacemdbmodel/v1.0','ModelResource',1)
             .Set(His(-1),'name',resultFile)
             .Add(mergeDir,'resources',His(-2)) )[0]#add the new resource
    else:
        domain.Do(Rem(storage,'contents',Qry(storage).Pth('contents').Idx(0)))
        
    domain.Do(Add(storage,'contents',originalClone))
    print("Saved MergeValidation.oaam")

    #directly open saved file as a new model
    modified = domain.Do( Get(Qry(mergeDir).Pth('resources').Sel(Pth('name').Equ("MergeValidation.oaam")).Idx(0).Pth('contents').Idx(0)) )
    
    #Validation of compare
    print("Starting compare... ")
    #res = domain.Do( Cpr(original, originalClone) )
    res = domain.Do( Cpr(original, modified) )
    print("Finished.")

    print("Diffs found")
    for cmd in res:
        print(textSerializer.Ser(cmd))
        #expecting only two changes, because all other addition are below the first addition
    print("Done.")
