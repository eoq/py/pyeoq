'''
PyEOQ Example: MergeObjects.py
----------------------------------------------------------------------------------------
This example shows how to merge the changes of a second model in a first model by using 
the MRG command.

See EOQ User Manual for more information: https://gitlab.com/eoq/doc

2020 Bjoern Annighoefer
'''
import os,sys,inspect,shutil
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 

from eoq2.mdb.pyecore import PyEcoreWorkspaceMdbProvider,PyEcoreMdbAccessor,PyEcoreIdCodec
from eoq2.domain.local import LocalMdbDomain

from eoq2 import Get,Mrg,Cpr,Clo,Crn,Rem,Add,CloModes #import GET and MRG command
from eoq2 import Cmp,Qry,His,Cls,Pth,Arr,Idx #import query segments

from eoq2.util import NoLogging,FileLogger,ConsoleLogger,ConsoleAndFileLogger,LogLevels,DEFAULT_LOG_LEVELS

from timeit import default_timer as timer #used to time the command's execution time.
      
'''
MAIN: Execution starts here
'''            
if __name__ == '__main__':
    # Initialize an WorkspaceMDB
    # logger = ConsoleLogger()
    # logger = ConsoleAndFileLogger()
    logger = FileLogger()
    mdbProvider = PyEcoreWorkspaceMdbProvider('Workspace2',metaDir=['./Meta']) #./Meta must contain oaam.ecore
    valueCodec = PyEcoreIdCodec() #create the default value codec that converts objects to ids
    mdbAccessor = PyEcoreMdbAccessor(mdbProvider.GetMdb(),valueCodec)
    domain = LocalMdbDomain(mdbAccessor,logger=logger)
    mdbProvider.CoupleWithDomain(domain, valueCodec)
    
    oaamV1 = domain.Do(Get(Qry().Pth('resources').Sel(Pth('name').Equ('ImportV1.oaam')).Idx(0).Pth('contents').Idx(0)))
    oaamV2 = domain.Do(Get(Qry().Pth('resources').Sel(Pth('name').Equ('ImportV2.oaam')).Idx(0).Pth('contents').Idx(0)))
    
    print("Diff... ",end="")
    start = timer()
    res = domain.Do( Cpr(oaamV1,oaamV2) )
    print("ok. (%f s)"%(timer()-start))
    for cmd in res:
        print(cmd)
        
    
    
