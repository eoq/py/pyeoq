'''
PyEOQ Example: BasicSingleModelMdb.py
----------------------------------------------------------------------------------------
This example shows how to initialize a local single domain-specific MDB, link it to a 
domain, and use the domain to issue queries. 

See EOQ User Manual for more information: https://gitlab.com/eoq/doc

2020 Bjoern Annighoefer
'''

from eoq2.mdb.pyecore import PyEcoreWorkspaceMdbProvider,PyEcoreMdbAccessor,PyEcoreIdCodec
from eoq2.domain.local import LocalMdbDomain

from eoq2 import Get, Set, Gac, Adc, Val, Rmc #import command
from eoq2 import Qry,Cls,Pth,Arr, Idx, Cns #import query segments

from timeit import default_timer as timer

def GetAllArticlesSequentially(domain,allArticles):
    start = timer()
    print("Reading all articles sequentially.", end='')
    for a in allArticles:
        name = domain.Do(Get(Qry(a).Pth('name'))) 
    end = timer()
    print("...%.4f"%(end-start))
    
def GetAllArticlesAtOnce(domain,allArticles): 
    start = timer()
    print("Reading all articles at once.", end='')
    names = domain.Do(Get(Arr([allArticles]).Pth('name'))) 
    end = timer()
    print("...%.4f"%(end-start))

def SetAllArticlesSequentially(domain,allArticles): 
    allConstraints = domain.Do( Gac())
    start = timer()
    print("Changing the name of all articles sequentially (%d constraints active)."%(len(allConstraints)), end='')
    for i in range(len(allArticles)):
        a = allArticles[i]
        newName = "Apple%d"%(i)
        domain.Do(Set(a,'name',newName)) 
    end = timer()
    print("...%.4f"%(end-start))
    
def SetAllArticlesAtOnce(domain,allArticles): 
    allConstraints = domain.Do( Gac())
    start = timer()
    print("Changing the name of all articles at once (%d constraints active)."%(len(allConstraints)), end='')
    newNames = ["Apple%d"%(i) for i in range(len(allArticles))]
    domain.Do(Set(allArticles,'name',newNames)) 
    end = timer()
    print("...%.4f"%(end-start))
      
'''
MAIN: Execution starts here
'''            
if __name__ == '__main__':
    MODELFILE = "ConstraintBenchmark.warehouse"
    
    mdbProvider = PyEcoreWorkspaceMdbProvider('Workspace',metaDir=['./Meta'])
    # Initialize an WorkspaceMDB
    valueCodec = PyEcoreIdCodec() #create the default value codec that converts objects to ids
    mdbAccessor = PyEcoreMdbAccessor(mdbProvider.GetMdb(),valueCodec)
    domain = LocalMdbDomain(mdbAccessor)
    mdbProvider.CoupleWithDomain(domain, valueCodec)
    
    cmd = Get(Pth('resources').Sel(Pth('name').Equ(MODELFILE)).Idx(0).Pth('contents').Idx(0)) #workspace mdb
    root = domain.Do(cmd) 
    
    #get all articles in the benchmark file
    start = timer()
    allArticles = domain.Do(Get(Qry(root).Cls('Article'))) 
    print("Found %d articles."%(len(allArticles)), end='')
    end = timer()
    print("...%.4f"%(end-start))
    
    #get the name for all articles in the benchmark file sequentially
    GetAllArticlesSequentially(domain, allArticles)
    #get the name for all articles in the benchmark file in one call
    GetAllArticlesAtOnce(domain, allArticles)
    #set the name for all articles in the benchmark file sequentially (no constraint)
    SetAllArticlesSequentially(domain, allArticles)
    #set the name for all articles in the benchmark file at once (no constraint)
    SetAllArticlesAtOnce(domain, allArticles)
    
    #add a new constraint
    const1 = domain.Do( Adc('SOFT','!Article','name','=Apple','sixth_constraint','All Article name should equal Apple'))
    
    #set the name for all articles in the benchmark file sequentially (no constraint)
    SetAllArticlesSequentially(domain, allArticles)
    #set the name for all articles in the benchmark file at once (no constraint)
    SetAllArticlesAtOnce(domain, allArticles)
    
    #add a new constraint
    const2 = domain.Do( Adc('SOFT','!Article','name','=Apple','sixth_constraint','All Article name should equal Apple'))
    
    #set the name for all articles in the benchmark file sequentially (no constraint)
    SetAllArticlesSequentially(domain, allArticles)
    #set the name for all articles in the benchmark file at once (no constraint)
    SetAllArticlesAtOnce(domain, allArticles)
    
    #add a new many constraints
    for i in range(10):
        domain.Do( Adc('SOFT','!Article','name','=Apple%d'%(i),'%d_constraint'%(i),'All Article name should equal Apple%d'%(i)))
    
    #get the name for all articles in the benchmark file sequentially
    GetAllArticlesSequentially(domain, allArticles)
    #get the name for all articles in the benchmark file in one call
    GetAllArticlesAtOnce(domain, allArticles)
    #set the name for all articles in the benchmark file sequentially (no constraint)
    SetAllArticlesSequentially(domain, allArticles)
    #set the name for all articles in the benchmark file at once (no constraint)
    SetAllArticlesAtOnce(domain, allArticles)
    
    # Remove all constraints
    print("Remove all constraints")
    result = domain.Do( Rmc(Cls('Article').Idx(0).Cns()))
