'''
PyEOQ Example: ArchDesignerUserModel.py
----------------------------------------------------------------------------------------
This example programmatically creates a user model for the ARCHDESIGNER meta-model. This 
user model comprise a dataflow, a hardware topology and function mappings, but no 
dataflow routing. It creates the file usermodel.archdesigner.

This example is based on the meta-model created in ArchDesignerMetaModel.py. Please run 
ArchDesignerMetaModel.py first.

2021 Bjoern Annighoefer
'''
from eoq2.mdb.pyecore import PyEcoreWorkspaceMdbProvider,PyEcoreMdbAccessor,PyEcoreIdCodec
from eoq2.domain.local import LocalMdbDomain

from eoq2.action.externalpy import ExternalPyScriptHandler
from eoq2 import Get,Set,Add,Rem,Mov,Clo,Crt,Crn,Gmm,Rmm,Umm,Sts,Chg,Gaa,Cal,Asc,Abc,Cmp,CloModes
from eoq2 import Qry,Obj,His,Cls,Ino,Met,Idx,Pth,Arr
from eoq2.serialization import JsonSerializer,TextSerializer,PySerializer,JsSerializer
from eoq2.event import EvtTypes

from eoq2.util import Backupper,NoLogging,ConsoleLogger,ConsoleAndFileLogger,LogLevels,DEFAULT_LOG_LEVELS

from timeit import default_timer as timer 

'''
OnEvent (EventCallback):
This function is registered below as an event handler. All event types can be handled by this generic event 
handler and are printed to the screen according to the event type. 
'''
def OnEvent(evts,source):
    for evt in evts:
        if(evt.evt == EvtTypes.CST):
            print("EVT: Status of call %d changed to %s"%(evt.a[0],evt.a[1]))
        elif(evt.evt == EvtTypes.OUP):
            print("EVT: Output of call %d on channel %s: %s"%(evt.a[0],evt.a[1],evt.a[2]))
        elif(evt.evt == EvtTypes.CVA):
            print("EVT: Result of call %d: %s"%(evt.a[0],evt.a[1]))
        elif(evt.evt == EvtTypes.CHG):
            print("EVT: Change (%d): %s t: %s, f: %s, n: %s, [was: v:%s, o: %s, l:%s, i:%s] (tid:%d)"%(evt.a[0],evt.a[1],evt.a[2],evt.a[3],evt.a[4],evt.a[5],evt.a[6],evt.a[7],evt.a[8],evt.a[9]))
        elif(evt.evt == EvtTypes.MSG):
            print("EVT: Message: %s"%(evt.a))
            
            
def Do(domain,cmd):
    serializer = TextSerializer()
    print(">> %s"%(serializer.Ser(cmd)))
    res = domain.Do(cmd)
    print("%s"%serializer.Ser(res))
    return res
        
'''
MAIN: Execution starts here
'''          
if __name__ == '__main__':
    #Basic configuration
    workspaceDir = 'Workspace'
    logDir = './log'
    
    #define loglevels (chose one of the following lines)
    #logLevels = DEFAULT_LOG_LEVELS
    logLevels = DEFAULT_LOG_LEVELS+["change","transaction","event"]
    #logLevels = DEFAULT_LOG_LEVELS+[LogLevels.DEBUG,"change","transaction","event"]
    
    #initialize logger. For the file based logger this must happen after the backup. (chose one of the following lines)
    logger = NoLogging() #no output at all
    #logger = ConsoleLogger() #only console output
    #logger = ConsoleAndFileLogger(logDir=logDir,activeLevels=logLevels) #console and file output
          
    #Create a model data base (MDB) (chose one of the following lines)
    #mdb = PyEcoreSingleFileMdb("workspace/MinimalFlightControl.oaam","workspace/.meta/oaam.EPACK",saveTimeout=1.0,logger=logger)
    mdbProvider = PyEcoreWorkspaceMdbProvider(workspaceDir,metaDir=['./Meta'],saveTimeout=1.0,logger=logger,trackFileChanges=False)
    
    #Create an encoding strategy for model based data (chose one of the following lines)
    #valueCodec = SimpleEObjectCodec()
    valueCodec = PyEcoreIdCodec()
    
    #Create an unique accessor to the data
    mdbAccessor = PyEcoreMdbAccessor(mdbProvider.GetMdb(),valueCodec)
    
    #Create a domain and couple it with the mdb provider
    domain = LocalMdbDomain(mdbAccessor,logger=logger)
    mdbProvider.CoupleWithDomain(domain, valueCodec)
    
    
    #Listen to all events of the domain (optional)
    #domain.Observe(OnEvent)
    
    #Register external actions (optional)
    externalActionHandler = ExternalPyScriptHandler(domain.cmdRunner.callManager,'Actions',logger=logger)

    
    ADNSURI = 'ADPACK'
    ADPACK = Do(domain,Cmp()
                 .Gmm()
                 .Get(His(-1).Sel(Pth('nsURI').Equ(ADNSURI)).Idx(0)))[1]
    
    #get the classes           
    ARCHMODEL = Do(domain,Get(Qry(ADPACK).Pth('eClassifiers').Sel(Pth('name').Equ('ArchModel')).Idx(0)))
    TASK = Do(domain,Get(Qry(ADPACK).Pth('eClassifiers').Sel(Pth('name').Equ('Task')).Idx(0)))
    SIGNAL = Do(domain,Get(Qry(ADPACK).Pth('eClassifiers').Sel(Pth('name').Equ('Signal')).Idx(0)))
    DEVICE = Do(domain,Get(Qry(ADPACK).Pth('eClassifiers').Sel(Pth('name').Equ('Device')).Idx(0)))
    CONNECTION = Do(domain,Get(Qry(ADPACK).Pth('eClassifiers').Sel(Pth('name').Equ('Connection')).Idx(0)))
    TASKASIGN = Do(domain,Get(Qry(ADPACK).Pth('eClassifiers').Sel(Pth('name').Equ('TaskAssign')).Idx(0)))
    ROUTE = Do(domain,Get(Qry(ADPACK).Pth('eClassifiers').Sel(Pth('name').Equ('Route')).Idx(0)))
    SEGMENT = Do(domain,Get(Qry(ADPACK).Pth('eClassifiers').Sel(Pth('name').Equ('Segment')).Idx(0)))
    
    #create user model
    root = Do(domain,Crn(ADNSURI,'ArchModel',1))
    tasks = Do(domain,Cmp().Crn(ADNSURI,'Task',2)
               .Add(root,'tasks',His(0))
               .Set(His(0),'name',['Task1','Task2']))[0]
    signals = Do(domain,Cmp().Crn(ADNSURI,'Signal',1)
               .Add(root,'signals',His(0))
               .Set(His(0),['name','src','dst'],['Signal12',tasks[0],tasks[1]]))[0]
    devices = Do(domain,Cmp().Crn(ADNSURI,'Device',4)
               .Add(root,'devices',His(0))
               .Set(His(0),'name',['Device1','Device2','Device3','Device4']))[0]
    connections = Do(domain,Cmp().Crn(ADNSURI,'Connection',4)
               .Add(root,'connections',His(0))
               .Set(His(0),['name','length','start','end'],[
                    ['Con12',3,devices[0],devices[1]],
                    ['Con13',1,devices[0],devices[2]],
                    ['Con24',4,devices[1],devices[3]],
                    ['Con34',2,devices[2],devices[3]] ]))[0]
    taskAssigns = Do(domain,Cmp().Crn(ADNSURI,'TaskAssign',2)
               .Add(root,'taskAssigns',His(0))
               .Set(His(0),['task','device'],[
                   [tasks[0],devices[0]],
                   [tasks[1],devices[3]] ]))[0]
    #save the model
    Do(domain,Cmp().Crn('http://www.eoq.de/workspacemdbmodel/v1.0','ModelResource',1)
                .Set(His(0),'name','usermodel.archdesigner')
                .Add(Obj(0),'resources',His(0)) #add the new resource
                .Add(His(0),'contents',root))
    
    
    