'''
PyEOQ Example: BasicSingleModelMdb.py
----------------------------------------------------------------------------------------
This example shows how to initialize a local single domain-specific MDB, link it to a 
domain, and use the domain to issue queries. 

See EOQ User Manual for more information: https://gitlab.com/eoq/doc

2020 Bjoern Annighoefer
'''

from eoq2.mdb.pyecore import PyEcoreSingleFileMdbProvider,PyEcoreMdbAccessor,PyEcoreIdCodec
from eoq2.domain.local import LocalMdbDomain

from eoq2 import Get #import GET command
from eoq2 import Qry,Cls,Pth,Arr #import query segments
      
'''
MAIN: Execution starts here
'''            
if __name__ == '__main__':
    # Initialize an WorkspaceMDB
    mdbProvider = PyEcoreSingleFileMdbProvider("Workspace/MinimalFlightControl.oaam","Workspace/Meta/oaam.ecore")
    valueCodec = PyEcoreIdCodec() #create the default value codec that converts objects to ids
    mdbAccessor = PyEcoreMdbAccessor(mdbProvider.GetMdb(),valueCodec)
    domain = LocalMdbDomain(mdbAccessor)
    mdbProvider.CoupleWithDomain(domain, valueCodec)
    
    # Get all elements of class task contained in the model
    allTasks = domain.Do( Get(Cls('Task')) ) 
    print("These are all tasks in the model: %s"%(allTasks))
    
    # Get the name property of all tasks
    allTaskNames = domain.Do( Get(Cls('Task').Pth('name')) ) 
    print("These tasks are named': %s"%(allTaskNames))
    
    # Get the name property of all tasks by using intermediate result
    allTaskNames = domain.Do( Get(Arr(allTasks).Pth('name')) ) 
    print("These tasks are named (second time)': %s"%(allTaskNames))
    
    # Find the object which is referenced by the subfunctions association of 
    # the object with the name 'Systems' of the subfunctions association of 
    # the object in function association.
    root = domain.Do( Get(Pth('functions').Pth('subfunctions').Sel(Pth('name').Equ("Systems")).Idx(0).Pth('subfunctions').Sel(Pth('name').Equ("FlightControl")).Idx(0)) ) 
    print("The root object of the OAAM functions element named 'FlightControl' is %s"%(root))
    
    # Get all elements of class task contained in the models root
    allTasksInRoot = domain.Do( Get(Qry(root).Cls('Task')) ) 
    print("These are all tasks in 'FlightControl': %s"%(allTasksInRoot))
    