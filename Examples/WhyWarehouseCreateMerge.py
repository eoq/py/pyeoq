'''
PyEOQ Example: BasicWorkspaceMdb.py
----------------------------------------------------------------------------------------
This example shows how to initialize a local workspace MDB, link it to a domain, and use
the domain to issue queries. 

See EOQ User Manual for more information: https://gitlab.com/eoq/doc

2020 Bjoern Annighoefer
'''

import os,sys,inspect,shutil
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 

from eoq2.mdb.pyecore import PyEcoreWorkspaceMdbProvider,PyEcoreMdbAccessor,PyEcoreIdCodec
from eoq2.domain.local import LocalMdbDomain
from eoq2.util import NoLogging,ConsoleLogger,ConsoleAndFileLogger,LogLevels,DEFAULT_LOG_LEVELS

from eoq2 import Get,Set,Add,Crn,Cmp,Rem,Mrg #import commands
from eoq2 import Cls,Qry,Pth,Obj,His,Idx #import query segments
      
'''
MAIN: Execution starts here
'''            
if __name__ == '__main__':
    warehouseDb = 'GeneralStore.warehouse'
    articles = [
        ("food/fresh/fruit", "Apple", 0.25, 400),
        ("food/fresh/fruit", "Banana", 0.35, 1),
        ("food/fresh/fruit", "Orange", 0.45, 20),
        ("food/fresh/fruit", "Melon", 1.1, 2),
        ("food/fresh/vegetables", "Cucumber", 0.5, 4),
        ("food/fresh/vegetables", "Lettuce", 0.8, 2),
        ("food/fresh/milk/cheese", "Goat Cheese", 5.0, 0),
        ("food/fresh/milk/cheese", "Gouda", 2.0, 100),
        ("food/fresh/milk", "Cream", 0.5, 300),
        ("food/fresh/milk", "Milk", 0.6, 700),
        ("food/drinks", "Coffee", 2.0, 100),
        ("food/drinks", "Hot Chocolate", 2.5, 1),
        ("food/drinks/lemonade", "Coke", 1.0, 1000),
        ("food/drinks/lemonade", "Energy Drink", 10.0, 0),
        ('food/drinks/lemonade', 'Ice Tea',      1.49,  20),
        ("food/bakery/", "Flour", 0.19, 25)
    ]
    
    # Initialize an WorkspaceMDB
    logger = ConsoleAndFileLogger()
    mdbProvider = PyEcoreWorkspaceMdbProvider('Workspace',metaDir=['./Meta'],saveTimeout=0.5, logger = logger) 
    valueCodec = PyEcoreIdCodec() #create the default value codec that converts objects to #IDs
    mdbAccessor = PyEcoreMdbAccessor(mdbProvider.GetMdb(),valueCodec)
    domain = LocalMdbDomain(mdbAccessor, logger = logger)
    mdbProvider.CoupleWithDomain(domain, valueCodec)
    
    # Look if the warehouse DB already exists and it in that case
    resource = domain.Do( Get(Pth('resources').Sel(Pth('name').Equ(warehouseDb)).Trm(Idx('SIZE').Les(1),None).Idx(0)) )
    if(resource):
        # Empty the content
        domain.Do( Rem(resource,'contents',Qry(resource).Pth('contents')) )
    else:
        # Create file 
        cmd = Cmp().Crn('http://www.eoq.de/workspacemdbmodel/v1.0','ModelResource',1).Set(His(0),'name',warehouseDb).Add(Obj(0),'resources',His(0)) #add the new resource
        res = domain.Do(cmd)
        resource = res[0]
    # Create the warehouse root
    cmd = Cmp().Crn('http://examples.eoq.de/warehouse','Warehouse',1).Add(Qry(resource),'contents',His(0)) #add the new resource
    res = domain.Do(cmd)
    warehouse = res[0]
    
    # Fill the warehouse with all the articles
    for a in articles:
        # Find the appropriate parent category
        parent = warehouse
        categoryName = a[0].split("/")
        for c in categoryName:
            category = domain.Do( Get(Qry(parent).Pth('categories').Sel(Pth('name').Equ(c)).Trm(Idx('SIZE').Les(1),None).Idx(0)) )
            if(not category):
                category = domain.Do(Crn('http://examples.eoq.de/warehouse','Category',1))
                domain.Do(Set(category,'name',c))
                domain.Do( Add(parent,'categories',category) )
            parent = category
        # Create article and set elements
        article = domain.Do(Crn('http://examples.eoq.de/warehouse','Article',1))
        domain.Do( Set(article,['name','price','sells'],[a[1],a[2],a[3]]) )
        domain.Do( Add(parent,'articles',article) )

    # Copy file for debugging
    #shutil.copyfile('/home/fabian/Dokumente/ILS/essentialobjectquery/py/Examples/Workspace/GeneralStore.warehouse', 
    #    '/home/fabian/Dokumente/ILS/essentialobjectquery/py/Examples/Workspace/GeneralStoreDebug.warehouse')
    
    ### 2 ### Create second user model for merge tests
    warehouseDb2 = 'GeneralStore2.warehouse'

    # Look if the warehouse DB already exists and it in that case
    resource = domain.Do( Get(Pth('resources').Sel(Pth('name').Equ(warehouseDb2)).Trm(Idx('SIZE').Les(1),None).Idx(0)) )
    if(resource):
        # Empty the content
        domain.Do( Rem(resource,'contents',Qry(resource).Pth('contents')) )
    else:
        # Create file 
        cmd = Cmp().Crn('http://www.eoq.de/workspacemdbmodel/v1.0','ModelResource',1).Set(His(0),'name',warehouseDb2).Add(Obj(0),'resources',His(0)) #add the new resource
        res = domain.Do(cmd)
        resource = res[0]
    # Create the warehouse root
    cmd = Cmp().Crn('http://examples.eoq.de/warehouse','Warehouse',1).Add(Qry(resource),'contents',His(0)) #add the new resource
    res = domain.Do(cmd)
    warehouse2 = res[0]
    
    # Fill the warehouse with all the articles
    for a in articles:
        # Find the appropriate parent category
        parent = warehouse2
        categoryName = a[0].split("/")
        for c in categoryName:
            category = domain.Do( Get(Qry(parent).Pth('categories').Sel(Pth('name').Equ(c)).Trm(Idx('SIZE').Les(1),None).Idx(0)) )
            if(not category):
                category = domain.Do(Crn('http://examples.eoq.de/warehouse','Category',1))
                domain.Do(Set(category,'name',c))
                domain.Do( Add(parent,'categories',category) )
            parent = category
        # Create article and set elements
        article = domain.Do(Crn('http://examples.eoq.de/warehouse','Article',1))
        domain.Do( Set(article,['name','price','sells'],[a[1],a[2],a[3]]) )
        domain.Do( Add(parent,'articles',article) )

    ### 3 ### Modify the Warehouse

    # add pears, butter and pasta
    articles2 = [
        ('food/fresh/fruit', 'Pears', 0.69, 100),
        ('food/fresh/milk', 'Butter', 1.99,  75),
        ('food/pasta', 'Spaghetti',   1.29,  50),
        ('food/pasta', 'Lasagne',     1.39,  35),
        ('nonfood/newspapers', 'SZ',  3.99,  40),
        ('nonfood/tobacco', 'HB',     7.99,   5)
        ]
    # Add new Articles
    for a in articles2:
        # Find the appropriate parent category
        parent = warehouse2
        categoryName = a[0].split("/")
        for c in categoryName:
            category = domain.Do( Get(Qry(parent).Pth('categories').Sel(Pth('name').Equ(c)).Trm(Idx('SIZE').Les(1),None).Idx(0)) )
            if(not category):
                category = domain.Do(Crn('http://examples.eoq.de/warehouse','Category',1))
                domain.Do(Set(category,'name',c))
                domain.Do( Add(parent,'categories',category) )
            parent = category
        # Create article and set elements
        article = domain.Do(Crn('http://examples.eoq.de/warehouse','Article',1))
        domain.Do( Set(article,['name','price','sells'],[a[1],a[2],a[3]]) )
        domain.Do( Add(parent,'articles',article) )

    # remove bananas (just selling locally produced fruit)
    # banana = domain.Do( Get(Cls('Article').Sel(Pth('name').Equ('Banana')).Idx(0)) )
    # domain.Do( Rem(Obj(0), 'Article', banana) )

    # 1 change name of Lettuce
    lettuce = domain.Do( Get(Qry(warehouse2).Cls('Article').Sel(Pth('name').Equ('Lettuce')).Idx(0)) )
    domain.Do( Set(lettuce, 'name', 'Iceberg Lettuce') )

    # 2 change price of Coffee
    coffee = domain.Do( Get(Qry(warehouse2).Cls('Article').Sel(Pth('name').Equ('Coffee')).Idx(0)) )
    domain.Do( Set(coffee, 'price', 0.99) )

    # 3 change sells of Hot Chocolate
    coffee = domain.Do( Get(Qry(warehouse2).Cls('Article').Sel(Pth('name').Equ('Hot Chocolate')).Idx(0)) )
    domain.Do( Set(coffee, 'sells', 2) )

    # remove oranges
    domain.Do( Cmp().Get( Qry(warehouse2).Cls('Article').Sel(Pth('name').Equ('Orange')).Idx(0) )
                    .Get( His(-1).Met('PARENT'))
                    .Rem( His(-1), 'articles', His(-2) ) )

    # remove bakery
    domain.Do( Cmp().Get( Qry(warehouse2).Cls('Category').Sel(Pth('name').Equ('bakery')).Idx(0) )
                    .Get( His(-1).Met('PARENT'))
                    .Rem( His(-1), 'categories', His(-2) ) )

    # move coke (switch indexes of coke and energy drink)
    domain.Do( Cmp().Get( Qry(warehouse2).Cls('Article').Sel(Pth('name').Equ('Coke')).Idx(0) )
                    .Mov( His(-1), 1 ) )

    # Copy file for debugging
    #shutil.copyfile('/home/fabian/Dokumente/ILS/essentialobjectquery/py/Examples/Workspace/GeneralStore2.warehouse', 
    #    '/home/fabian/Dokumente/ILS/essentialobjectquery/py/Examples/Workspace/GeneralStore2Debug.warehouse')

    ### 4 ### Merge
    print('Merging ...')

    res = domain.Do( Mrg(warehouse, warehouse2) )

    print('\n\nDone.')
    print(res)
