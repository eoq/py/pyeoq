'''
PyEOQ Example: ArchDesignerMetaModel.py
----------------------------------------------------------------------------------------
This example creates the new metamodel archdesigner.ecore, which holds a data model
for an hypothetic engineering tool ARCHDESIGNER. The ARCHDESIGNER meta-model comprises
classes to model a dataflow between software tasks, as well as the hardware topology of 
a distributed computing system. In addition, the allocation of the dataflow to the 
topology can be modeled.

2021 Bjoern Annighoefer
'''
from eoq2.mdb.pyecore import PyEcoreWorkspaceMdbProvider,PyEcoreMdbAccessor,PyEcoreIdCodec
from eoq2.domain.local import LocalMdbDomain

from eoq2.action.externalpy import ExternalPyScriptHandler
from eoq2 import Get,Set,Add,Rem,Mov,Clo,Crt,Crn,Gmm,Rmm,Umm,Sts,Chg,Gaa,Cal,Asc,Abc,Cmp,CloModes
from eoq2 import Qry,Obj,His,Cls,Ino,Met,Idx,Pth,Arr
from eoq2.serialization import JsonSerializer,TextSerializer,PySerializer,JsSerializer
from eoq2.event import EvtTypes

from eoq2.util import Backupper,NoLogging,ConsoleLogger,ConsoleAndFileLogger,LogLevels,DEFAULT_LOG_LEVELS

from timeit import default_timer as timer 

'''
OnEvent (EventCallback):
This function is registered below as an event handler. All event types can be handled by this generic event 
handler and are printed to the screen according to the event type. 
'''
def OnEvent(evts,source):
    for evt in evts:
        if(evt.evt == EvtTypes.CST):
            print("EVT: Status of call %d changed to %s"%(evt.a[0],evt.a[1]))
        elif(evt.evt == EvtTypes.OUP):
            print("EVT: Output of call %d on channel %s: %s"%(evt.a[0],evt.a[1],evt.a[2]))
        elif(evt.evt == EvtTypes.CVA):
            print("EVT: Result of call %d: %s"%(evt.a[0],evt.a[1]))
        elif(evt.evt == EvtTypes.CHG):
            print("EVT: Change (%d): %s t: %s, f: %s, n: %s, [was: v:%s, o: %s, l:%s, i:%s] (tid:%d)"%(evt.a[0],evt.a[1],evt.a[2],evt.a[3],evt.a[4],evt.a[5],evt.a[6],evt.a[7],evt.a[8],evt.a[9]))
        elif(evt.evt == EvtTypes.MSG):
            print("EVT: Message: %s"%(evt.a))
            
            
def Do(domain,cmd):
    serializer = TextSerializer()
    print(">> %s"%(serializer.Ser(cmd)))
    res = domain.Do(cmd)
    print("%s"%serializer.Ser(res))
    return res
        
'''
MAIN: Execution starts here
'''          
if __name__ == '__main__':
    #Basic configuration
    workspaceDir = 'Workspace'
    logDir = './log'
    
    #define loglevels (chose one of the following lines)
    #logLevels = DEFAULT_LOG_LEVELS
    logLevels = DEFAULT_LOG_LEVELS+["change","transaction","event"]
    #logLevels = DEFAULT_LOG_LEVELS+[LogLevels.DEBUG,"change","transaction","event"]
    
    #initialize logger. For the file based logger this must happen after the backup. (chose one of the following lines)
    logger = NoLogging() #no output at all
    #logger = ConsoleLogger() #only console output
    #logger = ConsoleAndFileLogger(logDir=logDir,activeLevels=logLevels) #console and file output
          
    #Create a model data base (MDB) (chose one of the following lines)
    #mdb = PyEcoreSingleFileMdb("workspace/MinimalFlightControl.oaam","workspace/.meta/oaam.EPACK",saveTimeout=1.0,logger=logger)
    mdbProvider = PyEcoreWorkspaceMdbProvider(workspaceDir,metaDir=['./Meta'],saveTimeout=1.0,logger=logger,trackFileChanges=False)
    
    #Create an encoding strategy for model based data (chose one of the following lines)
    #valueCodec = SimpleEObjectCodec()
    valueCodec = PyEcoreIdCodec()
    
    #Create an unique accessor to the data
    mdbAccessor = PyEcoreMdbAccessor(mdbProvider.GetMdb(),valueCodec)
    
    #Create a domain and couple it with the mdb provider
    domain = LocalMdbDomain(mdbAccessor,logger=logger)
    mdbProvider.CoupleWithDomain(domain, valueCodec)
    
    
    #Listen to all events of the domain (optional)
    #domain.Observe(OnEvent)
    
    #Register external actions (optional)
    externalActionHandler = ExternalPyScriptHandler(domain.cmdRunner.callManager,'Actions',logger=logger)

    
    ENSURI = 'http://www.eclipse.org/emf/2002/Ecore'
    EPACK = Do(domain,Cmp()
                 .Gmm()
                 .Get(His(-1).Sel(Pth('nsURI').Equ(ENSURI)).Idx(0)))[1]
    
    #get the EPACK basic types              
    ESTR = Do(domain,Get(Qry(EPACK).Pth('eClassifiers').Sel(Pth('name').Equ('EString')).Idx(0)))
    EINT = Do(domain,Get(Qry(EPACK).Pth('eClassifiers').Sel(Pth('name').Equ('EInt')).Idx(0)))
    
    
    #create new meta model
    ADPACK = Do(domain,Cmp().Crn(ENSURI,'EPackage',1)
                 .Set(His(0),'name','archdesigner')
                 .Set(His(0),'nsURI','ADPACK'))[0]
    TASK = Do(domain,Cmp().Crn(ENSURI,'EClass',1,['Task'])
                 .Add(ADPACK,'eClassifiers',His(0)) 
                 .Crn(ENSURI,'EAttribute',1,['name',ESTR]) 
                 .Add(His(0),'eStructuralFeatures',His(-1)))[0]
    SIGNAL = Do(domain,Cmp().Crn(ENSURI,'EClass',1,['Signal']) 
                 .Add(ADPACK,'eClassifiers',His(0)) 
                 .Crn(ENSURI,'EAttribute',1,['name',ESTR]) 
                 .Add(His(0),'eStructuralFeatures',His(-1)) 
                 .Crn(ENSURI,'EReference',1,['src']) 
                 .Add(His(0),'eStructuralFeatures',His(-1))
                 .Set(His(-2),'eType',TASK).Set(His(-3),'lowerBound',1).Set(His(-4),'upperBound',1)
                 .Crn(ENSURI,'EReference',1,['dst']) 
                 .Add(His(0),'eStructuralFeatures',His(-1))
                 .Set(His(-2),'eType',TASK).Set(His(-3),'lowerBound',1).Set(His(-4),'upperBound',1))[0]
    DEVICE = Do(domain,Cmp().Crn(ENSURI,'EClass',1,['Device'])
                 .Add(ADPACK,'eClassifiers',His(0)) 
                 .Crn(ENSURI,'EAttribute',1,['name',ESTR]) 
                 .Add(His(0),'eStructuralFeatures',His(-1)))[0]
    CONNECTION = Do(domain,Cmp().Crn(ENSURI,'EClass',1,['Connection']) 
                 .Add(ADPACK,'eClassifiers',His(0)) 
                 .Crn(ENSURI,'EAttribute',1,['name',ESTR]) 
                 .Add(His(0),'eStructuralFeatures',His(-1)) 
                 .Crn(ENSURI,'EAttribute',1,['length',EINT]) 
                 .Add(His(0),'eStructuralFeatures',His(-1)) 
                 .Crn(ENSURI,'EReference',1,['start']) 
                 .Add(His(0),'eStructuralFeatures',His(-1))
                 .Set(His(-2),'eType',DEVICE).Set(His(-3),'lowerBound',1).Set(His(-4),'upperBound',1)
                 .Crn(ENSURI,'EReference',1,['end']) 
                 .Add(His(0),'eStructuralFeatures',His(-1))
                 .Set(His(-2),'eType',DEVICE).Set(His(-3),'lowerBound',1).Set(His(-4),'upperBound',1))[0]
    TASKASSIGN = Do(domain,Cmp().Crn(ENSURI,'EClass',1,['TaskAssign']) 
                 .Add(ADPACK,'eClassifiers',His(0)) 
                 .Crn(ENSURI,'EReference',1,['task']) 
                 .Add(His(0),'eStructuralFeatures',His(-1))
                 .Set(His(-2),'eType',TASK).Set(His(-3),'lowerBound',1).Set(His(-4),'upperBound',1)
                 .Crn(ENSURI,'EReference',1,['device']) 
                 .Add(His(0),'eStructuralFeatures',His(-1))
                 .Set(His(-2),'eType',DEVICE).Set(His(-3),'lowerBound',1).Set(His(-4),'upperBound',1))[0]
    SEGMENT = Do(domain,Cmp().Crn(ENSURI,'EClass',1,['Segment']) 
                 .Add(ADPACK,'eClassifiers',His(0)) 
                 .Crn(ENSURI,'EReference',1,['device']) 
                 .Add(His(0),'eStructuralFeatures',His(-1))
                 .Set(His(-2),'eType',DEVICE).Set(His(-3),'lowerBound',1).Set(His(-4),'upperBound',1)
                 .Crn(ENSURI,'EReference',1,['connection']) 
                 .Add(His(0),'eStructuralFeatures',His(-1))
                 .Set(His(-2),'eType',CONNECTION).Set(His(-3),'lowerBound',1).Set(His(-4),'upperBound',1))[0]
    ROUTE = Do(domain,Cmp().Crn(ENSURI,'EClass',1,['Route']) 
                 .Add(ADPACK,'eClassifiers',His(0)) 
                 .Crn(ENSURI,'EReference',1,['signal']) 
                 .Add(His(0),'eStructuralFeatures',His(-1))
                 .Set(His(-2),'eType',SIGNAL).Set(His(-3),'lowerBound',1).Set(His(-4),'upperBound',1)
                 .Crn(ENSURI,'EReference',1,['segments']) 
                 .Add(His(0),'eStructuralFeatures',His(-1))
                 .Set(His(-2),'eType',SEGMENT).Set(His(-3),'lowerBound',1).Set(His(-4),'upperBound',-1)
                 .Set(His(-5),'containment',True))[0]
    ARCHMODEL = Do(domain,Cmp().Crn(ENSURI,'EClass',1,['ArchModel']) 
                 .Add(ADPACK,'eClassifiers',His(0))
                 .Crn(ENSURI,'EReference',1,['tasks']) 
                 .Add(His(0),'eStructuralFeatures',His(-1))
                 .Set(His(-2),'eType',TASK).Set(His(-3),'lowerBound',1).Set(His(-4),'upperBound',-1)
                 .Set(His(-5),'containment',True)
                 .Crn(ENSURI,'EReference',1,['signals']) 
                 .Add(His(0),'eStructuralFeatures',His(-1))
                 .Set(His(-2),'eType',SIGNAL).Set(His(-3),'lowerBound',1).Set(His(-4),'upperBound',-1)
                 .Set(His(-5),'containment',True)
                 .Crn(ENSURI,'EReference',1,['devices']) 
                 .Add(His(0),'eStructuralFeatures',His(-1))
                 .Set(His(-2),'eType',DEVICE).Set(His(-3),'lowerBound',1).Set(His(-4),'upperBound',-1)
                 .Set(His(-5),'containment',True)
                 .Crn(ENSURI,'EReference',1,['connections']) 
                 .Add(His(0),'eStructuralFeatures',His(-1))
                 .Set(His(-2),'eType',CONNECTION).Set(His(-3),'lowerBound',1).Set(His(-4),'upperBound',-1)
                 .Set(His(-5),'containment',True)
                 .Crn(ENSURI,'EReference',1,['taskAssigns']) 
                 .Add(His(0),'eStructuralFeatures',His(-1))
                 .Set(His(-2),'eType',TASKASSIGN).Set(His(-3),'lowerBound',1).Set(His(-4),'upperBound',-1)
                 .Set(His(-5),'containment',True)
                 .Crn(ENSURI,'EReference',1,['routes']) 
                 .Add(His(0),'eStructuralFeatures',His(-1))
                 .Set(His(-2),'eType',ROUTE).Set(His(-3),'lowerBound',1).Set(His(-4),'upperBound',-1)
                 .Set(His(-5),'containment',True)
                 )[0]
                  
    #register the new meta model
    Do(domain,Rmm(ADPACK))
    
    #serialize the meta model
    Do(domain,Cmp().Crn('http://www.eoq.de/workspacemdbmodel/v1.0','ModelResource',1)
                 .Set(His(0),'name','archdesigner.ecore')
                 .Add(Obj(0),'resources',His(0)) #add the new resource
                 .Add(His(0),'contents',ADPACK))
    