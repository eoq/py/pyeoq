'''
PyEOQ Example: WhyWarehouseExample.py
----------------------------------------------------------------------------------------
This file implements the following warehouse example in PyEOQ with programmatic commands: 

Imagine having the following hierarchically organized domain-specific model of you warehouse? 

                                                          _______________
     ___________              _______________            |    Article    |
    | Warehouse | categories |   Category    |  articles |---------------|
    |-----------|<>--------->|---------------|<>-------->| name : string |
    |           |           *| name : string |          *| price : real  |
    |___________|        o-->|_______________|<>-o       | sells : int   |
                         |  *                    |       |_______________|
                         o-----------------------o
                               categories

Would you like to get a list of articles?

    GET !Article

Would you like to know which one is sold less than 3-times?

    GET !Article{/sells<3}

You might want to reduce their price by 10%?

    SET !Article{/sells<3} 'price' !Article{/sells<3}/price&MUL0.9

Would you like to see a list of the names of the categories of the badly selling articles sorted by ascendingly?

    GET !Article{/sells<3}@PARENT/name_[]:SORTASC

See EOQ User Manual for more information: https://gitlab.com/eoq/doc

2020 Bjoern Annighoefer
'''

from eoq2.mdb.pyecore import PyEcoreSingleFileMdbProvider,PyEcoreMdbAccessor,PyEcoreIdCodec
from eoq2.domain.local import LocalMdbDomain
from eoq2 import Get,Set #commands
from eoq2 import Cls,Pth #queries
'''
MAIN: Execution starts here
'''            
if __name__ == '__main__':
    # Initialize a local domain with the warehouse user and meta-model
    mdbProvider = PyEcoreSingleFileMdbProvider('Workspace/GeneralStore.warehouse',"Workspace/Meta/warehouse.ecore")
    valueCodec = PyEcoreIdCodec() #create the default value codec that converts objects to #IDs
    mdbAccessor = PyEcoreMdbAccessor(mdbProvider.GetMdb(),valueCodec)
    domain = LocalMdbDomain(mdbAccessor)
    mdbProvider.CoupleWithDomain(domain, valueCodec)
    
    # Would you like to get a list of articles?
    articles = domain.Do( Get( Cls('Article') ) )
    print(articles)
    
    # Would you like to know which one is sold less than 3-times?
    badArticles = domain.Do( Get( Cls('Article').Sel(Pth('sells').Les(3)) ) )
    print(badArticles)
    
    # You might want to reduce their price by 10%?
    newPrice = domain.Do( Set( 
        Cls('Article').Sel(Pth('sells').Les(3)),
        'price',
        Cls('Article').Sel(Pth('sells').Les(3)).Pth('price').Mul(0.9) 
    ) )
    print(newPrice)
    
    # Would you like to see a list of the names of the categories of the badly selling articles sorted by ascendingly?
    badCategories = domain.Do( Get( Cls('Article').Sel(Pth('sells').Les(3)).Met('PARENT').Pth('name').Uni([]).Idx('SORTASC') ) )
    print(badCategories)
    
    # (Revert all changes back so the example can be executed multiple times)
    newPrice = domain.Do( Set( 
        Cls('Article').Sel(Pth('sells').Les(3)),
        'price',
        Cls('Article').Sel(Pth('sells').Les(3)).Pth('price').Div(0.9) 
    ) )
    print(newPrice)