'''
PyEOQ Example: MultivalueAttributes.py
----------------------------------------------------------------------------------------
This example works with attributes that store a list of values

See EOQ User Manual for more information: https://gitlab.com/eoq/doc

2022 Bjoern Annighoefer
'''

from eoq2.mdb.pyecore import PyEcoreWorkspaceMdbProvider,PyEcoreMdbAccessor,PyEcoreIdCodec
from eoq2.domain.local import LocalMdbDomain
from eoq2 import Get,Set,Add,Rem,Mov,Clo,Crt,Crn,Sts,Chg,Gaa,Cal,Asc,Abc,Cmp,CloModes
from eoq2 import Qry,Obj,His,Cls,Ino,Met,Idx,Pth,Arr
from eoq2.serialization import TextSerializer
from eoq2.util import Backupper,NoLogging,ConsoleLogger,ConsoleAndFileLogger,LogLevels,DEFAULT_LOG_LEVELS
      
'''
MAIN: Execution starts here
'''            
if __name__ == '__main__':
    #Basic configuration
    workspaceDir = 'Workspace'
    modelfile = "Playground.playground"
    
    logLevels = DEFAULT_LOG_LEVELS+["change","transaction","event"]
    logger = ConsoleLogger() #only console output
    
    # Initialize an WorkspaceMDB
    serializer = TextSerializer()
    mdbProvider = PyEcoreWorkspaceMdbProvider(workspaceDir,metaDir=['./Meta'],saveTimeout=1.0,logger=logger) #./Meta must contain oaam.ecore
    valueCodec = PyEcoreIdCodec() #create the default value codec that converts objects to ids
    mdbAccessor = PyEcoreMdbAccessor(mdbProvider.GetMdb(),valueCodec)
    domain = LocalMdbDomain(mdbAccessor)
    mdbProvider.CoupleWithDomain(domain, valueCodec)
    
    # Create a new instance of the multi-value meta model (only required once)
#     cmd = Cmp().Crn('http://www.eoq.de/workspacemdbmodel/v1.0','ModelResource',1)\
#          .Set(His(0),'name',modelfile)\
#          .Add(Obj(0),'resources',His(0))\
#          .Crn('https://eoqplayground.net','Playground',1)\
#          .Add(His(0),'contents',His(-1))
#     domain.Do(cmd)
    
    #load the playground model and modify it.
    root = domain.Do( Get(Pth('resources').Sel(Pth('name').Equ(modelfile)).Idx(0).Pth('contents').Idx(0)) )
    
    # add a single value to a non unique multi-value attribute 
    domain.Do(Add(root,'multiValueFloat',1.0))
    print( serializer.Ser(domain.Get(Qry(root).Pth('multiValueFloat'))) )
    
    # add a single value to a non unique multi-value attribute 
    domain.Do(Add(root,'multiValueFloatUnique',1.0))
    print( serializer.Ser(domain.Get(Qry(root).Pth('multiValueFloatUnique'))) )
    
    # add a multiple values to a non unique multi-value attribute 
    domain.Do(Add(root,'multiValueFloat',[1.0, 2.0, 3.0, 0.0]))
    print( serializer.Ser(domain.Get(Qry(root).Pth('multiValueFloat'))) )
    
    # add a multiple value to a unique multi-value attribute 
    domain.Do(Add(root,'multiValueFloatUnique',[1.0, 2.0, 3.0, 0.0]))
    print( serializer.Ser(domain.Get(Qry(root).Pth('multiValueFloatUnique'))) )
    
    #remove all added values to keep the model in the original state
    domain.Do(Rem(root,'multiValueFloatUnique',[2.0, 3.0, 0.0]))
    print( serializer.Ser(domain.Get(Qry(root).Pth('multiValueFloatUnique'))) )
    
    # add a multiple values to a non unique multi-value attribute 
    domain.Do(Rem(root,'multiValueFloat',[1.0, 2.0, 3.0, 1.0, 0.0]))
    print( serializer.Ser(domain.Get(Qry(root).Pth('multiValueFloat'))) )
             
    
    