'''
PyEOQ Example: BasicSingleModelMdb.py
----------------------------------------------------------------------------------------
This example shows how to initialize a local single domain-specific MDB, link it to a 
domain, and use the domain to issue queries. 

See EOQ User Manual for more information: https://gitlab.com/eoq/doc

2020 Bjoern Annighoefer
'''

from eoq2.mdb.pyecore import PyEcoreWorkspaceMdbProvider,PyEcoreMdbAccessor,PyEcoreIdCodec
from eoq2.domain.local import LocalMdbDomain

from eoq2 import Get, Set, Gac, Adc, Val, Rmc #import command
from eoq2 import Qry,Cls,Pth,Arr, Idx, Cns #import query segments
      
'''
MAIN: Execution starts here
'''            
if __name__ == '__main__':
    mdbProvider = PyEcoreWorkspaceMdbProvider('Workspace',metaDir=['./Meta'])
    # Initialize an WorkspaceMDB
    valueCodec = PyEcoreIdCodec() #create the default value codec that converts objects to ids
    mdbAccessor = PyEcoreMdbAccessor(mdbProvider.GetMdb(),valueCodec)
    domain = LocalMdbDomain(mdbAccessor)
    mdbProvider.CoupleWithDomain(domain, valueCodec)
    
    # Get all elements of class article contained in the model
    print("Get all elements of class article contained in the model")
    result = domain.Do( Get(Cls('Article')))
    print(result)
    
    print("\n\n")
    
    # Set article name to Apple
    print("Set article name to Apple")
    result = domain.Do( Set(Cls('Article'),'name','Apple'))
    print(result)
    
    print("\n\n")
    
    # Get the name property of all articles
    print("Get the name property of all articles")
    result = domain.Do( Get(Cls('Article').Pth('name')))
    print(result)
    
    print("\n\n")
    
    # Get all constraints in the workspace
    print("Get all constraints in the workspace")
    result = domain.Do( Gac())
    print(result)
    
    print("\n\n")
    
    # Add first and second constraint
    print("Add first and second constraint")
    result1 = domain.Do( Adc('HARD','!Article','name','&RGX\'\\w+\'','fivth_constraint','All Article name should follow backslashw+'))
    result2 = domain.Do( Adc('SOFT','!Article','name','=Apple','sixth_constraint','All Article name should equal Apple'))
    print(result1, result2)
    
    print("\n\n")
    
    # Get all constraints for class Article
    print("Get all constraints for class Article")
    result = domain.Do( Get(Cls('Article').Idx(0).Cns()))
    print(result)
    
    print("\n\n")
    
    # Get all constraints
    print("Get all constraints")
    result = domain.Do( Gac())
    print(result)
    
    print("\n\n")
    
    # Validate all constraints
    print("Validate all constraints")
    result = domain.Do( Val(None))
    print(result)
    
    print("\n\n")
    
    # Violate HARD constraint
    print("Violate HARD constraint")
    try:
        result = domain.Do( Set(Cls('Article'),'name','!!!!'))
    except:
        result = "Violate HARD constraint!"
    print(result)
    
    print("\n\n")

    # Get Articles new name
    print("Get Articles new name")
    result = domain.Do( Get(Cls('Article').Pth('name')))
    print(result)
    
    print("\n\n")
    
    # Remove all constraints
    print("Remove all constraints")
    result = domain.Do( Rmc(Cls('Article').Idx(0).Cns()))
    print(result)
    
    # Try to add invalid constraints
    
    #invalid target
    print("Add constraint with invalid target...",end='')
    try:
        result = domain.Do( Adc('SOFT','!Article.','name','=Apple','7_constraint','Invalid target')) 
        print('ADDED...',end='')
        result = domain.Do( Set(Cls('Article'),'name','!!!!'))
        print('WORKS')
    except:
        print('FAILED OK')
    
    
    #non-existing target
    print("Add constraint with not existing target...",end='')
    try:
        result = domain.Do( Adc('SOFT','!ProductNG','name','=Apple','8_constraint','Non-existing target')) 
        print('ADDED...',end='')
        result = domain.Do( Set(Cls('Article'),'name','!!!!'))
        print('WORKS')
    except:
        print('FAILED OK')
    
    #invalid feature
    print("Add constraint with invalid feature...",end='')
    try:
        result = domain.Do( Adc('SOFT','!Article','names','=Apple','9_constraint','Invalid feature'))
        print('ADDED...',end='')
        result = domain.Do( Set(Cls('Article'),'name','!!!!'))
        print('WORKS')
    except:
        print('FAILED OK')
    
    #invalid law
    print("Add constraint with invalid law...",end='')
    try:
        result = domain.Do( Adc('SOFT','!Article','name','=!Apple','10_constraint','Invalid law'))
        print('ADDED...',end='')
        result = domain.Do( Set(Cls('Article'),'name','!!!!'))
        print('WORKS')
    except:
        print('FAILED OK')
    
    #invalid law 
    print("Add constraint with non-boolean law...",end='')
    try:
        result = domain.Do( Adc('SOFT','!Article','name','@SIZE','11_constraint','Non boolean'))
        print('ADDED...',end='')
        result = domain.Do( Set(Cls('Article'),'name','!!!!'))
    except:
        print('FAILED OK')

    # Remove all constraints
    print("Remove all constraints")
    result = domain.Do( Rmc(Cls('Article').Idx(0).Cns()))
    print(result)
    
    print("\n\n")

    # Get all constraints
    print("Get all constraints")
    result = domain.Do( Gac())
    print(result)
    