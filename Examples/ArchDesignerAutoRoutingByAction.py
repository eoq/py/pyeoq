'''
PyEOQ Example: ArchDesignerAutoRoutingByAction.py
----------------------------------------------------------------------------------------
This example shows how to invoke the data flow routing action ArchDesigner/autoroute.py
from EOQ to assign the dataflow from a ARCHDESIGNER meta-model to devices and connections.
The functionality of this example is the same as ArchDesignerAutoRouting.py.

This example is based on the user model created in ArchDesignerUserModel.py. Please run 
ArchDesignerUserModel.py first.

2021 Bjoern Annighoefer
'''
from eoq2.mdb.pyecore import PyEcoreWorkspaceMdbProvider,PyEcoreMdbAccessor,PyEcoreIdCodec
from eoq2.domain.local import LocalMdbDomain

from eoq2.action.externalpy import ExternalPyScriptHandler
from eoq2 import Get,Set,Add,Rem,Mov,Clo,Crt,Crn,Gmm,Rmm,Umm,Sts,Chg,Gaa,Cal,Asc,Abc,Cmp,CloModes
from eoq2 import Qry,Obj,His,Cls,Ino,Met,Idx,Pth,Arr,Sel
from eoq2.serialization import JsonSerializer,TextSerializer,PySerializer,JsSerializer
from eoq2.event import EvtTypes

from eoq2.util import Backupper,NoLogging,ConsoleLogger,ConsoleAndFileLogger,LogLevels,DEFAULT_LOG_LEVELS

import sys

'''
OnEvent (EventCallback):
This function is registered below as an event handler. All event types can be handled by this generic event 
handler and are printed to the screen according to the event type. 
'''
def OnEvent(evts,source):
    # mask the print function because action to prevent that the stdout capturing of 
    # actions calls captures the event printout as well.
    def print(data):
        sys.stderr.write(data)
    # handle events
    for evt in evts:
        if(evt.evt == EvtTypes.CST):
        #    print("EVT: Status of call %d changed to %s"%(evt.a[0],evt.a[1]))
            pass
        elif(evt.evt == EvtTypes.OUP):
        #    print("EVT: Output of call %d on channel %s: %s"%(evt.a[0],evt.a[1],evt.a[2]))
            pass
        elif(evt.evt == EvtTypes.CVA):
        #    print("EVT: Result of call %d: %s"%(evt.a[0],evt.a[1]))
            pass
        elif(evt.evt == EvtTypes.CHG):
            print("EVT: Change (%d): %s t: %s, f: %s, n: %s, [was: v:%s, o: %s, l:%s, i:%s] (tid:%d)\n"%(evt.a[0],evt.a[1],evt.a[2],evt.a[3],evt.a[4],evt.a[5],evt.a[6],evt.a[7],evt.a[8],evt.a[9]))
        elif(evt.evt == EvtTypes.MSG):
        #    print("EVT: Message: %s"%(evt.a))
            pass
            
            
def Do(domain,cmd):
    serializer = TextSerializer()
    print(">> %s"%(serializer.Ser(cmd)))
    res = domain.Do(cmd)
    print("%s"%serializer.Ser(res))
    #print("") #newline
    return res
        
'''
MAIN: Execution starts here
'''          
if __name__ == '__main__':
    #Basic configuration
    workspaceDir = 'Workspace'
    logDir = './log'
    
    #define loglevels (chose one of the following lines)
    logLevels = DEFAULT_LOG_LEVELS
    #logLevels = DEFAULT_LOG_LEVELS+["change","transaction","event"]
    #logLevels = DEFAULT_LOG_LEVELS+[LogLevels.DEBUG,"change","transaction","event"]
    
    #initialize logger. For the file based logger this must happen after the backup. (chose one of the following lines)
    logger = NoLogging() #no output at all
    #logger = ConsoleLogger() #only console output
    #logger = ConsoleAndFileLogger(logDir=logDir,activeLevels=logLevels) #console and file output
          
    #Create a model data base (MDB) (chose one of the following lines)
    #mdb = PyEcoreSingleFileMdb("workspace/MinimalFlightControl.oaam","workspace/.meta/oaam.EPACK",saveTimeout=1.0,logger=logger)
    mdbProvider = PyEcoreWorkspaceMdbProvider(workspaceDir,metaDir=['./Meta'],saveTimeout=1.0,logger=logger,trackFileChanges=False)
    
    #Create an encoding strategy for model based data (chose one of the following lines)
    #valueCodec = SimpleEObjectCodec()
    valueCodec = PyEcoreIdCodec()
    
    #Create an unique accessor to the data
    mdbAccessor = PyEcoreMdbAccessor(mdbProvider.GetMdb(),valueCodec)
    
    #Create a domain and couple it with the mdb provider
    domain = LocalMdbDomain(mdbAccessor,logger=logger)
    mdbProvider.CoupleWithDomain(domain, valueCodec)
    
    
    #Listen to all events of the domain (optional)
    domain.Observe(OnEvent)
    
    #Register external actions (optional)
    externalActionHandler = ExternalPyScriptHandler(domain.cmdRunner.callManager,'Actions',logger=logger)

    
    ADNSURI = 'ADPACK'
    ADPACK = Do(domain,Cmp()
                 .Gmm()
                 .Get(His(-1).Sel(Pth('nsURI').Equ(ADNSURI)).Idx(0)))[1]
    
    #get the classes           
    ARCHMODEL = Do(domain,Get(Qry(ADPACK).Pth('eClassifiers').Sel(Pth('name').Equ('ArchModel')).Idx(0)))
    TASK = Do(domain,Get(Qry(ADPACK).Pth('eClassifiers').Sel(Pth('name').Equ('Task')).Idx(0)))
    SIGNAL = Do(domain,Get(Qry(ADPACK).Pth('eClassifiers').Sel(Pth('name').Equ('Signal')).Idx(0)))
    DEVICE = Do(domain,Get(Qry(ADPACK).Pth('eClassifiers').Sel(Pth('name').Equ('Device')).Idx(0)))
    CONNECTION = Do(domain,Get(Qry(ADPACK).Pth('eClassifiers').Sel(Pth('name').Equ('Connection')).Idx(0)))
    TASKASIGN = Do(domain,Get(Qry(ADPACK).Pth('eClassifiers').Sel(Pth('name').Equ('TaskAssign')).Idx(0)))
    ROUTE = Do(domain,Get(Qry(ADPACK).Pth('eClassifiers').Sel(Pth('name').Equ('Route')).Idx(0)))
    SEGMENT = Do(domain,Get(Qry(ADPACK).Pth('eClassifiers').Sel(Pth('name').Equ('Segment')).Idx(0)))
    
    #create user model
    root = Do(domain,Get(Pth('resources').Sel(Pth('name').Equ('usermodel.archdesigner')).Idx(0).Pth('contents').Idx(0)))
    #only get those tasks which have tasks assigned to devices
    signals = Do(domain,Cmp().Get(Qry(root).Pth('taskAssigns').Pth('task').Idx('FLATTEN'))
                 .Get(Qry(root).Pth('signals'))
                 .Get(His(-1).Sel(Pth('src').Eqa(His(0))))
                 .Get(His(-1).Sel(Pth('dst').Eqa(His(0)))) )[3]
    #route all signals by action
    [callid,routes,status,output] = Do(domain,Cal('ArchDesigner/autoroute',[root,signals]))
    #validate route
    for route in routes:
        routeNames = Do(domain,Get(Qry(route).Pth('segments').Try(Pth('device').Pth('name'),Pth('connection').Pth('name'))))
        print("Route: %s"%('->'.join(routeNames)))
    