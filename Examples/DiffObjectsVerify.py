'''
PyEOQ Example: MergeObjects.py
----------------------------------------------------------------------------------------
This example shows how to merge the changes of a second model in a first model by using 
the MRG command.

See EOQ User Manual for more information: https://gitlab.com/eoq/doc

2020 Bjoern Annighoefer
'''
import os,sys,inspect,shutil
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 

from eoq2.mdb.pyecore import PyEcoreWorkspaceMdbProvider,PyEcoreMdbAccessor,PyEcoreIdCodec
from eoq2.domain.local import LocalMdbDomain

from eoq2 import Get,Mrg,Cpr,Clo,Crn,Rem,Add,CloModes #import GET and MRG command
from eoq2 import Cmp,Qry,His,Cls,Pth,Arr,Idx #import query segments

from eoq2.util import NoLogging,FileLogger,ConsoleLogger,ConsoleAndFileLogger,LogLevels,DEFAULT_LOG_LEVELS

from timeit import default_timer as timer #used to time the command's execution time.
      
'''
MAIN: Execution starts here
'''            
if __name__ == '__main__':
    # Initialize an WorkspaceMDB
    # logger = ConsoleLogger()
    # logger = ConsoleAndFileLogger()
    logger = FileLogger()
    mdbProvider = PyEcoreWorkspaceMdbProvider('Workspace',metaDir=['./Meta']) #./Meta must contain oaam.ecore
    valueCodec = PyEcoreIdCodec() #create the default value codec that converts objects to ids
    mdbAccessor = PyEcoreMdbAccessor(mdbProvider.GetMdb(),valueCodec)
    domain = LocalMdbDomain(mdbAccessor,logger=logger)
    mdbProvider.CoupleWithDomain(domain, valueCodec)
    
    #Get the merge Subdir:
    print("Getting merge dir... ",end="")
    start = timer()
    mergeDir = domain.Do( Get(Pth('subdirectories').Sel(Pth('name').Equ('Merge')).Idx(0)) )
    print("ok. (%f s)"%(timer()-start))
    
    #get the original file
    original = domain.Do( Get(Qry(mergeDir).Pth('resources').Sel(Pth('name').Equ("MinimalFlightControlChanged.oaam")).Idx(0).Pth('contents').Idx(0)) )
    
    
    #Diff test 1: Diff two identical files
    print("Diff test 1: changed vs merge2: ")
    resultFile = "Merge1.oaam"
    print("Cloning original... ",end="")
    start = timer()
    originalClone = domain.Do( Clo(original,CloModes.FUL))
    print("ok. (%f s)"%(timer()-start))
    
    same = domain.Do( Get(Qry(mergeDir).Pth('resources').Sel(Pth('name').Equ("Merge2.oaam")).Idx(0).Pth('contents').Idx(0))  )
    
    print("Diff... ",end="")
    start = timer()
    domain.Do( Cpr(originalClone,same) )
    print("ok. (%f s)"%(timer()-start))
    

    #Diff test 2: Diff two different files
    print("Diff test 2: changed vs merge1: ")
    resultFile = "Merge2.oaam"
    print("Cloning original... ",end="")
    start = timer()
    originalClone = domain.Do( Clo(original,CloModes.FUL))
    print("ok. (%f s)"%(timer()-start))
    
    changed = domain.Do( Get(Qry(mergeDir).Pth('resources').Sel(Pth('name').Equ("Merge1.oaam")).Idx(0).Pth('contents').Idx(0))  )
    
    print("Diff... ",end="")
    start = timer()
    domain.Do( Cpr(originalClone,changed) )
    print("ok. (%f s)"%(timer()-start))
        
    
    
