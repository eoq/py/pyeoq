"""
Author: Jona Dallmann (student Assistant at Institute of Aircraft Systems ILS at University of Stuttgart)
Date: 30.01.2020
This Module should automate the testing of the PyEoq2.

Usage is the following:
- first use the static method Configure() to configure the UnitTestClass()
- second create as many instances of UnitTestClass() to test specific commands
- at least use the static method CreateResults() to get the test results and evaluation of the test

--> for further documentation please have a look at the docstrings of the specific functions
"""
from eoq2.mdb.pyecore import PyEcoreWorkspaceMdbProvider, PyEcoreMdbAccessor, PyEcoreIdCodec
from eoq2.domain.local import LocalMdbDomain

from eoq2.action.externalpy import ExternalPyScriptHandler
from eoq2 import Get, Set, Add, Rem, Mov, Clo, Crt, Crn, Sts, Chg, Gaa, Cal, Asc, Abc, Cmp, CloModes, Seg, Ino, Not, Neq, Les, Gre
from eoq2 import Qry, Obj, His, Cls, Ino, Met, Idx, Pth, Arr, Set, Gmm, Umm, Rmm, Sts, Hel, Ses, Gby, Obs
from eoq2.serialization import JsonSerializer, TextSerializer, PySerializer, JsSerializer
from eoq2.event import EvtTypes

from eoq2.util import Backupper, NoLogging, ConsoleLogger, ConsoleAndFileLogger, LogLevels, DEFAULT_LOG_LEVELS, \
    EoqError

from shutil import copyfile
from os.path import exists

from time import time, process_time_ns, perf_counter_ns
from datetime import datetime
from os import getcwd, remove, mkdir, path
from sys import exit


class UnitTestClass:
    """
    This is a simple class for testing commands of the PyEoq2 it stores the relevant test input and outputs
    """
    # variables used for every test case
    domain = None
    serializer = None
    classVarCurrentCommandNumber=0
    testTimeStart = None
    testResultList = []  # saves information and result of all performed commands
    warningsList = []  # list to store warnings when problems appear
    eventRecordingQueue = []  # store event id so domain specific commands like UBS and OBS can be tested
    useEventRecordingQueue = False  # make the use of the above event queue controllable

    # variables for general settings, it is recommended to set them with the Configure method before creating the first UnitTestClass object
    useResultGenerator = False  # variable to generate a template with the test results for later reuse, set this to true
    # will overwrite the settings below and they will have no effect
    resultTemplateFile = None  # sets a result template to read results from
    logTestResultsToFile = False  # variable to create a .txt file in the current dir which contains the results of the test

    # variables used with the setting resultTemplateFile = True
    resultsListFromResultTemplate = []

    # variables used with the setting useResultGenerator = True
    resultGeneratorResultsList=[]

    # this static method gives the option to run the whole test as one command line command
    @staticmethod
    def RunUnitTest(modelfile='MinimalFlightControl.oaam', useResultGenerator=False, useResultTemplateFile=True,
                    logTestResultsToFile=False):
        """
        This method basically uses all the functions of the UnitTestClass to make the whole test executable with one
        command line command.
                The following settings can be set but are predefined (setting can be seen with '()'):
        * modelfile = the file to perform the test on, default is 'MinimalFlightControl.oaam' since it has no content related
                      to any business partners
        * useResultGeneratur = True/(False), uses the test to generate a testResultTemplate in the current dir which can be
                               used later for testing. The commands will not be evaluated during this test. Using this will
                               ignore the above settings.
        * useResultTemplateFile = (False)/True, use a resultTemplateFile created with the above useResultGenerator
                               Setting. This will ignore the hardcoded command results in the code and instead use the ones
                               of the given test result template file.
        * logTestResultsToFile = True/(False), simply activates a logger which will save the test results in a .txt file in the
                                 log folder

        The input variables resultTemplateFile, logTestResultsToFile and resultTemplateFile will be checked if they are valid,
        otherwise they will be set to their default value.
        """

        # IMPORTANT: This program will use the EoqUserManual (version 14.01.2020) as orientation to perform unittests on
        # PyEoq2 and ensure functionality. It will roughly use the examples given in the documentation.

        # reset all class variables at new call of the function to prevent case in which the previous results led to some bugs
        if UnitTestClass.domain is not None:
            UnitTestClass.domain=None
            UnitTestClass.serializer=None
            UnitTestClass.classVarCurrentCommandNumber=0
            UnitTestClass.testTimeStart=None
            UnitTestClass.testResultList=[]
            UnitTestClass.warningsList=[]
            UnitTestClass.useResultGenerator=False
            UnitTestClass.resultTemplateFile=None
            UnitTestClass.resultsListFromResultTemplate=[]
            UnitTestClass.resultGeneratorResultsList=[]

        # Basic configuration
        workspaceDir='./Workspace'
        metaModelDir='./Meta'  # relative to the workspace
        logDir='./log'

        # copy .oaam file to prevent it from corruption during the test
        copiedModelFile = modelfile[0:-5]+'UnitTest.oaam'
        if exists(workspaceDir+'/'+copiedModelFile):
            remove(workspaceDir+'/'+copiedModelFile)
        copyfile(workspaceDir+'/'+modelfile, workspaceDir+'/'+copiedModelFile)

        # create a backup. This is only for testing purposes and not necessary (both lines needed)
        # backupper = Backupper([workspaceDir,logDir])
        # backupper.CreateBackup()

        # define loglevels (chose one of the following lines)
        # logLevels = DEFAULT_LOG_LEVELS
        logLevels = DEFAULT_LOG_LEVELS+["change", "transaction", "event"]
        # logLevels = DEFAULT_LOG_LEVELS+[LogLevels.DEBUG,"change","transaction","event"]

        # initialize logger. For the file based logger this must happen after the backup. (chose one of the following lines)
        logger = NoLogging()  # no output at all
        # logger = ConsoleLogger()  # only console output
        # logger = ConsoleAndFileLogger(logDir=logDir,activeLevels=logLevels) #console and file output

        # Create a model data base (MDB) (chose one of the following lines)
        # mdb = PyEcoreSingleFileMdb("workspace/MinimalFlightControl.oaam","workspace/.meta/oaam.ecore",saveTimeout=1.0,logger=logger)
        mdbProvider = PyEcoreWorkspaceMdbProvider(workspaceDir, metaDir=[metaModelDir], saveTimeout=1.0, logger=logger, trackFileChanges=False)

        # Create an encoding strategy for model based data (chose one of the following lines)
        # valueCodec = SimpleEObjectCodec()
        valueCodec = PyEcoreIdCodec()

        # Create an unique accessor to the data
        mdbAccessor = PyEcoreMdbAccessor(mdbProvider.GetMdb(), valueCodec)

        # Create a domain and couple it with the mdb provider
        domain=LocalMdbDomain(mdbAccessor, logger=logger)
        mdbProvider.CoupleWithDomain(domain, valueCodec)

        # Listen to all events of the domain (optional)
        domain.Observe(UnitTestClass.OnEvent)

        # Register external actions (optional)
        externalActionHandler = ExternalPyScriptHandler(domain.cmdRunner.callManager, './Workspace/Actions', logger=logger)

        # prepare serializer
        serializer3 = PySerializer()

        # set resultTemplateFile parameter if using the result template file is activated
        if useResultTemplateFile:
            resultTemplateFile = 'PyEoq_UnitTest_result_template.txt'
        else:
            resultTemplateFile = None

        # configure UnitTestClass for more details see help(UnitTestClass)
        UnitTestClass.Configure(domain, serializer3, useResultGenerator, resultTemplateFile, logTestResultsToFile)

        UnitTestClass('Get() test with string', Get('test'), 'test')
        UnitTestClass('Qry() with element', Get(Qry()), '#0')
        UnitTestClass('Obj()', Get(Obj(12)), '#12')
        UnitTestClass('list() with different value ypes', Get([231, 'name', True, 3.14156, None, []]), [231, 'name', True, 3.14156, None, []])
        UnitTestClass('Arr()', Get(Arr(['first', 'second', 'third'])), ['first', 'third', 'second'])
        UnitTestClass('Not()', Get(Obj(0).Not()), False)
        UnitTestClass('Not() operator with array', Get(Arr([True, False]).Not()), [False, True])
        UnitTestClass('Pth()', Get(Obj(0).Pth('name')), '.')
        # this command is a command which should fail to test the function, also produces non supressable warning in console
        UnitTestClass('test fail-check function of UnitTestClass', Get(Pth('test')), '', True)
        UnitTestClass('Equ()', Get(Obj(0).Pth('name').Equ(Obj(0).Pth('name'))), True)
        UnitTestClass("Met('SIZE')", Get(Arr(['first', 'second', 'third']).Met('SIZE')), 3)

        # testing all variations of the index command
        UnitTestClass('Idx()', Get(Arr(['first', 'second', 'third']).Idx(0).Equ('first')), True)
        UnitTestClass('Idx(FLATTEN)', Get(Arr([1, 2, [2, 3, []], [12, 12]]).Idx('FLATTEN')), [1, 2, 2, 3, 12, 12])
        UnitTestClass('Idx(SORTASC)', Get(Arr([[1.1, 5, 2, 6], [100, 4, 3000, 12]]).Idx('SORTASC')), [[1.1, 2, 5, 6], [4, 12, 100, 3000]])
        UnitTestClass('Idx(SORTDSC)', Get(Arr([[1.1, 5, 2, 6], [100, 4, 3000, 12]]).Idx('SORTDSC')), [[6, 5, 2, 1.1], [3000, 100, 12, 4]])
        UnitTestClass('Idx(SIZE) mixed Array', Get(Arr([1, 2, [2, 3, []], [12, 12]]).Idx('SIZE')), 4)
        UnitTestClass('Idx(SIZE) Array of Arrays', Get(Arr([[1, 2, 3], [1, 2, 3]]).Idx('SIZE')), [3, 3])
        UnitTestClass('Idx() Range', Get(Arr([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]).Idx([1, 6, 2])), [2, 4, 6])

        UnitTestClass('Zip()', Get(Arr([1, 2, 3]).Zip([Arr([1, 2, 3]), Arr([1, 2, 3])])),[[1, 1], [2, 2], [3, 3]])
        UnitTestClass('Any() with different element types',
                      Get(Arr([[2, 5, 7, 8], [0, 0, 0, 0], [1.2, 12.4, 'a']]).Any([31, 2, 6, 'a'])), [True, False, True])
        UnitTestClass('All()',
                      Get(Arr([[2, 5, 7, None], [0.1, 't', 0, 0], [1.2, 12.4, True, None, 6.2, 'a', 'test', 1]]).
                          All([True, None, 6.2, 'a', 1])), [False, False, True])
        UnitTestClass('Eqa()', Get(Arr([2, 5, None, 8, 'a', 6.9]).Eqa([31, 2, 6.9, 'a', None])), [True, False, True, False, True, True])
        UnitTestClass('Neq()', Get(Arr([1, 2, 3, 4]).Neq([1, 2, 3, 5])), [False, False, False, True])
        UnitTestClass('Les(), True', Get(Arr([1]).Les(Arr([10]))), "[True]")
        UnitTestClass('Les(), False', Get(Arr([10]).Les(Arr([1]))), "[False]")
        UnitTestClass('Gre(), False', Get(Arr([1]).Gre(Arr([10]))), "[False]")
        UnitTestClass('Gre(), True', Get(Arr([10]).Gre(Arr([1]))), "[True]")

        # all variations of the ADD operator
        UnitTestClass('ADD(INT)', Get(Arr([3]).Add(Arr([1]))), [4])
        UnitTestClass('ADD(INT,STR)', Get(Arr(['test']).Add(Arr([1]))), 4, True)
        UnitTestClass('ADD(INT,FLO)', Get(Arr([3]).Add(Arr([0.5]))), [3.5])
        UnitTestClass('ADD(INT,-FLO)', Get(Arr([3]).Add(Arr([-0.5]))), [2.5])
        UnitTestClass('ADD(-INT,FLO)', Get(Arr([-3]).Add(Arr([0.5]))), [-2.5])
        UnitTestClass('ADD(-INT,-FLO)', Get(Arr([-3]).Add(Arr([-0.5]))), [-3.5])
        UnitTestClass('ADD(BOL) (True,True)', Get(Arr([True]).Add(Arr([True]))), "[True]")
        UnitTestClass('ADD(BOL) (True,False)', Get(Arr([True]).Add(Arr([False]))), "[True]")
        UnitTestClass('ADD(BOL) (False,False)', Get(Arr([False]).Add(Arr([False]))), "[False]")
        UnitTestClass('ADD(BOL) (False,True)', Get(Arr([False]).Add(Arr([True]))), "[True]")
        UnitTestClass('ADD(FLO)', Get(Arr([3.1]).Add(Arr([0.4]))), [3.5])
        UnitTestClass('ADD(STR)', Get(Arr(['conca']).Add(Arr(['tenate']))), "['concatenate']")
        UnitTestClass('ADD(OBJ,INT)', Get(Obj(1).Add(Arr([1]))), "[[#1, 1]]")
        UnitTestClass('ADD(OBJ,STR)', Get(Obj(1).Add(Arr(['test']))), "[[#1, 'test']]")
        UnitTestClass('ADD(OBJ)', Get(Obj(1).Add(Obj(1))), "[#1, #1]")

        # all variations of the ORR operator
        UnitTestClass('ORR(INT)', Get(Arr([3]).Orr(Arr([1]))), [4])
        UnitTestClass('ORR(INT,STR)', Get(Arr(['test']).Orr(Arr([1]))), 4, True)
        UnitTestClass('ORR(INT,FLO)', Get(Arr([3]).Orr(Arr([0.5]))), [3.5])
        UnitTestClass('ORR(INT,-FLO)', Get(Arr([3]).Orr(Arr([-0.5]))), [2.5])
        UnitTestClass('ORR(-INT,FLO)', Get(Arr([-3]).Orr(Arr([0.5]))), [-2.5])
        UnitTestClass('ORR(-INT,-FLO)', Get(Arr([-3]).Orr(Arr([-0.5]))), [-3.5])
        UnitTestClass('ORR(BOL) (True,True)', Get(Arr([True]).Orr(Arr([True]))), "[True]")
        UnitTestClass('ORR(BOL) (True,False)', Get(Arr([True]).Orr(Arr([False]))), "[True]")
        UnitTestClass('ORR(BOL) (False,False)', Get(Arr([False]).Orr(Arr([False]))), "[False]")
        UnitTestClass('ORR(BOL) (False,True)', Get(Arr([False]).Orr(Arr([True]))), "[True]")
        UnitTestClass('ORR(FLO)', Get(Arr([3.1]).Orr(Arr([0.4]))), [3.5])
        UnitTestClass('ORR(STR)', Get(Arr(['conca']).Orr(Arr(['tenate']))), "['concatenate']")
        UnitTestClass('ORR(OBJ,INT)', Get(Obj(1).Orr(Arr([1]))), "[[#1, 1]]")
        UnitTestClass('ORR(OBJ,STR)', Get(Obj(1).Orr(Arr(['test']))), "[[#1, 'test']]")
        UnitTestClass('ORR(OBJ)', Get(Obj(1).Orr(Obj(1))), "[#1, #1]")

        # all variations of the SUB operator
        UnitTestClass('SUB(INT)', Get(Arr([3]).Sub(Arr([1]))), [2])
        UnitTestClass('SUB(INT,STR)', Get(Arr(['test']).Sub(Arr([1]))), 4, True)
        UnitTestClass('SUB(INT,FLO)', Get(Arr([3]).Sub(Arr([0.5]))), [2.5])
        UnitTestClass('SUB(INT,-FLO)', Get(Arr([3]).Sub(Arr([-0.5]))), [3.5])
        UnitTestClass('SUB(-INT,FLO)', Get(Arr([-3]).Sub(Arr([0.5]))), [-3.5])
        UnitTestClass('SUB(-INT,-FLO)', Get(Arr([-3]).Sub(Arr([-0.5]))), [-2.5])
        UnitTestClass('SUB(BOL) (True,True)', Get(Arr([True]).Sub(Arr([True]))), "[False]")
        UnitTestClass('SUB(BOL) (True,False)', Get(Arr([True]).Sub(Arr([False]))), "[True]")
        UnitTestClass('SUB(BOL) (False,False)', Get(Arr([False]).Sub(Arr([False]))), "[False]")
        UnitTestClass('SUB(BOL) (False,True)', Get(Arr([False]).Sub(Arr([True]))), "[True]")
        UnitTestClass('SUB(FLO)', Get(Arr([3.1]).Sub(Arr([0.4]))), [2.7])
        UnitTestClass('SUB(OBJ,INT)', Get(Obj(1).Sub(Arr([1]))), "[[#1, 1]]")
        UnitTestClass('SUB(OBJ,STR)', Get(Obj(1).Sub(Arr(['test']))), "[[#1, 'test']]")
        UnitTestClass('SUB(OBJ)', Get(Obj(1).Sub(Obj(1))), "[#1, #1]")

        # all variations of the XOR operator
        UnitTestClass('XOR(INT)', Get(Arr([3]).Xor(Arr([1]))), [2])
        UnitTestClass('XOR(INT,STR)', Get(Arr(['test']).Xor(Arr([1]))), 4, True)
        UnitTestClass('XOR(INT,FLO)', Get(Arr([3]).Xor(Arr([0.5]))), [2.5])
        UnitTestClass('XOR(INT,-FLO)', Get(Arr([3]).Xor(Arr([-0.5]))), [3.5])
        UnitTestClass('XOR(-INT,FLO)', Get(Arr([-3]).Xor(Arr([0.5]))), [-3.5])
        UnitTestClass('XOR(-INT,-FLO)', Get(Arr([-3]).Xor(Arr([-0.5]))), [-2.5])
        UnitTestClass('XOR(BOL) (True,True)', Get(Arr([True]).Xor(Arr([True]))), "[False]")
        UnitTestClass('XOR(BOL) (True,False)', Get(Arr([True]).Xor(Arr([False]))), "[True]")
        UnitTestClass('XOR(BOL) (False,False)', Get(Arr([False]).Xor(Arr([False]))), "[False]")
        UnitTestClass('XOR(BOL) (False,True)', Get(Arr([False]).Xor(Arr([True]))), "[True]")
        UnitTestClass('XOR(FLO)', Get(Arr([3.1]).Xor(Arr([0.4]))), [2.7])
        UnitTestClass('XOR(OBJ,INT)', Get(Obj(1).Xor(Arr([1]))), "[[#1, 1]]")
        UnitTestClass('XOR(OBJ,STR)', Get(Obj(1).Xor(Arr(['test']))), "[[#1, 'test']]")
        UnitTestClass('XOR(OBJ)', Get(Obj(1).Xor(Obj(1))), "[#1, #1]")

        # all variants of the MUL operator
        UnitTestClass('MUL(INT)', Get(Arr([3]).Mul(Arr([1]))), [3])
        UnitTestClass('MUL(INT,STR)', Get(Arr(['test']).Mul(Arr([1]))), 4, True)
        UnitTestClass('MUL(INT,FLO)', Get(Arr([3]).Mul(Arr([0.5]))), [1.5])
        UnitTestClass('MUL(INT,-FLO)', Get(Arr([3]).Mul(Arr([-0.5]))), [-1.5])
        UnitTestClass('MUL(-INT,FLO)', Get(Arr([-3]).Mul(Arr([0.5]))), [-1.5])
        UnitTestClass('MUL(-INT,-FLO)', Get(Arr([-3]).Mul(Arr([-0.5]))), [1.5])
        UnitTestClass('MUL(BOL) (True,True)', Get(Arr([True]).Mul(Arr([True]))), "[True]")
        UnitTestClass('MUL(BOL) (True,False)', Get(Arr([True]).Mul(Arr([False]))), "[False]")
        UnitTestClass('MUL(BOL) (False,False)', Get(Arr([False]).Mul(Arr([False]))), "[False]")
        UnitTestClass('MUL(BOL) (False,True)', Get(Arr([False]).Mul(Arr([True]))), "[False]")
        UnitTestClass('MUL(FLO)', Get(Arr([3.1]).Mul(Arr([0.4]))), "[1.2400000000000002]")
        UnitTestClass('MUL(STR)', Get(Arr(['conca']).Mul(Arr(['tenate']))), "['concatenate']")
        UnitTestClass('MUL(OBJ,INT)', Get(Obj(1).Mul(Arr([1]))), "[[#1, 1]]")
        UnitTestClass('MUL(OBJ,STR)', Get(Obj(1).Mul(Arr(['test']))), "[[#1, 'test']]")
        UnitTestClass('MUL(OBJ)', Get(Obj(1).Mul(Obj(1))), "[#1, #1]")

        # all variants of the AND operator
        UnitTestClass('AND(INT)', Get(Arr([3]).And(Arr([1]))), [3])
        UnitTestClass('AND(INT,STR)', Get(Arr(['test']).And(Arr([1]))), 4, True)
        UnitTestClass('AND(INT,FLO)', Get(Arr([3]).And(Arr([0.5]))), [1.5])
        UnitTestClass('AND(INT,-FLO)', Get(Arr([3]).And(Arr([-0.5]))), [-1.5])
        UnitTestClass('AND(-INT,FLO)', Get(Arr([-3]).And(Arr([0.5]))), [-1.5])
        UnitTestClass('AND(-INT,-FLO)', Get(Arr([-3]).And(Arr([-0.5]))), [1.5])
        UnitTestClass('AND(BOL) (True,True)', Get(Arr([True]).And(Arr([True]))), "[True]")
        UnitTestClass('AND(BOL) (True,False)', Get(Arr([True]).And(Arr([False]))), "[False]")
        UnitTestClass('AND(BOL) (False,False)', Get(Arr([False]).And(Arr([False]))), "[False]")
        UnitTestClass('AND(BOL) (False,True)', Get(Arr([False]).And(Arr([True]))), "[False]")
        UnitTestClass('AND(FLO)', Get(Arr([3.1]).And(Arr([0.4]))), "[1.2400000000000002]")
        UnitTestClass('AND(STR)', Get(Arr(['conca']).And(Arr(['tenate']))), "['concatenate']")
        UnitTestClass('AND(OBJ,INT)', Get(Obj(1).And(Arr([1]))), "[[#1, 1]]")
        UnitTestClass('AND(OBJ,STR)', Get(Obj(1).And(Arr(['test']))), "[[#1, 'test']]")
        UnitTestClass('AND(OBJ)', Get(Obj(1).And(Obj(1))), "[#1, #1]")

        # all variants of the DIV operator
        UnitTestClass('DIV(INT)', Get(Arr([3]).Div(Arr([1]))), [3])
        UnitTestClass('DIV(INT,STR)', Get(Arr(['test']).Div(Arr([1]))), 4, True)
        UnitTestClass('DIV(INT,FLO)', Get(Arr([3]).Div(Arr([0.5]))), [6])
        UnitTestClass('DIV(INT,-FLO)', Get(Arr([3]).Div(Arr([-0.5]))), [-6])
        UnitTestClass('DIV(-INT,FLO)', Get(Arr([-3]).Div(Arr([0.5]))), [-6])
        UnitTestClass('DIV(-INT,-FLO)', Get(Arr([-3]).Div(Arr([-0.5]))), [6])
        UnitTestClass('DIV(BOL) (True,True)', Get(Arr([True]).Div(Arr([True]))), "[False]")
        UnitTestClass('DIV(BOL) (True,False)', Get(Arr([True]).Div(Arr([False]))), "[True]")
        UnitTestClass('DIV(BOL) (False,False)', Get(Arr([False]).Div(Arr([False]))), "[True]")
        UnitTestClass('DIV(BOL) (False,True)', Get(Arr([False]).Div(Arr([True]))), "[True]")
        UnitTestClass('DIV(FLO)', Get(Arr([3.1]).Div(Arr([0.4]))), [7.75])
        UnitTestClass('DIV(STR)', Get(Arr(['conca']).Div(Arr(['tenate']))), "['concatenate']")
        UnitTestClass('DIV(OBJ,INT)', Get(Obj(1).Div(Arr([1]))), "[[#1, 1]]")
        UnitTestClass('DIV(OBJ,STR)', Get(Obj(1).Div(Arr(['test']))), "[[#1, 'test']]")
        UnitTestClass('DIV(OBJ)', Get(Obj(1).Div(Obj(1))), "[#1, #1]")

        # all variants of the NAD operator
        UnitTestClass('NAD(INT)', Get(Arr([3]).Nad(Arr([1]))), [3])
        UnitTestClass('NAD(INT,STR)', Get(Arr(['test']).Nad(Arr([1]))), 4, True)
        UnitTestClass('NAD(INT,FLO)', Get(Arr([3]).Nad(Arr([0.5]))), [6])
        UnitTestClass('NAD(INT,-FLO)', Get(Arr([3]).Nad(Arr([-0.5]))), [-6])
        UnitTestClass('NAD(-INT,FLO)', Get(Arr([-3]).Nad(Arr([0.5]))), [-6])
        UnitTestClass('NAD(-INT,-FLO)', Get(Arr([-3]).Nad(Arr([-0.5]))), [6])
        UnitTestClass('NAD(BOL) (True,True)', Get(Arr([True]).Nad(Arr([True]))), "[False]")
        UnitTestClass('NAD(BOL) (True,False)', Get(Arr([True]).Nad(Arr([False]))), "[True]")
        UnitTestClass('NAD(BOL) (False,False)', Get(Arr([False]).Nad(Arr([False]))), "[True]")
        UnitTestClass('NAD(BOL) (False,True)', Get(Arr([False]).Nad(Arr([True]))), "[True]")
        UnitTestClass('NAD(FLO)', Get(Arr([3.1]).Nad(Arr([0.4]))), [7.75])
        UnitTestClass('NAD(STR)', Get(Arr(['conca']).Nad(Arr(['tenate']))), "['concatenate']")
        UnitTestClass('NAD(OBJ,INT)', Get(Obj(1).Nad(Arr([1]))), "[[#1, 1]]")
        UnitTestClass('NAD(OBJ,STR)', Get(Obj(1).Nad(Arr(['test']))), "[[#1, 'test']]")
        UnitTestClass('NAD(OBJ)', Get(Obj(1).Nad(Obj(1))), "[#1, #1]")

        # cross product
        UnitTestClass('Csp() (int)', Get(Arr([1, 2, 3]).Csp(Arr([1, 2]))), [[1, 1], [1, 2], [2, 1], [2, 2], [3, 1], [3, 2]])
        UnitTestClass('Csp() (mixed elements)',
                      Get(Arr([1.1, 2, 'a', Obj(1)]).Csp(Arr([None, True]))), "[[1.1, None], [1.1, True], [2, None], [2, True],['a', None],"
                                                                              "['a', True], [#1, None], [#1, True]]")
        # set operators
        # "True" and 1 , "False" and 0 are the same in python and not caught during the test. The case when these two are mixed in a value
        # is seen as uncritical but mentioned here.
        UnitTestClass('Its()', Get(Arr([1.1, 2, True, 0, None, 100, 'test', 'a', [1, 2, 3]]).Its(Arr([1.1, True, None, 'a', [1, 2, 3]]))),
                      [1.1, True, None, 'a', [1, 2, 3]])
        UnitTestClass('Dif()', Get(Arr([0, 1, None, 'error', 'a', 1.2]).Dif(Arr([1, 'a', False]))), [None, 'error', 1.2])
        UnitTestClass('Uni()', Get(Arr([1, 100, False, None, 'error', 'a', 1.2]).Uni(Arr([1, 100, 'a', False, [1, 2, 3]]))),
                      [False, 1, 100, None, 'error', 'a', 1.2, [1, 2, 3]])
        UnitTestClass('Con()', Get(Arr([0, 1, False, None, 'error', 'a', 1.2]).Con(Arr([1, 'a', False]))), "[0, 1, False, None, 'error', "
                                                                                                           "'a', 1.2, 1, 'a', False]")
        # select .oaam model for testing by testing Sel()
        selectorTest =\
            UnitTestClass('Sel()', Get(Qry().Pth('resources').Sel(Qry().Pth('name').Equ(copiedModelFile)).Idx(0).Pth('name')), copiedModelFile)

        # get root element of .oaam model if sel() command worked correctly and make difference between generator mode and
        # normal mode
        if (UnitTestClass.useResultGenerator and UnitTestClass.GetGeneratedResult(-1) == copiedModelFile) or selectorTest.wasSuccessful:
            root = domain.Do(Get(Pth('resources').Sel(Pth('name').Equ(copiedModelFile)).Idx(0).Pth('contents').Idx(0)))
        else:
            raise NameError('The test can not be continued since the root element of %s could not be found.' % copiedModelFile)

        ## define variables needed for later tests
        # get connections to compare when checking class command
        channelAConnectionNamesList = domain.Do(Get(Qry(root).Pth('hardware').Pth('subhardware').
                                                    Sel(Qry().Pth('name').Equ('Systems')).Idx(0).Pth('subhardware').Idx(0).Pth('subhardware').
                                                    Sel(Qry().Pth('name').Equ('ChannelA')).Idx(0).Pth('connections').Pth('name')))
        # define location object attribute names
        locationObjectAttributeNames = ['modified', 'style', 'traceLink', 'name', 'length', 'documentation', 'id', 'modifier']
        # define location object attribute names
        locationObjectReferenceNames = ['operationModes', 'attributes', 'ductOpenings', 'variants', 'position', 'requiredModifiers', 'type']
        # list of all metamodel names
        metaModelNamesList = ['ecore', 'workspacemdbmodel', 'xmlresourcemodel', 'oaam', 'common', 'library', 'scenario', 'systems',
                              'functions', 'hardware', 'anatomy', 'capabilities', 'restrictions', 'allocations']

        # test terminate command
        UnitTestClass('Trm() Terminate for null values',
                      Get(Qry(root).Pth('anatomy').Pth('subanatomies').Idx(0).Pth('locations').Idx(0).Pth('modified').Trm().Pth('name')), None)
        UnitTestClass('Trm() Terminate not',
                      Get(Qry(root).Pth('anatomy').Pth('subanatomies').Idx(0).Pth('locations').Idx(0).Pth('type').Trm().Pth('name')), 'NOSE-L')
        UnitTestClass('Trm() Terminate on custom condition with custom result',
                      Get(Qry(root).Pth('anatomy').Pth('subanatomies').Idx(0).Pth('locations').Trm(Met('SIZE').Equ(15), Met('SIZE')).Pth('name')), 15)
        UnitTestClass('Trm() Terminate on custom condition with custom result',
                      Get(Qry(root).Pth('anatomy').Pth('subanatomies').Idx(0).Pth('locations').Trm(Met('SIZE').Neq(15), Met('SIZE'))
                          .Idx([0, 3, 1]).Pth('name')), ['NOSE-L', 'NOSE-R', 'EBAY-L'])
        UnitTestClass('Trm() Terminate on custom condition with custom result on a list',
                      Get(Qry(root).Pth('anatomy').Pth('subanatomies').Idx(0).Pth('ducts').Pth('length').Idx([3, 7, 1]).Trm(Gre(2), 10)),
                      [2.0, 2.0, 10, 10])
        UnitTestClass('Trm() Terminate on custom condition with default result on a list',
                      Get(Qry(root).Pth('anatomy').Pth('subanatomies').Idx(0).Pth('ducts').Pth('length').Idx([3, 7, 1]).Trm(Gre(2))),
                      [2.0, 2.0, None, None])
        # get into model to a place where only a few objects of type "connection" exist and test class selector command
        UnitTestClass('Cls()',
                      Get(Qry(root).Pth('hardware').Pth('subhardware').Sel(Qry().Pth('name').Equ('Systems')).Idx(0).Pth('subhardware')
                          .Idx(0).Pth('subhardware').Sel(Qry().Pth('name').Equ('ChannelA')).Idx(0).Cls('Connection').Pth('name')),
                      channelAConnectionNamesList)
        # check get all elements of type device from the subhardware Flightcontrol
        UnitTestClass('Ino()', Get(Qry(root).Pth('hardware').Pth('subhardware').Idx(0).Pth('subhardware').Idx(0).Pth('devices').
                                   Ino('Device').Pth('name').Idx('FLATTEN')), ['StickCA', 'StickFO'])
        # Met operators
        UnitTestClass("Met('CLASS') by package and name",
                      Get(Met('CLASS', ['http://www.oaam.de/oaam/model/v140/functions', 'Task']).Pth('name')), 'Task')
        UnitTestClass("Met('CLASS') by object", Get(Qry(root).Met('CLASS').Pth('name')), 'Architecture')
        UnitTestClass("Met('CLASSNAME')", Get(Qry(root).Met('CLASSNAME')), 'Architecture')
        UnitTestClass("Met('CONTAINER')", Get(Qry(root).Pth('anatomy').Pth('subanatomies').Idx(0).Pth('locations').Idx(0).Met('CONTAINER').
                                              Pth('name')), 'Anatomy')
        UnitTestClass("Met('PARENT')", Get(Qry(root).Pth('anatomy').Pth('subanatomies').Idx(0).Pth('locations').Idx(0).Met('PARENT').
                                           Pth('name')), 'Anatomy')
        UnitTestClass("Met('ALLPARENTS')", Get(Qry(root).Pth('hardware').Pth('subhardware').Idx(0).Pth('subhardware').Idx(0).Pth('devices')
                                               .Idx(0).Met('ALLPARENTS').Pth('name')), ['.', 'MinimalFlightControlUnitTest.oaam', None,
                                                                                        None, 'Systems', 'FlightControl'])
        UnitTestClass("Met('ASSOCIATES')", Get(Qry(root).Pth('hardware').Pth('subhardware').Idx(0).Pth('subhardware').Idx(0).Pth('devices')
                                               .Idx(0).Pth('ios').Idx(0).Met('ASSOCIATES').Pth('name').Idx(0)),
                      'StickCA_connection_->StickCA Ch1 1')
        UnitTestClass("Met('CONTAININGFEATURE')", Get(Qry(root).Pth('hardware').Pth('subhardware').Idx(0).Met('CONTAININGFEATURE').
                                                      Pth('name')), 'subhardware')
        UnitTestClass("MET('INDEX')", Get(Qry(root).Pth('hardware').Pth('subhardware').Met('INDEX')), '[0, 1]')
        UnitTestClass("MET('INDEX')", Get(Qry(root).Met('INDEX')), '0')  # returns 1 because root is not a child within a multi-element feature
        UnitTestClass("Met('FEATURES')", Get(Qry(root).Pth('anatomy').Pth('subanatomies').Idx(0).Pth('locations').Idx(0)
                                             .Met('FEATURES').Pth('name')), locationObjectAttributeNames+locationObjectReferenceNames)
        UnitTestClass("Met('FEATURENAMES')",
                      Get(Qry(root).Pth('anatomy').Pth('subanatomies').Idx(0).Pth('locations').Idx(0).Met('FEATURENAMES')),
                      locationObjectAttributeNames+locationObjectReferenceNames)
        UnitTestClass("Met('FEATUREVALUES')",
                      Get(Qry(root).Pth('anatomy').Pth('subanatomies').Idx(0).Pth('locations').Idx(0)
                          .Met('FEATUREVALUES').Idx([3, 7, 1])), [1.0, [], 'NOSE-L', 'NOSE-L'])
        UnitTestClass("Met('ATTRIBUTES')",
                      Get(Qry(root).Pth('anatomy').Pth('subanatomies').Idx(0).Pth('locations').Idx(0).Met('ATTRIBUTES').Pth('name')),
                      locationObjectAttributeNames)
        UnitTestClass("Met('ATTRIBUTENAMES')",
                      Get(Qry(root).Pth('anatomy').Pth('subanatomies').Idx(0).Pth('locations').Idx(0).Met('ATTRIBUTENAMES')),
                      locationObjectAttributeNames)
        UnitTestClass("Met('ATTRIBUTEVALUES')",
                      Get(Qry(root).Pth('anatomy').Pth('subanatomies').Idx(0).Pth('locations').Idx(0).Met('ATTRIBUTEVALUES')),
                      ['NOSE-L', None, None, None, 'NOSE-L', 1.0, '', ''])
        UnitTestClass("Met('REFERENCES')",
                      Get(Qry(root).Pth('anatomy').Pth('subanatomies').Idx(0).Pth('locations').Idx(0).Met('REFERENCES').Pth('name')),
                      locationObjectReferenceNames)
        UnitTestClass("Met('REFERENCENAMES')",
                      Get(Qry(root).Pth('anatomy').Pth('subanatomies').Idx(0).Pth('locations').Idx(0).Met('REFERENCENAMES')),
                      locationObjectReferenceNames)
        UnitTestClass("Met('REFERNCEVALUES')",
                      Get(Qry(root).Pth('anatomy').Pth('subanatomies').Idx(0).Pth('locations').Idx(0).Met('REFERENCEVALUES').Pth('name').Idx('FLATTEN')),
                      ['NOSE-L', 'from_NOSE-L_to_NOSE-R', 'from_NOSE-L_to_EBAY-L', None])
        UnitTestClass("Met('CONTAINMENTS')", Get(Qry(root).Pth('anatomy').Pth('subanatomies').Idx(0).Pth('locations').Idx(0).Met('CONTAINMENTS').Pth('name')),
                      ['attributes', 'ductOpenings', 'position', 'operationModes'])
        UnitTestClass("Met('CONTAINMENTNAMES')", Get(Qry(root).Pth('anatomy').Pth('subanatomies').Idx(0).Pth('locations').Idx(0).
                                                     Met('CONTAINMENTNAMES')), ['attributes', 'ductOpenings', 'position', 'operationModes'])
        UnitTestClass("Met('CONTAINMENTVALUES')", Get(Qry(root).Pth('anatomy').Pth('subanatomies').Idx(0).Pth('locations').Idx(0).
                                                      Met('CONTAINMENTVALUES').Pth('name')),
                      [None, [], [], ['from_NOSE-L_to_NOSE-R', 'from_NOSE-L_to_EBAY-L']])
        UnitTestClass("Met('METAMODELS')", Get(Qry(root).Pth('anatomy').Pth('subanatomies').Idx(0).Pth('locations').Idx(0).
                                               Met('METAMODELS').Pth('name')), metaModelNamesList)
        UnitTestClass("Met('PACKAGE')", Get(Qry(root).Pth('anatomy').Pth('subanatomies').Idx(0).Pth('locations').Idx(0).
                                            Met('CLASS').Met('PACKAGE').Pth('name')), 'anatomy')
        UnitTestClass("Met('SUPERTYPES')", Get(Qry(root).Pth('anatomy').Pth('subanatomies').Idx(0).Pth('locations').Idx(0).Met('CLASS').
                                               Met('SUPERTYPES').Pth('name')), ['ResourceProviderInstanceA', 'OaamBaseElementA',
                                                                                'VariantDependentElementA', 'ModeDependentElementA'])
        UnitTestClass("Met('ALLSUPERTYPES')", Get(Qry(root).Pth('anatomy').Pth('subanatomies').Idx(0).Pth('locations').Idx(0)
                                                  .Met('CLASS').Met('ALLSUPERTYPES').Pth('name')), ['ResourceProviderInstanceA',
                                                                                                    'OaamBaseElementA',
                                                                                                    'VariantDependentElementA',
                                                                                                    'ModeDependentElementA'])
        # below command results looks like a big mess but this is wanted and needed
        UnitTestClass("Met('IMPLEMENTERS')",
                      Cmp().Get(Met('CLASS',['http://www.oaam.de/oaam/model/v140/common','OaamBaseElementA']))
                      .Get(His(0).Met('IMPLEMENTERS').Pth('name')),"[#96, ['Architecture', 'AttributeA', 'BoolOperation', 'BoolNot',"
                                                                   "'DataTypeA', 'LibraryContainerA', 'ResourceType', 'Resource',"
                                                                   "'ResourceAlternatives', 'ResourceBundle', 'TaskType', 'SignalType',"
                                                                   "'DeviceType', 'ConnectionType', 'LocationType', 'DuctType', 'WireType',"
                                                                   "'IoType', 'InputDeclaration', 'OutputDeclaration', 'IoDeclaration',"
                                                                   "'DuctOpeningDeclaration', 'ResourceGroup', 'DeviceTypeSymmetry',"
                                                                   "'IoGroup', 'AttributeDefinition', 'FaultPropagation', 'TaskInputState',"
                                                                   "'PowerSource', 'ResourceLink', 'ResourceTypeModifier',"
                                                                   "'ResourceTypeModifierLevel', 'ResourceTypeModifierReference',"
                                                                   "'TaskTypeDissimilarity', 'DeviceTypeDissimilarity',"
                                                                   "'ResourceTypeDissimilarity', 'TaskOutputTrigger', 'TaskInputTrigger',"
                                                                   "'TaskStateDeclaration', 'TaskParameterDeclaration', 'ScenarioContainerA',"
                                                                   " 'OperationMode', 'ScenarioParameterNumeric', 'ScenarioParameterBool',"
                                                                   "'Variant', 'OperationModeReference', 'SystemsContainerA', 'System',"
                                                                   "'InformationFlow', 'InformationSignal', 'InformationMaterial',"
                                                                   "'InformationPower', 'InputSegregation', 'FunctionsContainerA', 'Task',"
                                                                   "'ExternalTaskLink', 'TaskGroup', 'TaskSymmetry', 'TaskRedundancy',"
                                                                   "'FailureCondition', 'OutputIntegrityState', 'Signal', 'SignalGroup',"
                                                                   "'Input', 'Output', 'ExternalOutputLink', 'TaskParameter',"
                                                                   "'HardwareContainerA', 'Device', 'Connection', 'Io', 'DeviceSymmetry',"
                                                                   "'AnatomyContainerA', 'Location', 'Area', 'Duct', 'DuctOpening',"
                                                                   "'Position3D', 'LocationSymmetry', 'AreaSymmetry',"
                                                                   "'CapabilitiesContainerA', 'TaskOnDeviceCapability',"
                                                                   "'DeviceInLocationCapability', 'ConnectionInDuctOrLocationCapability',"
                                                                   "'SignalOnConnectionOrDeviceCapability', 'SubdeviceInDeviceCapability',"
                                                                   "'SubconnectionInDeviceCapability', 'ResourceConsumption',"
                                                                   "'RestrictionsContainerA', 'LocationRestriction', 'AreaRestriction',"
                                                                   "'PowerSourceRestriction', 'DeviceRestriction', 'DeviceTypeRestriction',"
                                                                   "'ConnectionTypeRestriction', 'ConnectionRestriction',"
                                                                   "'SegregationRestriction', 'SynchronicityRestriction',"
                                                                   "'TaskAtomicRestriction', 'TaskSymmetryRestriction',"
                                                                   "'TimeDelayRestriction', 'AllocationsContainerA', 'TaskAssignment',"
                                                                   "'SignalAssignment', 'ConnectionAssignment', 'SignalAssignmentSegment',"
                                                                   "'DeviceAssignment', 'ConnectionAssignmentSegment', 'SubdeviceAssignment',"
                                                                   " 'SubconnectionAssignment', 'Schedule', 'ScheduledTime']]")
        UnitTestClass("Met('ALLIMPLEMENTERS')",
                      Cmp().Get(Met('CLASS',['http://www.oaam.de/oaam/model/v140/common','OaamBaseElementA']))
                      .Get(His(0).Met('ALLIMPLEMENTERS').Pth('name')),
                      "[#96, ['Architecture', 'AttributeA', 'AttributeString', 'AttributeNumeric', 'AttributeContainment',"
                      "'AttributeReference', 'BoolOperation', 'BoolNot', 'DataTypeA', 'Integer', 'Array', 'Struct', 'FloatingPoint', 'Byte',"
                      " 'Character', 'Boolean', 'LibraryContainerA', 'ResourceType', 'Resource', 'ResourceAlternatives', 'ResourceBundle',"
                      "'TaskType', 'SignalType', 'DeviceType', 'ConnectionType', 'LocationType', 'DuctType', 'WireType', 'IoType',"
                      "'InputDeclaration', 'OutputDeclaration', 'IoDeclaration', 'DuctOpeningDeclaration', 'ResourceGroup',"
                      "'DeviceTypeSymmetry', 'IoGroup', 'AttributeDefinition', 'FaultPropagation', 'TaskInputState', 'PowerSource',"
                      "'ResourceLink', 'ResourceTypeModifier', 'ResourceTypeModifierLevel', 'ResourceTypeModifierReference',"
                      "'TaskTypeDissimilarity', 'DeviceTypeDissimilarity', 'ResourceTypeDissimilarity', 'Sublibrary', 'TaskOutputTrigger',"
                      "'TaskInputTrigger', 'TaskStateDeclaration', 'TaskParameterDeclaration', 'Library', 'ScenarioContainerA',"
                      "'OperationMode', 'ScenarioParameterNumeric', 'ScenarioParameterBool', 'Variant', 'OperationModeReference','Scenario',"
                      " 'Subscenario', 'SystemsContainerA', 'Systems', 'Subsystem', 'System', 'InformationFlow', 'InformationSignal',"
                      "'InformationMaterial', 'InformationPower', 'ElectricPower', 'HydraulicPower', 'RotaryPower', 'LinearPower',"
                      "'InputSegregation', 'Functions', 'FunctionsContainerA', 'Task', 'ExternalTaskLink', 'TaskGroup', 'TaskSymmetry',"
                      "'TaskRedundancy', 'FailureCondition', 'OutputIntegrityState', 'Signal', 'SignalGroup', 'Input', 'Output',"
                      "'ExternalOutputLink', 'Subfunctions', 'TaskParameter', 'HardwareContainerA', 'Device', 'Connection', 'Io',"
                      "'DeviceSymmetry', 'Hardware', 'Subhardware', 'AnatomyContainerA', 'Location', 'Area', 'Duct', 'DuctOpening',"
                      "'Position3D', 'LocationSymmetry', 'AreaSymmetry', 'Subanatomy', 'Anatomy', 'CapabilitiesContainerA',"
                      "'TaskOnDeviceCapability', 'DeviceInLocationCapability', 'ConnectionInDuctOrLocationCapability',"
                      "'SignalOnConnectionOrDeviceCapability', 'SubdeviceInDeviceCapability', 'SubconnectionInDeviceCapability',"
                      "'ResourceConsumption', 'Capabilities', 'Subcapabilities', 'RestrictionsContainerA', 'Restrictions',"
                      "'LocationRestriction', 'AreaRestriction', 'PowerSourceRestriction', 'DeviceRestriction', 'DeviceTypeRestriction',"
                      "'ConnectionTypeRestriction', 'ConnectionRestriction', 'SegregationRestriction', 'SynchronicityRestriction',"
                      "'TaskAtomicRestriction', 'TaskSymmetryRestriction', 'TimeDelayRestriction', 'Subrestrictions', 'AllocationsContainerA',"
                      " 'TaskAssignment', 'SignalAssignment', 'ConnectionAssignment', 'SignalAssignmentSegment', 'DeviceAssignment',"
                      "'ConnectionAssignmentSegment', 'SubdeviceAssignment', 'SubconnectionAssignment', 'Allocations', 'Suballocations',"
                      "'Schedule', 'ScheduledTime']]")

        # control flow operation
        UnitTestClass("Met('IF') (True)", Get(Qry(root).Met('IF', [True, Arr([1]), Arr([2])])), [1])
        UnitTestClass("Met('IF') (False)", Get(Qry(root).Met('IF', [False, Arr([1]), Arr([2])])), [2])

        # additional tests to catch some examples of the eoq user manual
        UnitTestClass('Arr() obtain special elements of result',
                      Get(Qry(root).Pth('hardware').Pth('subhardware').Idx(0).Pth('subhardware').Idx(0).Pth('connections').Cls('Connection')
                          .Idx('FLATTEN').Arr([Idx(2), Idx(7)]).Pth('name')),['Rudder1_connection_->Rudder1', 'Rudder2_connection_->Rudder2'])
        UnitTestClass('Any()',
                      Get(Qry(root).Pth('hardware').Pth('subhardware').Idx(0).Pth('subhardware').Idx(0).Pth('connections').
                          Sel(Qry().Pth('startingPoints').Pth('name').Any(Arr(['P_ElevatorL2_device_ElevatorL2_io','P_AileronL1_device_AileronL1_io']))).
                                   Pth('name')), ['AileronL1_connection_->AileronL1', 'ElevatorL2_connection_->ElevatorL2'])

        # get test element to perform next tests on
        testElements = domain.Do(Get(Qry(root).Pth('hardware').Pth('subhardware').Idx(0).Pth('subhardware').Idx(0)))

        # the following tests will change the .oaam model
        # Set command
        UnitTestClass('Set() a single feature', Set(Qry(testElements).Pth('connections').Idx(0), 'name', 'test'), ignoreResult=True)
        UnitTestClass("Check Set() a single feature", Get(Qry(root).Pth('hardware').Pth('subhardware').Idx(0).Pth('subhardware').Idx(0).
                                                          Pth('connections').Idx(0).Pth('name')), 'test')
        UnitTestClass("Set() multiple features to a single value (name)",
                      Set(Qry(root).Pth('hardware').Pth('subhardware').Idx(0).Pth('subhardware').Idx(0).Pth('connections').Idx(1),
                          ['name', 'style'], 'test'), ignoreResult=True)
        UnitTestClass("Check Set() multiple features to a single value (name)",
                      Get(Qry(testElements).Pth('connections').Idx(1).Pth('name')), 'test')
        UnitTestClass("Check Set() multiple features to a single value (style)",
                      Get(Qry(testElements).Pth('connections').Idx(1).Pth('style')), 'test')

        # Domain interaction Commands
        UnitTestClass('Sts() obtain latest change id', Sts(), 2)
        UnitTestClass('Hel() initiate session', Hel('Egon', 'pa$$wort'), ignoreResult=True)
        sessionId = domain.Do(Hel('Egon', 'pa$$wort'))
        UnitTestClass('Ses() with real id', Ses(sessionId), True)
        UnitTestClass('Ses() with non existing ID', Ses('sessionID'), mustFail=True)
        UnitTestClass('Gby() close session with real id', Gby(sessionId), True)
        UnitTestClass('Gby() close session with no valid id', Gby('sessionId'), mustFail=True)
        # parallel to the change command the OBS() command is tested
        sessionId = domain.Do(Hel('Egon', 'pa$$wort'))
        UnitTestClass.ChangeEventRecording()  # activate event recording to check wether the obs command worked
        domain.Observe(UnitTestClass.OnEvent, sessionId=sessionId)
        UnitTestClass('Obs() with User Egon and password Pa$$word', Cmp().Ses(sessionId).Obs('CHG', '*'),[True,True])
        UnitTestClass('Chg() Get all changes', Chg(0,0),
                      "[[0, 'SET', #241, 'name', 'test', 'AileronR1_connection_->AileronR1', None, None, None, 196, '', 0],"
                      "[1, 'SET', #242, 'name', 'test', 'ElevatorR1_connection_->ElevatorR1', None, None, None, 198, '', 0],"
                      "[2, 'SET', #242, 'style', 'test', None, None, None, None, 198, '', 0]]")
        UnitTestClass('Chg() get specific change',
                      Chg(0,1), "[[0, 'SET', #241, 'name', 'test', 'AileronR1_connection_->AileronR1', None, None, None, 196, '', 0]]")
        UnitTestClass('Chg() get specific changes',
                      Chg(0,2), "[[0, 'SET', #241, 'name', 'test', 'AileronR1_connection_->AileronR1', None, None, None, 196, '', 0],"
                                "[1, 'SET', #242, 'name', 'test', 'ElevatorR1_connection_->ElevatorR1', None, None, None, 198, '', 0]]")
        UnitTestClass('Chg() get specific change out of range', Chg(4, 1), [])
        UnitTestClass('Chg() get specific changes out of range', Chg(4, 2), [])
        UnitTestClass('Chg() get specific changes in and out of range',
                      Chg(2, 1), "[[2, 'SET', #242, 'style', 'test', None, None, None, None, 198, '', 0]]")
        UnitTestClass('Set() multiple features to multiple values',
                      Set(Qry(root).Pth('hardware').Pth('subhardware').Idx(0).Pth('subhardware').Idx(0).Pth('connections').Idx(2),
                          ['name', 'style'], ['test', 'test1']), ignoreResult=True)
        UnitTestClass("Check Set() multiple features to multiple values (name)",
                      Get(Qry(testElements).Pth('connections').Idx(2).Pth('name')), 'test')
        UnitTestClass("Check Set() multiple features to multiple values (style)",
                      Get(Qry(testElements).Pth('connections').Idx(2).Pth('style')), 'test1')
        UnitTestClass('Set() feature of multiple targets to single value',
                      Set(Qry(root).Pth('hardware').Pth('subhardware').Idx(0).Pth('subhardware').Idx(0).Pth('connections').Idx([3, 5, 1]),
                          'name', 'test'), ignoreResult=True)
        UnitTestClass("Check Set() feature of multiple targets to single value (name)",
                      Get(Qry(testElements).Pth('connections').Idx([3, 5, 1]).Pth('name')),
                      ['test', 'test'])
        UnitTestClass('Set() feature of multiple targets to multiple values',
                      Set(Qry(root).Pth('hardware').Pth('subhardware').Idx(0).Pth('subhardware').Idx(0).Pth('connections').Idx([3, 5, 1]),
                          'style', ['a', 'b']), ignoreResult=True)
        UnitTestClass("Check Set() feature of multiple targets to multiple values (style)",
                      Get(Qry(testElements).Pth('connections').Idx([3, 5, 1]).Pth('style')), ['a', 'b'])
        UnitTestClass("Set() multiple features of multiple targets to single value",
                      Set(Qry(root).Pth('hardware').Pth('subhardware').Idx(0).Pth('subhardware').Idx(0).Pth('connections').Idx([3, 5, 1]),
                          ['name', 'id'], 'c'), ignoreResult=True)
        UnitTestClass("Check Set() multiple features of multiple targets to single value (name)",
                      Get(Qry(testElements).Pth('connections').Idx([3, 5, 1]).Pth('name')), ['c', 'c'])
        UnitTestClass("Check Set() multiple features of multiple targets to single value (id)",
                      Get(Qry(testElements).Pth('connections').Idx([3, 5, 1]).Pth('id')), ['c', 'c'])
        UnitTestClass("Set() multiple features of multiple targets to multiple values",
                      Set(Qry(root).Pth('hardware').Pth('subhardware').Idx(0).Pth('subhardware').Idx(0).Pth('connections').Idx(Arr([3, 5, 1])),
                          ['name', 'id'], [['e', 'f'], ['g', 'h']]), ignoreResult=True)
        UnitTestClass("Check Set() multiple features of multiple targets to multiple values (name)",
                      Get(Qry(testElements).Pth('connections').Idx([3, 5, 1]).Pth('name')), ['e', 'g'])
        UnitTestClass("Check Set() multiple features of multiple targets to multiple values (id)",
                      Get(Qry(testElements).Pth('connections').Idx([3, 5, 1]).Pth('id')), ['f', 'h'])
        UnitTestClass("Check Obs() command", Get(UnitTestClass.GetElementTypesOfEventRecordingQueue()), "{'CHG'}")
        UnitTestClass.ClearEventRecording()  # clear event queue of UnitTestClass
        UnitTestClass("Check Ubs() command", Cmp().Ses(sessionId).Ubs('CHG','*'), [True,True])

        # Cmp() trailing commands
        UnitTestClass('Cmp() multiple commands', Cmp().Get(Qry(testElements).Pth('connections').Idx(5).Pth('name')).
                      Get(Qry(testElements).Pth('connections').Idx(5).Pth('style')),['AileronR2_connection_->AileronR2', None])

        # His() history
        UnitTestClass('His(-1) of Set() Command', Cmp().Set(Qry(testElements).Pth('connections').Idx(0), 'name', 'testHistory').
                      Set(Qry(testElements).Pth('connections').Idx(0), 'name', 'egonWalter').Set(
            Qry(testElements).Pth('connections').Idx(0), 'name', 'egonWalter').Get(His(-1)),
                      "[[#241, 'name', 'testHistory'], [#241, 'name', 'egonWalter'], [#241, 'name', 'egonWalter'], [#241, 'name', 'egonWalter']]")
        UnitTestClass('His(0) of Set() Command',
                      Cmp().Set(Qry(testElements).Pth('connections').Idx(0), 'name', 'testStage').Set(
                          Qry(testElements).Pth('connections').Idx(0), 'name', 'egonWalter').Get(His(0)),
                      "[[#241, 'name', 'testStage'], [#241, 'name', 'egonWalter'], [#241, 'name', 'testStage']]")
        UnitTestClass('His(1) of Set() Command',
                      Cmp().Set(Qry(testElements).Pth('connections').Idx(0), 'name', 'testHistory').Set(
                          Qry(testElements).Pth('connections').Idx(0), 'name', 'egonWalter').Get(His(1)),
                      "[[#241, 'name', 'testHistory'], [#241, 'name', 'egonWalter'], [#241, 'name', 'egonWalter']]")

        # get new elements for testing
        testElements = domain.Do(Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Idx('FLATTEN').Idx(0)))
        listOfFlightControlTaskNames = domain.Do(Get(Qry(testElements).Pth('tasks').Pth('name')))
        listOfFlightControlTaskObjects = domain.Do(Get(Qry(testElements).Pth('tasks')))

        # Rem() and Add()
        connectionNamesBeforeRemove = domain.Do(Get(Qry(testElements).Pth('tasks').Pth('name')))
        UnitTestClass('Rem() single element from feature',
                      Cmp().Get(Qry(testElements).Pth('tasks').Sel(Pth('name').Equ(listOfFlightControlTaskNames[-1]))).Rem(Qry(testElements),
                      'tasks', His(-1)), ignoreResult=True)
        UnitTestClass('Check Rem() of element from single Feature',
                      Get(Qry(testElements).Pth('tasks').Sel(Qry().Pth('name').Equ(listOfFlightControlTaskNames[-1]))), [])
        UnitTestClass('Add() deleted element from before command', Add(Qry(testElements), 'tasks', listOfFlightControlTaskObjects[-1]),
                      ignoreResult=True)
        UnitTestClass('Check Add() of element to single feature',
                      Get(Qry(testElements).Pth('tasks').Sel(Qry().Pth('name').Equ(listOfFlightControlTaskNames[-1])).Pth('name').
                          Equ(listOfFlightControlTaskNames[-1]).Idx(0)), True)
        UnitTestClass('Rem() multiple elements from single feature', Rem(Qry(testElements), 'tasks', listOfFlightControlTaskObjects[2:5]),
                      ignoreResult=True)
        UnitTestClass('Check Rem() of multiple elements from single feature',
                      Get(Arr(listOfFlightControlTaskNames).Dif(Qry(testElements).Pth('tasks').Pth('name'))), listOfFlightControlTaskNames[2:5])
        UnitTestClass('Add() deleted elements from before command', Add(Qry(testElements), 'tasks', listOfFlightControlTaskObjects[2:5]),
                      ignoreResult=True)
        UnitTestClass('Check Add() multiple elements to single feature', Get(Qry(testElements).Pth('tasks').Pth('name')), listOfFlightControlTaskNames)

        # define further variables for tests
        functionFlightControlSubfunctionsRoot = domain.Do(Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Idx('FLATTEN').Idx(0)))
        functionsChannelARoot = domain.Do(Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions')
                                                        .Pth('subfunctions').Sel(Qry().Pth('name').Equ('ChannelA')).Idx('FLATTEN').Idx(0)))
        functionsChannelATaskNames = domain.Do(Get(Qry(functionsChannelARoot).Pth('tasks').Pth('name')))
        functionsChannelATaskObjects = domain.Do(Get(Qry(functionsChannelARoot).Pth('tasks')))
        functionsChannelASignalObjects = domain.Do(Get(Qry(functionsChannelARoot).Pth('signals')))
        functionsChannelASignalNames = domain.Do(Get(Qry(functionsChannelARoot).Pth('signals').Pth('name')))
        functionsChannelBRoot = domain.Do(Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions')
                                                        .Pth('subfunctions').Sel(Qry().Pth('name').Equ('ChannelB')).Idx('FLATTEN').Idx(0)))
        functionsChannelBTaskNames = domain.Do(Get(Qry(functionsChannelBRoot).Pth('tasks').Pth('name')))
        functionsChannelBTaskObjects = domain.Do(Get(Qry(functionsChannelBRoot).Pth('tasks')))

        UnitTestClass('Rem() single elements from multiple features',
                      Rem(Qry(functionsChannelARoot), ['tasks', 'signals'], [[functionsChannelATaskObjects[-1]], [functionsChannelASignalObjects[-1]]]), ignoreResult=True)
        UnitTestClass('Check Rem() single elements from multiple features',
                      Cmp().Get(Qry(functionsChannelARoot).Pth('signals').Sel(Pth('name').Equ(functionsChannelASignalNames[-1])))
                      .Get(Qry(functionsChannelARoot).Pth('tasks').Sel(Pth('name').Equ(functionsChannelATaskNames[-1]))), [[], []])
        UnitTestClass('Add() single elements to multiple features to undo above command',
                      Add(Qry(functionsChannelARoot), ['tasks', 'signals'], [[functionsChannelATaskObjects[-1]], [functionsChannelASignalObjects[-1]]]), ignoreResult=True)
        UnitTestClass('Check Add() single elements from multiple features',
                      Cmp().Get(Qry(functionsChannelARoot).Pth('signals').Sel(Pth('name').Equ(functionsChannelASignalNames[-1])).Pth('name'))
                      .Get(Qry(functionsChannelARoot).Pth('tasks').Sel(Pth('name').Equ(functionsChannelATaskNames[-1])).Pth('name')),
                      [[functionsChannelASignalNames[-1]], [functionsChannelATaskNames[-1]]])
        UnitTestClass('Rem() multiple elements from multiple features',
                      Rem(Qry(functionsChannelARoot), ['tasks', 'signals'], [functionsChannelATaskObjects[-2:], functionsChannelASignalObjects[-2:]]), ignoreResult=True)
        UnitTestClass('Check Rem() multiple elements from multiple features',
                      Cmp().Get(Arr(functionsChannelATaskNames).Dif(Qry(functionsChannelARoot).Pth('tasks').Pth('name')).Equ(Arr(functionsChannelATaskNames[-2:])))
                      .Get(Arr(functionsChannelASignalNames).Dif(Qry(functionsChannelARoot).Pth('signals').Pth('name')).Equ(Arr(functionsChannelASignalNames[-2:]))),
                      [[True,True], [True,True]])
        UnitTestClass('Add() multiple elements to multiple features',
                      Add(Qry(functionsChannelARoot), ['tasks', 'signals'], [functionsChannelATaskObjects[-2:], functionsChannelASignalObjects[-2:]]), ignoreResult=True)
        UnitTestClass('Check Add() multiple elements from multiple features',
                      Cmp().Get(Arr(functionsChannelATaskNames).Dif(Qry(functionsChannelARoot).Pth('tasks').Pth('name')))
                      .Get(Arr(functionsChannelASignalNames).Dif(Qry(functionsChannelARoot).Pth('signals').Pth('name'))),
                      [[],[]])

        # create Variables to work with for multi feature/element use
        connectionObjectsList = domain.Do(Get(Qry(root).Pth('hardware').Pth('subhardware').Pth('subhardware').Pth('connections').Idx('FLATTEN')))
        connectionObjectNamesList = domain.Do(Get(Arr(connectionObjectsList).Pth('name')))
        newIoObjectsList = domain.Do(Crn('http://www.oaam.de/oaam/model/v140/hardware', 'Io', 3))
        domain.Do(Cmp().Set(newIoObjectsList[0], 'name', 'newIo0').Set(newIoObjectsList[1], 'name', 'newIo1').Set(newIoObjectsList[2], 'name', 'newIo2'))
        newIoObjectNamesList = domain.Do(Get(Arr(newIoObjectsList).Pth('name')))

        UnitTestClass('Add() a single element to multiple features',
                       Add(connectionObjectsList[0], ['startingPoints','endPoints'], newIoObjectsList[0]), ignoreResult=True)
        UnitTestClass('Check Add() a single element to multiple features (startingPoints)',
                      Get(Arr(connectionObjectsList).Sel(Pth('name').Equ(connectionObjectNamesList[0])).Pth('startingPoints')
                          .Sel(Pth('name').Equ(newIoObjectNamesList[0])).Pth('name').Idx('FLATTEN')), [newIoObjectNamesList[0]])
        UnitTestClass('Check Add() a single element to multiple features (endPoints)',
                      Get(Arr(connectionObjectsList).Sel(Pth('name').Equ(connectionObjectNamesList[0])).Pth('endPoints')
                          .Sel(Pth('name').Equ(newIoObjectNamesList[0])).Pth('name').Idx('FLATTEN')), [newIoObjectNamesList[0]])
        UnitTestClass('Rem() a single element from multiple features',
                      Cmp().Get(Qry(connectionObjectsList[0]).Pth('endPoints').Sel(Pth('name').Equ(newIoObjectNamesList[0])).
                                Idx(0)).Rem(connectionObjectsList[0], ['startingPoints','endPoints'], His(0)), ignoreResult=True)
        UnitTestClass('Check Rem() a single element to multiple features (startingPoints)',
                      Get(Arr(connectionObjectsList).Sel(Pth('name').Equ(connectionObjectNamesList[0])).Pth('startingPoints')
                          .Sel(Pth('name').Equ(newIoObjectNamesList[0])).Pth('name').Idx('FLATTEN')), [])
        UnitTestClass('Check Rem() a single element to multiple features (endPoints)',
                      Get(Arr(connectionObjectsList).Sel(Pth('name').Equ(connectionObjectNamesList[0])).Pth('endPoints')
                          .Sel(Pth('name').Equ(newIoObjectNamesList[0])).Pth('name').Idx('FLATTEN')), [])
        UnitTestClass('Add() a single element to a single feature of multiple targets',
                      Add(Arr(connectionObjectsList[:2]), 'startingPoints', newIoObjectsList[0]), ignoreResult=True)
        UnitTestClass('Check Add() a single element to a single feature of multiple targets',
                      Get(Arr(connectionObjectsList[:2]).Pth('startingPoints').Sel(Pth('name').Equ(newIoObjectNamesList[0]))
                          .Pth('name')), [[newIoObjectNamesList[0]], [newIoObjectNamesList[0]]])
        UnitTestClass('Rem() a single element to a single feature of multiple targets',
                      Rem(Arr(connectionObjectsList[:2]), 'startingPoints', newIoObjectsList[0]), ignoreResult=True)
        UnitTestClass('Check rem() a single element to a single feature of multiple targets',
                      Get(Arr(connectionObjectsList[:2]).Pth('startingPoints').Sel(Pth('name').Equ(newIoObjectNamesList[0]))
                          .Pth('name')), [[], []])
        UnitTestClass('Add() single elements to a single feature of multiple targets',
                      Add(Arr(connectionObjectsList[:2]), 'endPoints', [[newIoObjectsList[0]], [newIoObjectsList[1]]]), ignoreResult=True)
        UnitTestClass('Check Add() single elements to a single feature of multiple targets (0)',
                      Get(Qry(connectionObjectsList[0]).Pth('endPoints').Sel(Pth('name').Equ(newIoObjectNamesList[0])).Pth('name')), ['newIo0'])
        UnitTestClass('Check Add() single elements to a single feature of multiple targets (1)',
                      Get(Qry(connectionObjectsList[1]).Pth('endPoints').Sel(Pth('name').Equ(newIoObjectNamesList[1])).Pth('name')), ['newIo1'])
        UnitTestClass('Rem() single elements from a single feature of multiple targets',
                      Rem(Arr(connectionObjectsList[:2]), 'endPoints', Arr([newIoObjectsList[:1], newIoObjectsList[1:2]])), ignoreResult=True)
        UnitTestClass('Check Rem() single elements to a single feature of multiple targets (0)',
                      Get(Qry(connectionObjectsList[0]).Pth('endPoints').Sel(Pth('name').Equ(newIoObjectNamesList[0]))), [])
        UnitTestClass('Check Rem() single elements to a single feature of multiple targets (1)',
                      Get(Qry(connectionObjectsList[1]).Pth('endPoints').Sel(Pth('name').Equ(newIoObjectNamesList[1]))), [])
        UnitTestClass('Add() multiple elements to a single feature of multiple targets',
                      Add(Arr(connectionObjectsList[:2]), 'endPoints', [newIoObjectsList[:2], newIoObjectsList[:2]]), ignoreResult=True)
        UnitTestClass('Check Add() multiple elements to a single feature of multiple targets',
                      Get(Arr(connectionObjectsList[:2]).Pth('endPoints').Pth('name')), [newIoObjectNamesList[:2], newIoObjectNamesList[:2]])
        UnitTestClass('Rem() multiple elements from a single feature of multiple targets',
                      Rem(Arr(connectionObjectsList[:2]), 'endPoints', [newIoObjectsList[:2], newIoObjectsList[:2]]), ignoreResult=True)
        UnitTestClass('Check Rem() multiple elements from a single feature of multiple targets',
                      Get(Arr(connectionObjectsList[:2]).Pth('endPoints').Pth('name')), [[], []])
        UnitTestClass('Add() multiple elements to multiple features of multiple targets',
                      Add(Arr(connectionObjectsList[:2]), ['endPoints', 'startingPoints'],
                          [[newIoObjectsList[:2], newIoObjectsList[:2]], [newIoObjectsList[:2], []]]), ignoreResult=True)
        UnitTestClass('Check Add() multiple elements to multiple features of multiple targets (endPoints)',
                      Get(Arr(connectionObjectsList[:2]).Pth('endPoints').Pth('name')), [newIoObjectNamesList[:2], newIoObjectNamesList[:2]])
        UnitTestClass('Check Add() multiple elements to multiple features of multiple targets (startingPoints)',
                      Get(Arr(connectionObjectsList[:2]).Pth('startingPoints').Pth('name')), [['P_AileronR1_device_AileronR1_io', 'newIo0', 'newIo1'],
                                                                                              ['P_ElevatorR1_device_ElevatorR1_io']])
        UnitTestClass('Rem() multiple elements from multiple features of multiple targets',
                      Rem(Arr(connectionObjectsList[:2]), ['endPoints', 'startingPoints'],
                          [[newIoObjectsList[:2], newIoObjectsList[:2]], [newIoObjectsList[:2], []]]), ignoreResult=True)
        UnitTestClass('Check Rem() multiple elements to multiple features of multiple targets (endPoints)',
                      Get(Arr(connectionObjectsList[:2]).Pth('endPoints').Pth('name')), [[], []])
        UnitTestClass('Check Rem() multiple elements to multiple features of multiple targets (startingPoints)',
                      Get(Arr(connectionObjectsList[:2]).Pth('startingPoints').Pth('name')), [['P_AileronR1_device_AileronR1_io'],
                                                                                              ['P_ElevatorR1_device_ElevatorR1_io']])
        UnitTestClass('Add() a single element to multiple features of multiple targets',
                      Add(Arr(connectionObjectsList[:2]), ['endPoints', 'startingPoints'], newIoObjectsList[0]), ignoreResult=True)
        UnitTestClass('Check Add() a single element to multiple features of multiple targets (endPoints)',
                      Get(Arr(connectionObjectsList[:2]).Pth('endPoints').Sel(Pth('name').Equ(newIoObjectNamesList[0])).Pth('name')),
                      [[newIoObjectNamesList[0]], [newIoObjectNamesList[0]]])
        UnitTestClass('Check Add() a single element to multiple features of multiple targets (startingPoints)',
                      Get(Arr(connectionObjectsList[:2]).Pth('startingPoints').Sel(Pth('name').Equ(newIoObjectNamesList[0])).Pth('name')),
                      [[newIoObjectNamesList[0]], [newIoObjectNamesList[0]]])
        UnitTestClass('Rem() a single element to multiple features of multiple targets',
                      Rem(Arr(connectionObjectsList[:2]), ['endPoints', 'startingPoints'], newIoObjectsList[0]), ignoreResult=True)
        UnitTestClass('Check Rem() a single element to multiple features of multiple targets (endPoints)',
                      Get(Arr(connectionObjectsList[:2]).Pth('endPoints').Sel(Pth('name').Equ(newIoObjectNamesList[0])).Pth('name')),
                      [[], []])
        UnitTestClass('Check Rem() a single element to multiple features of multiple targets (startingPoints)',
                      Get(Arr(connectionObjectsList[:2]).Pth('startingPoints').Sel(Pth('name').Equ(newIoObjectNamesList[0])).Pth('name')),
                      [[], []])

        # Mov() command
        UnitTestClass('Mov() a single object', Mov(listOfFlightControlTaskObjects[0], 4), ignoreResult=True)
        UnitTestClass('check Mov() a single object', Get(Qry(testElements).Pth('tasks').Idx(4).Pth('name').
                                                          Equ(listOfFlightControlTaskNames[0])), True)
        UnitTestClass('Mov() multiple objects to the same index', Mov(Arr([functionsChannelATaskObjects[0],
                                                                            functionsChannelBTaskObjects[0]]), 4), ignoreResult=True)
        UnitTestClass('check Mov() multiple objects to the same index',
                      Get(Qry(functionFlightControlSubfunctionsRoot).Pth('subfunctions').Pth('tasks').Idx(4).Pth('name').
                          Equ(Arr([functionsChannelATaskNames[0], functionsChannelBTaskNames[0]]))), [True, True])
        domain.Do(Cmp().Mov(Arr([listOfFlightControlTaskObjects[0], functionsChannelATaskObjects[0], functionsChannelBTaskObjects[0]]), 0))  # undo changes
        UnitTestClass('Mov() multiple objects to different indices', Mov(Arr(functionsChannelATaskObjects[0:4]), Arr([3,2,1,0])), ignoreResult=True)
        UnitTestClass('check Mov() multiple objects to different indices',
                      Cmp().Get(Qry(functionsChannelARoot).Pth('tasks').Idx(0).Equ(functionsChannelATaskObjects[3]))
                      .Get(Qry(functionsChannelARoot).Pth('tasks').Idx(1).Equ(functionsChannelATaskObjects[2]))
                      .Get(Qry(functionsChannelARoot).Pth('tasks').Idx(2).Equ(functionsChannelATaskObjects[1]))
                      .Get(Qry(functionsChannelARoot).Pth('tasks').Idx(3).Equ(functionsChannelATaskObjects[0])), [True,True,True,True])
        domain.Do(Mov(Arr(functionsChannelATaskObjects[0:4]), Arr([0,1,2,3])))  # undo changes

        # Clo() command
        UnitTestClass('Clo() single object (ATT)',
                      Cmp().Clo(functionsChannelBTaskObjects[0], 'ATT').Add(Qry(functionsChannelARoot), 'tasks', His(-1)), ignoreResult=True)
        UnitTestClass('check Clo() single object attributes',
                      Get(Arr([functionsChannelBTaskObjects[0]]).Met('FEATUREVALUES').
                          Dif(Qry(functionsChannelARoot).Pth('tasks').Sel(Pth('name').Equ(Qry(functionsChannelBTaskObjects[0]).Pth('name')))
                          .Met('FEATUREVALUES')).Idx('FLATTEN').Pth('name')), ['Stick', 'P_StickCA_device_StickCA_in'])
        domain.Do(Rem(Qry(functionsChannelARoot), 'tasks', Qry(functionsChannelARoot).Pth('tasks').Sel(Pth('name').Equ(functionsChannelBTaskNames[0]))))  # undo changes
        UnitTestClass('Clo() multiple objects (ATT)', Cmp().Clo(Arr(functionsChannelATaskObjects[0:3]),'ATT')
                                                      .Add(Qry(functionsChannelBRoot), 'tasks', His(-1)), ignoreResult=True)
        UnitTestClass('Check Clo() multiple objects (ATT)',
                      Cmp().Get(Qry(functionsChannelBRoot).Pth('tasks').Dif(Arr(functionsChannelBTaskObjects)).Met('FEATUREVALUES')
                                .Dif(Arr(functionsChannelATaskObjects[0:3]).Met('FEATUREVALUES')).Idx('FLATTEN')), [[]])
        domain.Do(Rem(Qry(functionsChannelBRoot), 'tasks', Qry(functionsChannelBRoot).Pth('tasks').Sel(Pth('name').Equ(functionsChannelATaskNames[0]))))  # undo changes
        domain.Do(Rem(Qry(functionsChannelBRoot), 'tasks', Qry(functionsChannelBRoot).Pth('tasks').Sel(Pth('name').Equ(functionsChannelATaskNames[1]))))  # undo changes
        domain.Do(Rem(Qry(functionsChannelBRoot), 'tasks', Qry(functionsChannelBRoot).Pth('tasks').Sel(Pth('name').Equ(functionsChannelATaskNames[2]))))  # undo changes
        UnitTestClass('Clo() single object (DEP)',
                      Cmp().Clo(functionsChannelBTaskObjects[0], 'DEP').Add(Qry(functionsChannelARoot), 'tasks', His(-1)), ignoreResult=True)
        UnitTestClass('check Clo() single object (DEP)',
                      Cmp().Get(Qry(functionsChannelBTaskObjects[0]).Pth('inputs').Idx(0).Neq(Qry(functionsChannelARoot).Pth('tasks').
                                                                                              Sel(Pth('name').Equ(functionsChannelBTaskNames[0])).Pth('inputs').Idx(0)))
                      .Get(Qry(functionsChannelBTaskObjects[0]).Pth('inputs').Idx(0).Met('FEATUREVALUES')
                           .Dif(Qry(functionsChannelARoot).Pth('tasks').Sel(Pth('name').Equ(functionsChannelBTaskNames[0])).
                                Pth('inputs').Idx(0).Idx(0).Met('FEATUREVALUES'))), "[[True], []]")
        domain.Do(Rem(Qry(functionsChannelARoot), 'tasks', Qry(functionsChannelARoot).Pth('tasks').Sel(Pth('name').Equ(functionsChannelBTaskNames[0]))))  # undo changes
        UnitTestClass('Clo() multiple objects (DEP)', Cmp().Clo(Arr(functionsChannelBTaskObjects[0:2]), 'DEP')
                      .Add(Qry(functionsChannelARoot), 'tasks', His(-1)), ignoreResult=True)
        UnitTestClass('Check first of Clo() multiple objects (DEP)', Cmp().Get(Qry(functionsChannelBTaskObjects[0]).Pth('inputs').Idx(0).Neq(Qry(functionsChannelARoot).Pth('tasks').
                                                                                              Sel(Pth('name').Equ(functionsChannelBTaskNames[0]))))
                      .Get(Qry(functionsChannelBTaskObjects[0]).Pth('inputs').Idx(0).Met('FEATUREVALUES')
                           .Dif(Qry(functionsChannelARoot).Pth('tasks').Sel(Pth('name').Equ(functionsChannelBTaskNames[0])).
                                Pth('inputs').Idx(0).Idx(0).Met('FEATUREVALUES'))), "[[True], []]")
        UnitTestClass('Check second of Clo() multiple objects (DEP)', Cmp().Get(Qry(functionsChannelBTaskObjects[0]).Pth('inputs').Idx(0).Neq(Qry(functionsChannelARoot).Pth('tasks').
                                                                                              Sel(Pth('name').Equ(functionsChannelBTaskNames[0]))))
                      .Get(Qry(functionsChannelBTaskObjects[1]).Pth('inputs').Idx(0).Met('FEATUREVALUES')
                           .Dif(Qry(functionsChannelARoot).Pth('tasks').Sel(Pth('name').Equ(functionsChannelBTaskNames[1])).
                                Pth('inputs').Idx(0).Idx(0).Met('FEATUREVALUES'))), "[[True], []]")
        domain.Do(Rem(Qry(functionsChannelARoot), 'tasks', Qry(functionsChannelARoot).Pth('tasks').Sel(Pth('name').Equ(functionsChannelBTaskNames[0]))))
        domain.Do(Rem(Qry(functionsChannelARoot), 'tasks', Qry(functionsChannelARoot).Pth('tasks').Sel(Pth('name').Equ(functionsChannelBTaskNames[1]))))
        UnitTestClass('Clo() single object (FUL)', Clo(root, 'FUL'), ignoreResult=True)
        UnitTestClass('check Clo() single object (FUL) partially with task object',
                      Get(Obj(327).Pth('functions').Pth('subfunctions').Pth('subfunctions').Idx(0).Pth('tasks').Idx(0).Met('FEATUREVALUES').
                          Equ(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Idx(0).Pth('tasks').Idx(0).Met('FEATUREVALUES'))),
                      [[False, [False], [], True, True, True, True, [], True, True, True, [], True, True, True, True, [], []]])
        UnitTestClass('check Clo() single object (FUL) partially with device object',
                      Get(Obj(327).Pth('hardware').Pth('subhardware').Idx(0).Pth('subhardware').Pth('devices').Idx(0).Met('FEATUREVALUES').
                          Equ(Qry(root).Pth('hardware').Pth('subhardware').Idx(0).Pth('subhardware').Pth('devices').Idx(0).Met('FEATUREVALUES'))),
                      [[False, [False, False, False, False], [], False, [], [], [], True, True, True, [], True, True, True, True, [], []]])
        # clo multiple object with Full missing because there are now two different objects to test the clone
        UnitTestClass('Clo() multiple objects with different modes', Cmp().Clo(Arr([functionsChannelBTaskObjects[0], functionsChannelBTaskObjects[1]]),
                                                                         ['ATT','DEP']).Add(Qry(functionsChannelARoot), 'tasks', His(-1)),
                      ignoreResult=True)
        UnitTestClass('Check Clo() multiple objects with different modes (ATT)', Get(Arr([functionsChannelBTaskObjects[0]]).Met('FEATUREVALUES').
                          Dif(Qry(functionsChannelARoot).Pth('tasks').Sel(Pth('name').Equ(Qry(functionsChannelBTaskObjects[0]).Pth('name')))
                          .Met('FEATUREVALUES')).Idx('FLATTEN').Pth('name')), ['Stick', 'P_StickCA_device_StickCA_in'])
        UnitTestClass('Check Clo() multiple objects with different modes (DEP)',
                      Cmp().Get(Qry(functionsChannelBTaskObjects[1]).Pth('inputs').Idx(0).
                                Neq(Qry(functionsChannelARoot).Pth('tasks').Sel(Pth('name').Equ(functionsChannelBTaskNames[1])).Pth('inputs').Idx(0)))
                      .Get(Qry(functionsChannelBTaskObjects[1]).Pth('inputs').Idx(0).Met('FEATUREVALUES')
                           .Dif(Qry(functionsChannelARoot).Pth('tasks').Sel(Pth('name').Equ(functionsChannelBTaskNames[1])).
                                Pth('inputs').Idx(0).Idx(0).Met('FEATUREVALUES'))), "[[True], []]")

        # Crn create by name
        UnitTestClass('Crn() single object',
                      Cmp().Crn('http://www.oaam.de/oaam/model/v140/functions', 'Task', 1).Set(His(-1), 'name', 'TestName').
                      Add(Qry(functionsChannelARoot), 'tasks', His(-2)), ignoreResult=True)
        UnitTestClass('check Crn() single object', Get(Qry(functionsChannelARoot).Pth('tasks').Sel(Pth('name').Equ('TestName')).Met('CLASSNAME')), ['Task'])
        domain.Do(Rem(Qry(functionsChannelARoot), 'tasks', Qry(functionsChannelARoot).Pth('tasks').Sel(Pth('name').Equ('TestName'))))
        UnitTestClass('Crn() multiple objects',
                      Cmp().Crn('http://www.oaam.de/oaam/model/v140/functions', 'Task', 10).Set(His(-1), 'name', 'TestNames').
                      Add(Qry(functionsChannelARoot), 'tasks', His(-2)), ignoreResult=True)
        UnitTestClass('check Crn() multiple objects',
                      Get(Qry(functionsChannelARoot).Pth('tasks').Sel(Pth('name').Equ('TestNames')).Met('CLASSNAME')),
                      ['Task', 'Task', 'Task', 'Task', 'Task', 'Task', 'Task', 'Task', 'Task', 'Task'])
        domain.Do(Rem(Qry(functionsChannelARoot), 'tasks', Qry(functionsChannelARoot).Pth('tasks').Sel(Pth('name').Equ('TestNames'))))
        UnitTestClass('Crn() EClass Object and check Crn() EClass Object',
                      Cmp().Crn('http://www.eclipse.org/emf/2002/Ecore','EClass', 1, ['testClass']).Get(His(-1).Pth('name')),
                      "[#374, 'testClass']")

        # Crt() create
        UnitTestClass('Crt() single new object', Cmp().Crt(Arr(functionsChannelATaskObjects).Idx(0).Met('CLASS'), 1)
                      .Set(His(0), 'name', 'testTask').Add(Qry(functionsChannelARoot), 'tasks', His(0)), ignoreResult=True)
        UnitTestClass('check Crt() single new object',
                      Get(Qry(functionsChannelARoot).Pth('tasks').Sel(Pth('name').Equ('testTask')).Pth('name')), ['testTask'])
        domain.Do(Rem(Qry(functionsChannelARoot), 'tasks',
                      Qry(functionsChannelARoot).Pth('tasks').Sel(Pth('name').Equ('testTask'))))  # undo changes
        UnitTestClass('Crt() single new object with array as argument',
                      Cmp().Crt(Arr(functionsChannelATaskObjects).Idx(0).Met('CLASS'), 1, Arr(['test']))
                      .Set(His(0), 'name', 'testTask1').Add(Qry(functionsChannelARoot), 'tasks', His(0)), ignoreResult=True)
        UnitTestClass('check Crt() single new object',
                      Get(Qry(functionsChannelARoot).Pth('tasks').Sel(Pth('name').Equ('testTask1')).Pth('name')), ['testTask1'])
        domain.Do(Rem(Qry(functionsChannelARoot), 'tasks',
                      Qry(functionsChannelARoot).Pth('tasks').Sel(Pth('name').Equ('testTask1'))))  # undo changes
        UnitTestClass('Crt() new multiple objects', Cmp().Crt(Arr(functionsChannelATaskObjects).Idx(0).Met('CLASS'), 5).Set(His(0), 'name', 'testTask2').
                      Add(Qry(functionsChannelARoot), 'tasks', His(-2)), ignoreResult=True)
        UnitTestClass('check Crt() new multiple objects',
                      Get(Qry(functionsChannelARoot).Pth('tasks').Sel(Pth('name').Equ('testTask2')).Pth('name')),
                      ['testTask2', 'testTask2', 'testTask2', 'testTask2', 'testTask2'])
        domain.Do(Rem(Qry(functionsChannelARoot), 'tasks',
                      Qry(functionsChannelARoot).Pth('tasks').Sel(Pth('name').Equ('testTask2'))))  # undo changes

        # Action related commands
        UnitTestClass('Gaa() obtain the list of actions and its definition',
                      Gaa(), "[['abortTest', [['seconds', 'Real', 1, 1, '1.0', 'The sleep time in seconds', []]], [], None, ['misc', 'test']], "
                      "['helloworld', [['times', 'Integer', 1, 1, '', '', []]], [['return', 'String', 1, 1, '', '', []]], None, ['misc', 'test']], "
                      "['sum', [['arg1', 'Integer', 1, 1, '', '', []], ['arg2', 'Integer', 1, 1, '', '', []]], [['return', 'returns sum', 1, 1, '', '', []]], None, ['misc', 'test']], "
                      "['test', [], [['return', 'Nothing is returned', 1, 1, '', '', []]], None, ['misc', 'test']]]")
        UnitTestClass('Cal() action with no argument', Cal('test', []), "[0, None, 'FIN', [['STDOUT', 'Test Print']]")
        UnitTestClass('Cal() action with one argument', Cal('helloworld', [1]), [1, 'I printed 1 times Hello world!', 'FIN', []])
        UnitTestClass('Cal() action with two arguments', Cal('sum', [1,2]), "[2, 3, 'FIN', []]")
        UnitTestClass('Asc() action with no argument', Asc('test', []), 3)
        UnitTestClass('Asc() action with one argument', Asc('helloworld', [1]), 4)
        UnitTestClass('Asc() action with two arguments', Asc('sum', [1,2]), 5)
        UnitTestClass('Asc() start action for later abort', Asc('abortTest', [100]), 6)
        UnitTestClass('Abc() abort previous call', Abc(6), True)

        # Metamodel Commands
        UnitTestClass('Gmm() obtain list of all metamodels and check', Cmp().Gmm().Get(His(-1).Pth('name')),
                      "[[#80, #81, #82, #83, #84, #85, #86, #87, #88, #89, #90, #91, #92, #93], ['ecore', 'workspacemdbmodel',"
                      "'xmlresourcemodel', 'oaam', 'common', 'library', 'scenario', 'systems', 'functions', 'hardware', 'anatomy',"
                      "'capabilities', 'restrictions', 'allocations']]")
        UnitTestClass('Umm() unregister package as metamodell', Umm(Obj(90)), True)
        UnitTestClass('Check Umm() unregister package as metamodell',
                      Crn('http://www.oaam.de/oaam/model/v140/functions', 'Task', 1), mustFail=True)
        UnitTestClass('Rmm() register a new package as a metamodel to undo previous command', Rmm(Obj(90)), True)
        UnitTestClass('Check Rmm() register a new package as a metamodel to undo previous command',
                      Crn('http://www.oaam.de/oaam/model/v140/functions', 'Task', 1), ignoreResult=True)
        UnitTestClass("Check that the Ubs() command worked",Get(UnitTestClass.GetElementTypesOfEventRecordingQueue()),"{}")


        # calls the function which closes the test and creates the results
        UnitTestClass.CreateResults()

    @staticmethod
    def OnEvent(evts, source):

        for evt in evts:
            if evt.evt == EvtTypes.CST:
                event = "CST"
            elif evt.evt == EvtTypes.OUP:
                event = "OUP"
            elif evt.evt == EvtTypes.CVA:
                event = "CVA"
            elif evt.evt == EvtTypes.CHG:
                event = "CHG"
            elif evt.evt == EvtTypes.MSG:
                event = "MSG"

        # only record if needed to prevent to much memory use
        if UnitTestClass.useEventRecordingQueue:
            UnitTestClass.eventRecordingQueue.append(event)
        else:
            UnitTestClass.eventRecordingQueue = []

        return

    @staticmethod
    def ClearEventRecording():
        UnitTestClass.eventRecordingQueue = []

    # method to enable recording of the events in the queue
    @staticmethod
    def ChangeEventRecording():
        """Switch on/off the event recording. This is used in combination with the Obs() command"""
        if UnitTestClass.useEventRecordingQueue:
            UnitTestClass.useEventRecordingQueue = False
        else:
            UnitTestClass.useEventRecordingQueue = True


    @staticmethod
    def GetElementTypesOfEventRecordingQueue():
        """returns the types which where recorded int the event recording queue"""
        try:
            typesInEventRecordingQueue = {UnitTestClass.eventRecordingQueue[0]}
            for element in UnitTestClass.eventRecordingQueue[1:]:
                typesInEventRecordingQueue.add(element)
        except Exception:
            typesInEventRecordingQueue = {}
        return typesInEventRecordingQueue



    # below static methods are used to control the test
    @staticmethod
    def Configure(domain, serializer, useResultGenerator=False, resultTemplateFile=None, logTestResultsToFile=False):
        """
        This method is the set up method to configure parameters for the following PyEoq2 test and the UnitTestClass
        instances and should be used before the first instance of UnitTestClass is created.
        The following settings can be set but are predefined (setting can be seen with '()'):
        * useResultGeneratur = True/(False), uses the test to generate a testResultTemplate in the current dir which can be
                               used later for testing. The commands will not be evaluated during this test. Using this will
                               ignore the above settings.
        * resultTemplateFile = (None)/'*yourFileString*', use a resultTemplateFile created with the above useResultGenerator
                               Setting. This will ignore the hardcoded command results in the code and instead use the ones
                               of the given test result template file.
        * logTestResultsToFile = True/(False), simply activates a logger which will save the test results in a .txt file in the
                                 log folder

        The input variables resultTemplateFile, logTestResultsToFile and resultTemplateFile will be checked if they are valid,
        otherwise they will be set to their default value.
        """

        # check input for configuration and print warnings to console and write them into configureWarningsList to print them
        # into test summary
        if useResultGenerator not in [True, False]:
            warningString='warn (UnitTestClass.Configure()): Please use True/False for "useResultGenerator" as parameter,' \
                          ' default value "False" used.'
            print(warningString)
            UnitTestClass.warningsList.append(warningString)
            useResultGenerator = False

        # the pass command is necessary here, because if the negation of the if conditions is used, either one will be
        # true and therefore the warning will be printed
        if resultTemplateFile is None or type(resultTemplateFile) is str:
            pass
        else:
            warningString = 'warn (UnitTestClass.Configure()): Please use "*string*" or None for "resultTemplateFile" as parameter,' \
                          ' default value "None" used.'
            print(warningString)
            UnitTestClass.warningsList.append(warningString)
            resultTemplateFile = None

        if logTestResultsToFile not in [True, False]:
            warningString = 'warn (UnitTestClass.Configure()): Please use True/False for "logTestResultsToFile" as parameter, default' \
                          ' value "False" used.'
            print(warningString)
            UnitTestClass.warningsList.append(warningString)
            logTestResultsToFile = False

        # set start time of test
        UnitTestClass.testTimeStart = perf_counter_ns()

        # map function input values to corresponding attributes of the UnitTestClass
        UnitTestClass.domain = domain
        UnitTestClass.serializer = serializer
        UnitTestClass.useResultGenerator = useResultGenerator
        UnitTestClass.resultTemplateFile = resultTemplateFile
        UnitTestClass.logTestResultsToFile = logTestResultsToFile

        # if a testResultTemplate is given the results will be extracted and saved in UniTestClass.resultsListFromResultTemplate
        if UnitTestClass.resultTemplateFile is not None and not UnitTestClass.useResultGenerator:
            # catch case in which the file does not exist
            try:
                with open(UnitTestClass.resultTemplateFile) as resultTemplateFileHandler:
                    for result in resultTemplateFileHandler:
                        UnitTestClass.resultsListFromResultTemplate.append(result.rstrip())
            except FileNotFoundError:
                warningString='warn (testResultTemplate): specified result template file could not be found using hardcoded results instead'
                print(warningString)
                UnitTestClass.warningsList.append(warningString)

    @staticmethod
    def CreateResults():
        """
        This method closes the test procedure and creates the results, therefore it creates the results depending on
        the settings set in the UnitTestClass.Configure() method:
        * generator mode: the resultTemplateFile is created and saved in the current working dir
        * normal mode without logging: the result table of the test will be created and printed
        * normal mode with logging: the result table of the test will be created, printed and saved in a .txt file
        """

        testElapsedTime = (perf_counter_ns()-UnitTestClass.testTimeStart)/pow(10,9)

        if UnitTestClass.useResultGenerator:
            # set filename and testResultTemplateFileHandler for log file
            testResultTemplateFile = "./PyEoq_UnitTest_result_template.txt"

            # check if result template file already exists and if it exists remove the old file
            if exists(testResultTemplateFile):
                testResultTemplateFile = testResultTemplateFile
                warningString = 'warn (creating test result Template File): file with same name already exists in directory,' \
                                ' replacing file with new one'
                remove(testResultTemplateFile)
                print(warningString)
                UnitTestClass.warningsList.append(warningString)

            # debug printout
            print('Generating test result template file named "%s"\nin %s.......' % (testResultTemplateFile[2:], getcwd()), end='')

            # write results into testTemplateFile
            with open(testResultTemplateFile, "w",newline='\n') as testResultTemplateFileObject:
                for testResult in UnitTestClass.resultGeneratorResultsList:
                    print(testResult, file=testResultTemplateFileObject)

            # debug help to acknowledge that the result writing worked
            print('OK\n')

            # print all warnings which occurred during generator mode
            if UnitTestClass.warningsList:
                print('---Warnings---')
                for warningString in UnitTestClass.warningsList:
                    print(warningString[5:])
                # seperator
                print('')

            # print out time the whole test took
            print('Total time elapsed from start till finished file creating: %.3f [s]'%testElapsedTime)

        # case when the generator mode is not activated
        else:
            # count successful commands
            numberOfSuccesfulCommands = [item[2] for item in UnitTestClass.testResultList].count('OK')

            # insert row description for table at the beginning of the table, since the columns get swapped later, this
            # order is better not used for look up.
            # The order in the printout later is | Nr. | OK/NOK | Command | elapsed time [ms] | Error Message |
            UnitTestClass.testResultList.insert(0, ['Nr.', 'Command', 'OK/NOK', 'elapsed time [ms]', 'Error Message'])

            # get max sizes of columms by getting longest string of every column and saving the value
            # different values for logfile and console print to have a shorter printout for console
            longestStringValuesConsolePrint=[]
            longestStringValueslogFile=[]
            for column in range(0, 5):
                list=[item[column] for item in UnitTestClass.testResultList]
                # get len of longest entry of every column and limit it to 80 signs for the console output
                longestStringValuesConsolePrint.append(len(max(list, key=len)) if len(max(list, key=len)) < 80 else 80)
                longestStringValueslogFile.append(len(max(list, key=len)))

            # insert separator between first row of result table and first test result
            UnitTestClass.testResultList.insert(1, ["-"*element for element in longestStringValueslogFile])

            # printout of test summary and warnings for the logging starts here if it activated
            if UnitTestClass.logTestResultsToFile:
                # datetime object containing current date and time and convert it to the needed format
                dt_string = datetime.now().strftime("%y_%m_%d__%H_%M_%S")

                # set filename and testLogFileHandler for log file
                testLogFile = "./log/UnitTestLog_"+dt_string+".txt"
                try:
                    testLogFileHandler = open(testLogFile, "w")
                except FileNotFoundError:
                    # create directory if it could not be found
                    directory = "log"
                    parent_dir = getcwd()
                    logPath = path.join(parent_dir, directory)
                    mkdir(logPath)
                    testLogFileHandler = open(testLogFile, "w")

                # print test Summary to file
                print('###TestSummary###', file=testLogFileHandler)
                print('->%d out of %d commands successful' % (numberOfSuccesfulCommands, len(UnitTestClass.testResultList)-2),
                      file=testLogFileHandler)
                print('->Test duration %.3f [s]' % testElapsedTime, file=testLogFileHandler)

                # print warnings if any appeared
                if UnitTestClass.warningsList:
                    print('\n---Warnings---', file=testLogFileHandler)
                    for warningString in UnitTestClass.warningsList:
                        # print string without "warn" prefix since the headline already indicates that the message below is
                        # a warning
                        print(warningString[5:], file=testLogFileHandler)

                # print seperator between warnings and test result table
                print('', file=testLogFileHandler)

            # printout of the test summary and warnings for console starts here
            # print test Summary
            print('#'*90+' TestSummary '+'#'*90)
            print('->%d out of %d commands successful' % (numberOfSuccesfulCommands, len(UnitTestClass.testResultList)-2))
            print('->Test duration %.3f [s]' % testElapsedTime, end='\n\n')

            # print warnings from configure if they exist
            if UnitTestClass.warningsList:
                print('\n---Warnings---')
                for warningString in UnitTestClass.warningsList:
                    # print string without "warn" prefix since the headline already indicates that the message below is
                    # a warning
                    print(warningString[5:])
                # print seperator between warnings and result table
                print('')

            # printout of results starts here
            # print testResultList containing all the results of the tests
            for summaryOfCommandTest in UnitTestClass.testResultList:
                # if logging to file is activated also print the table to the .txt log file
                if UnitTestClass.logTestResultsToFile:
                    # format result of every command test for print into file
                    formatedTestResultList=[]
                    for column in range(0, 5):
                        formatedTestResultList.append(summaryOfCommandTest[column].center(longestStringValueslogFile[column]))

                    print(formatedTestResultList[0], formatedTestResultList[2], formatedTestResultList[1], formatedTestResultList[3],
                          formatedTestResultList[4], sep='|', file=testLogFileHandler)

                formatedTestResultList=[]
                for column in range(0, 5):
                    # format column headline
                    if len(summaryOfCommandTest[column]) > 80:
                        formatedTestResultList.append(summaryOfCommandTest[column][0:74]+'......')

                    # format rest of commands
                    else:
                        formatedTestResultList.append(summaryOfCommandTest[column].center(longestStringValuesConsolePrint[column]))

                # print row of table to display
                # the order get changed here to NR,OK/NOK,Command,elapsed time, Error
                print(formatedTestResultList[0], formatedTestResultList[2], formatedTestResultList[1],
                      formatedTestResultList[3],
                      formatedTestResultList[4], sep='|')

            # close file if logging is activated
            if UnitTestClass.logTestResultsToFile:
                testLogFileHandler.close()
                print('\nInfo: results successfully saved in "%s"'%testLogFile, end='\n\n')

            # raise error when not all commands where successfull to trigger a fail in the gitlab ci
            if numberOfSuccesfulCommands != len(UnitTestClass.testResultList)-2:
                failedCommandsNumbersArray = []
                for element in UnitTestClass.testResultList:
                    if element[2] == 'NOK':
                        failedCommandsNumbersArray.append(element[0])
                raise UnitTestFailedError(failedCommandsNumbersArray)

    @staticmethod
    def GetTestResult(testNumber: int):
        """
        Simply returns the test result list of test 'testNumber'
        """
        return UnitTestClass.testResultList[testNumber]

    @staticmethod
    def GetGeneratedResult(testNumber: int):
        """
        Simply returns the test result list of test 'testNumber' when generator mode is active
        """
        return UnitTestClass.resultGeneratorResultsList[testNumber]

    # functions below are used to perform the test on every command
    def __init__(self, commandDescribingString: str, command, expectedCommandResult=None, mustFail=False, ignoreResult=False):
        """This will create a UnitTestClass object with the following input parameters:
        * commandDescribingString = *string*, describes the commando and will be used in printout during test for better
                                    orientation and debugging
        * command = Eoq command string which should be performed
        * expectedCommandResult = this is the expected result of the command, but input string does not matter if mustFail
                                  is set to True. If this parameter is a list and the command result is a list, the procedure
                                  will check if they have the same elements, but not if they are the same order.
        * mustFail = True/False this is optional so also a failed command can be used, standard value is False

        The input values mustFail and commandDescribingString will be checked if they have the type of the expected value.
        Otherwise warnings will be created and the default values will be used.

        In addition the test for this command will be executed after its initialisation to minimize the commands needed
        to command the tests.
        """

        # input checker
        # check if commandDescribingString is of type string, if not try a conversion to string
        if type(commandDescribingString) is not str:
            warningString = 'warn (__init__ of command %d): input "commandDescribingString" is ' \
                          'not of type string, '%UnitTestClass.classVarCurrentCommandNumber \
                          + 'conversion to string will be performed'

            # conversion to string from commandDescribingString and appending error to previous warning if it did not work
            try:
                commandDescribingString=str(commandDescribingString)
            except Exception as e:
                warningString = warningString+'\n'+29*' '+' -> conversion Failed because of %s.'%repr(e)+\
                              ' "command DescribingString" input will be replaced with "Command %d"'%UnitTestClass.classVarCurrentCommandNumber
                # since conversion did not work replace commandDescribingString with command number
                commandDescribingString='Command %d' % UnitTestClass.classVarCurrentCommandNumber

            # the string is printed here at the end of the if block to ensure the final string is printed with all changes
            # made to it during the if-block
            print(warningString)

            # append warning string to list for later printout
            UnitTestClass.warningsList.append(warningString)

        # check if mustFail is correct value and otherwise print warning and set mustFail to default value False
        # and only check for this if no result template file or not the generator mode is used
        if UnitTestClass.resultTemplateFile is not None or not UnitTestClass.useResultGenerator:
            if mustFail not in [True, False]:
                warningString = 'warn (__init__ of command %d): input "mustFail" is not "True"/"False".' \
                              ' Default value "False" will be used.'%UnitTestClass.classVarCurrentCommandNumber
                print(warningString)
                mustFail = False
                UnitTestClass.warningsList.append(warningString)

        # check that ignoreResult is False or True and otherwise use default False
        if ignoreResult not in [True, False]:
            warningString = 'warn (__init__ of command %d): input "ignoreResult" is not "True"/"False".' \
                          ' Default value "False" will be used.'%UnitTestClass.classVarCurrentCommandNumber
            print(warningString)
            ignoreResult = False
            UnitTestClass.warningsList.append(warningString)

        # public variables
        self.wasSuccessful=False  # indicator to show if test was successful mainly used to indicate if the command,
        # which returns the root directory of the .oaam, was successfull

        # private variables
        self.__commandResult=None
        self.__commandExecutionTime=None
        self.__commandDescribingString=commandDescribingString
        self.__command=command
        self.__ignoreResult=ignoreResult

        # get number of current command and increment currentCommandNumber in the UnitTestClass class variable
        self.__commandNumber=UnitTestClass.classVarCurrentCommandNumber
        UnitTestClass.classVarCurrentCommandNumber+=1

        # if result generating is activated the results of the commands are not needed and the part in the init function
        # relating to them is skipped to speed up performance
        if not UnitTestClass.useResultGenerator:
            # if a result template file is given, the extracted results are in UnitTestClass.resultsListFromResultTemplate
            # and will be extracted and assigned to the command
            if len(UnitTestClass.resultsListFromResultTemplate) != 0:
                if 'failed' not in UnitTestClass.resultsListFromResultTemplate[self.__commandNumber]:
                    self.__commandMustFail=False
                else:
                    self.__commandMustFail=True

                # catch case when result should be ignored
                if 'ignore Result' in UnitTestClass.resultsListFromResultTemplate[self.__commandNumber]:
                    self.__ignoreResult=True

                expectedCommandResult=UnitTestClass.resultsListFromResultTemplate[self.__commandNumber]

                self.__expectedCommandResult=expectedCommandResult

            # if the command should fail the expectedResult is not needed and replaced with an output string for the test debug
            elif mustFail:
                self.__commandMustFail=mustFail
                self.__expectedCommandResult='Command should fail'

            # this is the normal case when the hardcoded results are used
            else:
                self.__expectedCommandResult=expectedCommandResult
                self.__commandMustFail=mustFail

        # start test in init function, this should reduce the number of commands needed to execute the test
        self.PerformTest()

    def PerformTest(self):
        """This method simply performs the test of the PyEoq2 based on the settings set in UniTestClass.Configure()"""
        if UnitTestClass.useResultGenerator:
            self.UseResultGenerator()
        else:
            self.EvaluateCommand()

    def EvaluateCommand(self):
        """This function passes the command string of the current UnitTestClass instance to QuickDoCommand, evaluates
        the result string and writes the result in the UnitTestClass.testResultList which will be later used for the test
        summary printout"""
        # create array to save all relevant test result information for later summary:
        # number of current test and serialized current command
        currentTestResultArray = [str(self.__commandNumber), self.__commandDescribingString]# UnitTestClass.serializer.Ser(self.__command)]

        # debug printout to give information about current command
        print('--->Test %d: %s<---'%(self.__commandNumber, self.__commandDescribingString))

        # execute command when no evaluation command is defined
        self.__commandResult, self.__commandExecutionTime=self.QuickDoCommand()

        # If command result is type int convert it to string otherwise, this is done because a int type object
        # is not iterable and so the search for a special char sequence in the command result could not be performed.
        # This should not affect the command result because when self.__mustFail is set true, the given command result is
        # not used.
        if type(self.__commandResult) == int and self.__commandMustFail:
            self.__commandResult = str(self.__commandResult)

        # check if command failed and it is not supposed
        elif self.__commandMustFail or 'Command failed:' not in str(self.__commandResult):
            print('Result: "%s"'%self.__commandResult)
            if self.__ignoreResult is False:
                print('Expected Result: "%s"'%self.__expectedCommandResult)
            else:
                print('Expected Result: [suppressed in command definition]')

            # case in which the result of the command should be ignored, for bare execution and error check
            if self.__ignoreResult is True:
                currentTestResultArray.extend(['OK', str(self.__commandExecutionTime), '-'])
                self.wasSuccessful=True
                print('--->passed<---\n')

            # case in which command should fail and it did
            elif self.__commandMustFail and str == type(self.__commandResult) and 'failed' in self.__commandResult:
                currentTestResultArray.extend(['OK', str(self.__commandExecutionTime), '-'])
                self.wasSuccessful = True
                print('--->passed<---\n')

            # case when command should fail but it did not
            elif self.__commandMustFail and (str != type(self.__commandResult) or 'failed' not in self.__commandResult):
                currentTestResultArray.extend(['NOK', str(self.__commandExecutionTime), 'Command should fail but is working'])
                print('===>FAILED<===\n')

            # case in which command result is not same as expected result
            elif str(self.__commandResult) != str(self.__expectedCommandResult):
                if str(self.__expectedCommandResult)[0] == "[" and str(self.__expectedCommandResult)[-1] == "]" and \
                        str(self.__commandResult)[0] == "[" and str(self.__commandResult)[-1] == "]":
                    # make copy from values as string to compare and delete brackets
                    expectedCommandResult=str(self.__expectedCommandResult)[1:-1].replace(" ", '').split(",")
                    commandResult=str(self.__commandResult)[1:-1]

                    # find position of element in command result and delete it command result and expected command result copy,
                    # then the result is the same as the expected if the leftover string of both variables is empty which means all elements
                    # existing in both strings
                    expectedCommandResultCopy = str(self.__expectedCommandResult)[1:-1]
                    for element in expectedCommandResult:
                        if element in commandResult:
                            start = commandResult.find(element)
                            commandResult = commandResult[:start]+commandResult[start+len(element):]
                            start = expectedCommandResultCopy.find(element)
                            expectedCommandResultCopy = expectedCommandResultCopy[:start]+expectedCommandResultCopy[start+len(element):]

                    commandResult = commandResult.replace(',', '').replace(' ', '')
                    expectedCommandResultCopy = expectedCommandResultCopy.replace(',', '').replace(' ', '')
                    if len(commandResult) == 0 and len(expectedCommandResultCopy) == 0:
                        print('info: Result and Expected Result contain the same elements but not in the same order.\n'
                              '      Therefore the test is evaluated as passed.')
                        currentTestResultArray.extend(['OK', str(self.__commandExecutionTime), '-'])
                        self.wasSuccessful = True
                        print('--->passed<---\n')

                    else:
                        errorString = 'Result Error::"%s" does not match expected "%s"'%(self.__commandResult, self.__expectedCommandResult)
                        currentTestResultArray.extend(['NOK', str(self.__commandExecutionTime), errorString])
                        print('===>FAILED<===\n')

                # case in which command result and expected result are lists but they dont have the same elements
                else:
                    errorString = 'Result Error::"%s" does not match expected "%s"'%(self.__commandResult, self.__expectedCommandResult)
                    currentTestResultArray.extend(['NOK', str(self.__commandExecutionTime), errorString])
                    print('===>FAILED<===\n')

            # case when command worked and command result is expected result
            elif str(self.__commandResult) == str(self.__expectedCommandResult):
                currentTestResultArray.extend(['OK', str(self.__commandExecutionTime), '-'])
                self.wasSuccessful = True  # successful
                print('--->passed<---\n')

        else:
            # covers case when the command did not work because any error like pyeoqError or similar during domain.do()
            currentTestResultArray.extend(['NOK', str(self.__commandExecutionTime), str(self.__commandResult)])
            print('===>FAILED<===\n')

        UnitTestClass.testResultList.append(currentTestResultArray)

    def UseResultGenerator(self):
        """this function is just getting the result of the UnitTestClass-objects commands and appending them to a list
        for later use."""
        # generate results
        print('--->Generating result %d: %s......'%(self.__commandNumber, self.__commandDescribingString))

        # execute command
        res, executionTime = self.QuickDoCommand()
        self.__commandExecutionTime = executionTime

        if self.__ignoreResult is False:
            self.__commandResult = res
        else:
            self.__commandResult = 'ignore Result'

        # append result to resultGeneratorResultsList class variable
        UnitTestClass.resultGeneratorResultsList.append(str(self.__commandResult))

        # acknowledge success
        print('.......OK\n')

    def QuickDoCommand(self):
        """This function uses the input command and returns the time it took to execute the command and the
        result string from the PyEoq2"""
        # take start time to get time take to execute the command
        start = perf_counter_ns()

        # execute command and catch errors
        try:
            val = UnitTestClass.domain.Do(self.__command)
        except Exception as e:
            val = "Command failed: %s"%str(e)

        end = perf_counter_ns()

        # convert time from nanoseconds to milliseconds
        elapsedTime = round((end-start)/pow(10, 6), 3)

        print("Time elapsed: %.3f [ms]"%elapsedTime)

        return val, elapsedTime

# custom error to let the ci of gitlab fail when the pyeoq changes are tested and one(or more) commands fail
class UnitTestFailedError(Exception):
    """One UnitTest failed. Please have a closer look!!!!"""
    def __init__(self,commandsFailedNumbersArray):
        # create a string of the numbers of the commands wich failed to pass this over to the
        # error message to help determining which commands failed
        self.commandsFailed = str(commandsFailedNumbersArray[0])
        for element in commandsFailedNumbersArray[1:]:
            self.commandsFailed = self.commandsFailed + ', ' + str(element)

    # overriding the str method of the exception class to pass information about which commands failed to the user
    def __str__(self):
        return 'Command(s) ' + self.commandsFailed + ' failed. Please fix this issue.'

