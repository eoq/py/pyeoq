import time

__tags__ = ['misc','test'] #classification tags to be used in clients

def abortTest(domain : 'Domain', seconds : 'Real=1.0:The sleep time in seconds'):
    time.sleep(seconds)
    return