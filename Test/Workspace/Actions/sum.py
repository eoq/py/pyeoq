__tags__ = ['misc','test'] #classification tags to be used in clients

def sum(domain: 'Domain', arg1: 'Integer', arg2: 'Integer') -> 'returns sum':
    return arg1 + arg2
