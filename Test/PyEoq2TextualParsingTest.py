"""
    Christian Mollière, 2020

    Script for testing purposes of the text 2 py implementation.
"""

from eoq2.serialization import TextSerializer
import traceback

""" 
    Test Codes
"""

invalidOnPurpose = 3

codes = [
    # cmd with nested qry
    "GET (!Device{(/path/name=CPM)})",
    # cmd with multiple args
    "SET #34 name untitled",
    # qry without cmd
    "/resources{/name='MinimalFlightControl.oaam'}",
    # extra
    "!Task{(/type/name='Elevator'-(/outputs@SIZE~1))}",
    # multi qry args
    "GET !Task.0@(ASSOCIATES,(/allocations))@CLASSNAME",
    # faulty cmd to test error handling
    "GET !Device.0//path",
    # faulty cmd, unfinished query
    "((/test",
    # faulty cmd, wrong stopping symbol
    "!Device{(/path}}",
    # multi line cmp command
    "GET !Device{(/path/name=CPM)};"
    "SET #1 name test;",
    # test alternative coding
    "GET &CLSDevice&IDX0&PTHname"
]

if __name__ == '__main__':
    """
        Testing starts here
    """

    serializer = TextSerializer()
    serializer.EnableDebugging()

    successful = 0
    for idx, code in enumerate(codes):
        try:
            print()
            print(f"Testing {code}")
            result = serializer.Des(code)
            try:
                print("RESULT TEXT2PY")
                print(result)
                print(result.cmd,result.a)
                print("RESULT PY2TEXT")
                print(serializer.Ser(result))
            except:
                pass
            successful += 1
        except:
            traceback.print_exc()
            print("Failed!")
    print()
    print()
    print(f"Successfully assembled {successful} of {len(codes)} codes. {invalidOnPurpose} codes were invalid on purpose!")