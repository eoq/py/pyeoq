from UnitTestFiles.UnitTestClass import UnitTestClass

if __name__ == '__main__':
    # IMPORTANT: This program will use the EoqUserManual (version 16.04.2020) as orientation to perform unittests on
    # PyEoq2 and ensure functionality. It will roughly use the examples given in the documentation.

    # function to Run Unit test in one command parameters are with default value in brackets:
    # modelfile = *string* ('MinimalFlightControl.oaam')
    # useResultGenerator = (False)/True to generate file to use with resultTemplateFile
    # useResultTemplateFile = (False)/True use a generated result template file from result-generator
    # logTestResultsToFile = (False)/True
    
    # generator mode
    #UnitTestClass.RunUnitTest(useResultGenerator=True, logTestResultsToFile=True)

    # normal unit test mode
    UnitTestClass.RunUnitTest(logTestResultsToFile=True, useResultTemplateFile=True)



